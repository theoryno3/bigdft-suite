#This is the configuration file for the BigDFT installer
#This is a python script which is executed by the build suite

#Add the condition testing to run tests and includes PyYaml
conditions.add("bio")
conditions.add("ase")
conditions.add("vdw")
conditions.add("python")
conditions.add("sirius")
conditions.add("devdoc")
conditions.add("simulation")
conditions.add("dill")
conditions.add("boost")
conditions.add("amber")
conditions.add("spg")
#List the module the this rcfile will build
modules = ['spred',]
#example of the potentialities of the python syntax in this file
def env_configuration():
    return  """ FCFLAGS="-I${MKLROOT}/include -O2 -fPIC -qopenmp -g" FC="mpif90 -fc=ifx" CC=icx CFLAGS=-fPIC --with-ext-linalg="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -liomp5 -lpthread -lm -ldl -lstdc++" --with-gobject=yes  --enable-dynamic-libraries CXX=icpx"""
#the following command sets the environment variable to give these settings
#to all the modules
import os
os.environ['BIGDFT_CONFIGURE_FLAGS']=env_configuration()
#here follow the configuration instructions for the modules built
#we specify the configurations for the modules to customize the options if needed
autogenargs = env_configuration()
os.environ['PYGOBJECT_WITHOUT_PYCAIRO']='1'
os.environ['OPENMM_INCLUDE_PATH']=os.path.join(prefix,'include')
os.environ['OPENMM_LIB_PATH']=os.path.join(prefix,'lib')
os.environ['I_MPI_F90']='ifx'
os.environ['I_MPI_CC']='icx'
os.environ['I_MPI_CXX']='icpx'
os.environ['FC']='mpif90'
os.environ['CC']='mpicc'
os.environ['CXX']='mpicxx'

def get_include_dir():
    from subprocess import check_output
    from os import environ
    py3config = environ['PYTHON']+'-config'
    includes = check_output([py3config,'--includes'])
    return includes.split()[0][2:].decode("utf-8")

module_autogenargs.update({
'biopython': "", 'simtk': "", 'pdbfixer': "", 'ase': "",
'dill': "", 'libffi': "CC=icx", 'glib':"AUTOMAKE=automake ACLOCAL=aclocal CC=gcc CXX=g++",
'gobject-introspection': "PYTHON=$PYTHON", 'dnaviewer': "", 'lsim-f2py': "",
'pygobject': "", 'v_sim-dev': "PYTHON=$PYTHON CC=icx"
})

module_cmakeargs.update({
    'ntpoly': "-DFORTRAN_ONLY=Yes -DCMAKE_Fortran_COMPILER='mpif90' -DCMAKE_Fortran_FLAGS_RELEASE='-O2 -g -fPIC' -DBUILD_SHARED_LIBS=Yes -DOpenMP_Fortran_FLAGS='-qopenmp' ",
    'rdkit': "-DRDK_INSTALL_COMIC_FONTS=OFF -DPYTHON_EXECUTABLE=/opt/intel/oneapi/intelpython/latest/bin/python -DPYTHON_LIBRARY=/opt/intel/oneapi/intelpython/latest/lib -DPYTHON_INCLUDE_DIR="+get_include_dir(),
    'hdf5': "-DCMAKE_CXX_COMPILER='icpx' -DCMAKE_Fortran_COMPILER='ifx' -DCMAKE_C_COMPILER='gcc'",
    'dbfft': "-DCMAKE_CXX_COMPILER=icpx", 'openbabel': "-DCMAKE_CXX_COMPILER=g++"
})

module_cmakeargs['spfft'] = "-DCMAKE_CXX_COMPILER='mpicxx' -DCMAKE_BUILD_TYPE=RELEASE -DSPFFT_SINGLE_PRECISION=OFF -DSPFFT_MPI=ON -DSPFFT_OMP=ON " # -DSPFFT_STATIC=ON " # -DSPFFT_GPU_BACKEND=CUDA  -DCMAKE_CUDA_COMPILER=/usr/local/cuda-10.1/bin/nvcc -DCMAKE_CUDA_FLAGS=-ccbin=gcc-8
module_autogenargs['gsl'] = env_configuration()
module_autogenargs['hdf5'] = env_configuration() + " --enable-fortran --disable-deprecated-symbols --disable-filters --disable-parallel --with-zlib=no --with-szlib=no" #--disable-shared --enable-static=yes
module_cmakeargs['sirius'] = "-DUSE_MKL=ON -DMKL_DEF_LIBRARY=${MKLROOT}/lib/intel64"
module_cmakeargs['costa'] = "-DCMAKE_CXX_FLAGS=-fPIC"
#prefix='/nfs/site/home/bauinger/bigDFT_benchmarks/upstream'
module_cmakeargs['dbfft'] = "-DCMAKE_CXX_COMPILER=icpx -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=YES"
prefix='/opt/upstream'
