module liborbs_profiling
  use time_profiling, only: f_timing, TIMING_UNINITIALIZED
  use dynamic_memory, only: f_routine, f_release_routine, f_malloc_dump_status
  implicit none

  public

  integer, private, save :: CONVOL_QUARTIC_ID = TIMING_UNINITIALIZED
  integer, private, save :: WFD_CREATION_ID = TIMING_UNINITIALIZED
  integer, private, save :: WFD_COMM_ID = TIMING_UNINITIALIZED
  integer, private, save :: BOUNDS_ID = TIMING_UNINITIALIZED

contains

  function CONVOL_QUARTIC_CAT() result(cat_id)
    use time_profiling, only: f_timing_category
    implicit none
    integer :: cat_id

    if (CONVOL_QUARTIC_ID == TIMING_UNINITIALIZED) then
       call f_timing_category('convolQuartic','Convolutions' ,'No OpenCL', &
            CONVOL_QUARTIC_ID)
    end if
    cat_id = CONVOL_QUARTIC_ID
  end function CONVOL_QUARTIC_CAT

  function WFD_CREATION_CAT() result(cat_id)
    use time_profiling, only: f_timing_category
    implicit none
    integer :: cat_id

    if (WFD_CREATION_ID == TIMING_UNINITIALIZED) then
       call f_timing_category('wfd_creation','Other' ,'Miscellaneous', &
            WFD_CREATION_ID)
    end if
    cat_id = WFD_CREATION_ID
  end function WFD_CREATION_CAT

  function WFD_COMM_CAT() result(cat_id)
    use time_profiling, only: f_timing_category
    implicit none
    integer :: cat_id

    if (WFD_COMM_ID == TIMING_UNINITIALIZED) then
       call f_timing_category('comm_llr','Other' ,'Miscellaneous', &
            WFD_COMM_ID)
    end if
    cat_id = WFD_COMM_ID
  end function WFD_COMM_CAT

  function BOUNDS_CAT() result(cat_id)
    use time_profiling, only: f_timing_category
    implicit none
    integer :: cat_id

    if (BOUNDS_ID == TIMING_UNINITIALIZED) then
       call f_timing_category('calc_bounds','Other' ,'Miscellaneous', &
            BOUNDS_ID)
    end if
    cat_id = BOUNDS_ID
  end function BOUNDS_CAT

end module liborbs_profiling
