#ifndef KINETIC_H
#define KINETIC_H

#include "liborbs_ocl.h"

void create_kinetic_kernels(liborbs_context * context, struct liborbs_kernels * kernels);
void build_kinetic_programs(liborbs_context * context);

void clean_kinetic_kernels(struct liborbs_kernels * kernels);
void clean_kinetic_programs(liborbs_context * context);

/** Performs the three dimensional Kinetic filter with periodic boundary conditions on K point data.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param h hgrid along the three dimensions. Vector of three values.
 *  @param x input buffer of size dimensions[0] * dimensions[1] * dimensions[2] * sizeof(complex double). Stored in column major order.
 *  @param y input and output buffer of size dimensions[0] * dimensions[1] * dimensions[2] * sizeof(complex double). Stored in column major order.
 *  @param work_x work buffer of size dimensions[0] * dimensions[1] * dimensions[2] * sizeof(complex double). Stored in column major order.
 *  @param work_y work buffer of size dimensions[0] * dimensions[1] * dimensions[2] * sizeof(complex double). Stored in column major order. work_y = y + kinetic(x).
 *  @param c_in constant affecting the scaling factor.
 *  @param k point coordinates. Verctor of three values.
 */
void kinetic_k_d(liborbs_command_queue *command_queue,
                 const cl_uint dimensions[3], const double h[3],
                 cl_mem x, cl_mem y, cl_mem work_x, cl_mem work_y,
                 double c_in,  const double k[3]);

/** Version of kinetic_d using two temporary buffer to avoid erasing input arrays. @see kinetic_d. */
void kinetic_stable_d(liborbs_command_queue *command_queue,
                      const cl_uint dimensions[3], const double h[3],
                      cl_mem x, cl_mem y, cl_mem work_x, cl_mem work_y,
                      cl_mem tmp_x, cl_mem tmp_y);

void kinetic_k_d_generic(liborbs_command_queue *command_queue,
                         const cl_uint dimensions[3], const cl_uint periodic[3],
                         const double h[3], const double k[3],
                         cl_mem x_r, cl_mem x_i, cl_mem y_r, cl_mem y_i,
                         cl_mem work_x_r, cl_mem work_x_i, cl_mem work_y_r, cl_mem work_y_i);

/** Performs the three dimensional Kinetic filter with periodic or non periodic boundary conditions. Input arrays are lost.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param h hgrid along the three dimensions. Vector of three values.
 *  @param x input buffer of size (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in column major order.
 *  @param y input buffer of size (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in column major order.
 *  @param work_x work buffer of size (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in column major order.
 *  @param work_y output buffer of size (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in column major order. work_y = y + kinetic(x).
 */
void kinetic_d_generic(liborbs_command_queue *command_queue,
                       const cl_uint dimensions[3], const cl_uint periodic[3],
                       const double h[3],
                       cl_mem x, cl_mem y, cl_mem work_x, cl_mem work_y);

/** Performs the three dimensional Kinetic filter with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param h hgrid along the three dimensions. Vector of three values.
 *  @param x input buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param y input buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param work_x work buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param work_y output buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order. work_y = y + kinetic(x).
 */
void kinetic_d(liborbs_command_queue *command_queue,
               const cl_uint dimensions[3], const double h[3],
               cl_mem x, cl_mem y, cl_mem work_x, cl_mem work_y);

/** Performs the one dimensional kinetic filter and transposition with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param h hgrid along the dimension processed.
 *  @param c scaling factor.
 *  @param x input buffer of size ndat * n * sizeof(double), stored in column major order.
 *  @param y output buffer of size n * ndat * sizeof(double), stored in column major order.
 *  @param workx output buffer of size n * ndat * sizeof(double), stored in column major order. Transposition of x.
 *  @param work_y temporary buffer used to store intermediate results. Size ndat * n * sizeof(double), stored in column major order.
 *  @param ekin dummy argument. Will be used to compute the kinetic energy.
 */
void kinetic1d_d(liborbs_command_queue *command_queue,
                 cl_uint n, cl_uint ndat, double h, double c,
                 cl_mem x, cl_mem y, cl_mem workx, cl_mem worky, double *ekin);

#endif
