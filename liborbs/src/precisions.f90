! +++++++++++++++++++++++++++++++++
! DON'T ADD ANYTHING TO THIS MODULE
! +++++++++++++++++++++++++++++++++
module liborbs_precisions
  use f_precisions, only: gp => f_double, dp => f_double, wp => f_double
  implicit none
end module liborbs_precisions
