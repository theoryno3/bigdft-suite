#ifndef WAVELET_H
#define WAVELET_H

#include "liborbs_ocl.h"

/* struct liborbs_wavelet { */
/*   cl_program anaProgram; */
/*   cl_program synProgram; */

/*   cl_kernel ana1d_kernel_d; */
/*   cl_kernel ana1d_block_kernel_d; */
/*   cl_kernel anashrink1d_kernel_d; */
/*   cl_kernel syn1d_kernel_d; */
/*   cl_kernel syngrow1d_kernel_d; */
/* }; */


void create_wavelet_kernels(liborbs_context * context, struct liborbs_kernels * kernels);
void build_wavelet_programs(liborbs_context * context);

void clean_wavelet_kernels(struct liborbs_kernels * kernels);
void clean_wavelet_programs(liborbs_context * context);

/** Slightly more performing version of anashrink1d_d. @see anashrink1d_d. */
void ana1d_block_d(liborbs_command_queue *command_queue,
                   cl_uint n,cl_uint ndat, cl_mem psi,cl_mem out);

/** Slightly more performing version of ana_d. @see ana_d. */
void ana_block_d(liborbs_command_queue *command_queue,
                 const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

/** Performs the one dimensional wavelet analysis and transposition with open boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * (2 * n + 14) * sizeof(double), stored in column major order.
 *  @param out output buffer of size (2 * n) * ndat * sizeof(double), stored in column major order.
 */
void anashrink1d_d(liborbs_command_queue *command_queue,
                   cl_uint n, cl_uint ndat, cl_mem psi, cl_mem out);

/** Performs the one dimensional wavelet analysis and transposition with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * (2 * n) * sizeof(double), stored in column major order.
 *  @param out output buffer of size (2 * n) * ndat * sizeof(double), stored in column major order.
 */
void ana1d_d(liborbs_command_queue *command_queue,
             cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Performs the three-dimensional wavelet analysis with periodic or non periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2]) * sizeof(double) in size.
 *  @param psi input buffer of dimension : (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in collumn major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 */
void ana_d_generic(liborbs_command_queue *command_queue,
                   const cl_uint dimensions[3], const cl_uint periodic[3],
                   cl_mem tmp, cl_mem psi, cl_mem out);

/** Version of ana_sef_d without the temporary buffer, psi is erased during the computation. @see ana_d_generic. */
void ana_self_d_generic(liborbs_command_queue *command_queue,
                        const cl_uint dimensions[3], const cl_uint periodic[3],
                        cl_mem psi, cl_mem out);

/** Performs the three-dimensional wavelet analysis with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param tmp temporary buffer to store intermediate results. Dimensions : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param psi input buffer of dimension : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 */
void ana_d(liborbs_command_queue *command_queue, const cl_uint dimensions[3],
           cl_mem tmp, cl_mem psi, cl_mem out);

/** Version of ana_d without the temporary buffer, psi is erased during the computation. @see ana_d. */
void ana_self_d(liborbs_command_queue *command_queue,
                const cl_uint dimensions[3], cl_mem psi, cl_mem out);


/** Performs the one dimensional wavelet analysis and transposition with open boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * (2 * n) * sizeof(double), stored in column major order.
 *  @param out output buffer of size (2 * n + 14) * ndat * sizeof(double), stored in column major order.
 */
void syngrow1d_d(liborbs_command_queue *command_queue,
                 cl_uint n, cl_uint ndat, cl_mem psi, cl_mem out);

/** Performs the one dimensional wavelet synthesis and transposition with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * (2 * n) * sizeof(double), stored in column major order.
 *  @param out output buffer of size (2 * n) * ndat * sizeof(double), stored in column major order.
 */
void syn1d_d(liborbs_command_queue *command_queue,
             cl_uint n, cl_uint ndat,cl_mem psi, cl_mem out);

/** Performs the three-dimensional wavelet analysis with periodic or non periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least (2 * dimensions[0]) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[1]?0:14)) * sizeof(double) in size.
 *  @param psi input buffer of dimension : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in column major order.
 */
void syn_d_generic(liborbs_command_queue *command_queue,
                   const cl_uint dimensions[3], const cl_uint periodic[3],
                   cl_mem tmp, cl_mem psi, cl_mem out);

/** Version of syn_sef_d without the temporary buffer, psi is erased during the computation. @see syn_d_generic. */
void syn_self_d_generic(liborbs_command_queue *command_queue,
                        const cl_uint dimensions[3], const cl_uint periodic[3],
                        cl_mem psi, cl_mem out);

/** Performs the three-dimensional wavelet synthesis with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param tmp temporary buffer to store intermediate results. Dimensions : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param psi input buffer of dimension : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 */
void syn_d(liborbs_command_queue *command_queue,
           const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

/** Version of syn_d without the temporary buffer, psi is erased during the computation. @see syn_d. */
void syn_self_d(liborbs_command_queue *command_queue,
                const cl_uint dimensions[3], cl_mem psi, cl_mem out);

#endif
