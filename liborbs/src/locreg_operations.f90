!> @file
!! Local Region operations
!! @author Copyright (C) 2015-2015 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Module for the local region operations on orbitals
module locreg_operations
  use liborbs_precisions
  use dynamic_memory
  use locregs
  use f_enums
  use f_utils, only: f_zero
  use f_precisions, only: f_address
  implicit none

  private

  integer,parameter,public :: NCPLX_MAX = 2

  !> Information for the confining potential to be used in TMB scheme
  !! The potential is supposed to be defined as prefac*(r-rC)**potorder
  type, public :: confpot_data
     integer :: potorder                !< Order of the confining potential
     integer, dimension(3) :: ioffset   !< Offset for the coordinates of potential lr in global region
     real(gp) :: prefac                 !< Prefactor
     real(gp), dimension(3) :: hh       !< Grid spacings in ISF grid
     real(gp), dimension(3) :: rxyzConf !< Confining potential center in global coordinates
     real(gp) :: damping                !< Damping factor to be used after the restart
  end type confpot_data

  !> Contains the work arrays needed for expressing wavefunction in real space
  !! with all the BC
  type, public :: workarr_sumrho
     integer :: nw1,nw2,nxc,nxf
     real(wp), dimension(:), pointer :: x_c,x_f,w1,w2
  end type workarr_sumrho

  !> Contains the work arrays needed for hamiltonian application with all the BC
  type, public :: workarr_locham
     integer :: nw1,nw2,nxc,nyc,nxf1,nxf2,nxf3,nxf,nyf
     real(wp), dimension(:), pointer :: w1,w2
     !for the periodic BC case, these arrays substitute
     !psifscf,psifscfk,psig,ww respectively
     real(wp), dimension(:,:), pointer :: x_c,y_c,x_f1,x_f2,x_f3,x_f,y_f
  end type workarr_locham

  !> Contains the work arrays needed for th preconditioner with all the BC
  !! Take different pointers depending on the boundary conditions
  type, public :: workarr_precond
     integer, dimension(:), pointer :: modul1,modul2,modul3
     real(wp), dimension(:), pointer :: psifscf,ww,x_f1,x_f2,x_f3,kern_k1,kern_k2,kern_k3
     real(wp), dimension(:,:), pointer :: af,bf,cf,ef
     real(wp), dimension(:,:,:), pointer :: xpsig_c,ypsig_c,x_c
     real(wp), dimension(:,:,:,:), pointer :: xpsig_f,ypsig_f,x_f,y_f
     real(wp), dimension(:,:,:,:,:), pointer :: z1,z3 ! work array for FFT
  end type workarr_precond

  type, public :: workarrays_quartic_convolutions
     real(wp), dimension(:,:,:), pointer :: xx_c, xy_c, xz_c
     real(wp), dimension(:,:,:), pointer :: xx_f1
     real(wp), dimension(:,:,:), pointer :: xy_f2
     real(wp), dimension(:,:,:), pointer :: xz_f4
     real(wp), dimension(:,:,:,:), pointer :: xx_f, xy_f, xz_f
     real(wp), dimension(:,:,:), pointer :: y_c
     real(wp), dimension(:,:,:,:), pointer :: y_f
     ! The following arrays are work arrays within the subroutine
     real(wp), dimension(:,:), pointer :: aeff0array, beff0array, ceff0array, eeff0array
     real(wp), dimension(:,:), pointer :: aeff0_2array, beff0_2array, ceff0_2array, eeff0_2array
     real(wp), dimension(:,:), pointer :: aeff0_2auxarray, beff0_2auxarray, ceff0_2auxarray, eeff0_2auxarray
     real(wp), dimension(:,:,:), pointer :: xya_c, xyc_c
     real(wp), dimension(:,:,:), pointer :: xza_c, xzc_c
     real(wp), dimension(:,:,:), pointer :: yza_c, yzb_c, yzc_c, yze_c
     real(wp), dimension(:,:,:,:), pointer :: xya_f, xyb_f, xyc_f, xye_f
     real(wp), dimension(:,:,:,:), pointer :: xza_f, xzb_f, xzc_f, xze_f
     real(wp), dimension(:,:,:,:), pointer :: yza_f, yzb_f, yzc_f, yze_f
  end type workarrays_quartic_convolutions

  type,public :: workarrays_projectors
     real(wp),pointer,dimension(:,:,:,:) :: wprojx,wprojy,wprojz
     real(wp),pointer,dimension(:) :: wproj
     real(wp),pointer,dimension(:,:,:) :: work
  end type workarrays_projectors

  type, public :: workarrays_ocl_sumrho
     integer(f_address) :: psi_c, psi_f
     integer(f_address) :: work1, work2, work3, d
  end type workarrays_ocl_sumrho

  type, public :: workarrays_ocl_precond
     integer(f_address) :: psi_c_r, psi_f_r
     integer(f_address) :: psi_c_b, psi_f_b
     integer(f_address) :: psi_c_d, psi_f_d

     integer(f_address), dimension(2) :: b_host
  end type workarrays_ocl_precond

  type, public :: workarrays_tolr
     integer :: nseg
     integer, dimension(:), pointer :: nbsegs_cf, keyag_lin_cf
  end type workarrays_tolr

  public :: psir_to_vpsi,isf_to_daub_kinetic
  public :: daub_to_isf_ocl, isf_to_daub_ocl
  public :: full_locham_ocl_async
  public :: precondition_residue_ocl
  public :: density_ocl
  public :: nullify_confpot_data
  public :: Lpsi_to_global2
  public :: global_to_local_parallel
  public :: boundary_weight
  public :: psi_to_locreg2
  public :: global_to_local
  public :: initialize_work_arrays_sumrho,deallocate_work_arrays_sumrho,nullify_work_arrays_sumrho
  public :: initialize_work_arrays_locham,deallocate_work_arrays_locham,reset_work_arrays_locham
  public :: memspace_work_arrays_sumrho,memspace_work_arrays_locham,memspace_work_arrays_precond
  public :: allocate_work_arrays,init_local_work_arrays,deallocate_work_arrays
  public :: deallocate_workarrays_quartic_convolutions,zero_local_work_arrays
  public :: nullify_workarrays_projectors, allocate_workarrays_projectors, deallocate_workarrays_projectors
  public :: set_lr_to_lr
  public :: workarrays_projectors_null

  public :: create_ocl_sumrho, deallocate_ocl_sumrho
  public :: set_ocl_sumrho_sync, set_ocl_sumrho_async
  public :: get_ocl_sumrho_sync, get_ocl_sumrho_async

  public :: create_ocl_precond, deallocate_ocl_precond
  public :: set_ocl_precond_b, pin_ocl_precond_b, unpin_ocl_precond_b

  public :: create_workarrays_tolr, deallocate_workarrays_tolr, workarrays_tolr_add_plr, workarrays_tolr_allocate
  interface create_workarrays_tolr
     module procedure create_workarrays_tolr_0d, create_workarrays_tolr_1d
  end interface create_workarrays_tolr

  ! to avoid creating array temporaries
  interface initialize_work_arrays_sumrho
      module procedure initialize_work_arrays_sumrho_nlr
      module procedure initialize_work_arrays_sumrho_llr
  end interface initialize_work_arrays_sumrho

  interface initialize_work_arrays_locham
      module procedure initialize_work_arrays_locham_nlr
      module procedure initialize_work_arrays_locham_llr
  end interface initialize_work_arrays_locham

  contains

    pure function workarrays_projectors_null() result(wp)
      implicit none
      type(workarrays_projectors) :: wp
      call nullify_workarrays_projectors(wp)
    end function workarrays_projectors_null

    pure subroutine nullify_workarrays_projectors(wp)
      implicit none
      type(workarrays_projectors),intent(out) :: wp
      nullify(wp%wprojx)
      nullify(wp%wprojy)
      nullify(wp%wprojz)
      nullify(wp%wproj)
      nullify(wp%work)
    end subroutine nullify_workarrays_projectors

    subroutine allocate_workarrays_projectors(ndims, wp)
      implicit none
      integer, dimension(3), intent(in) :: ndims
      type(workarrays_projectors),intent(inout) :: wp
      integer,parameter :: nterm_max=20
      integer,parameter :: nw=65536
      wp%wprojx = f_malloc_ptr((/ 1.to.NCPLX_MAX, 0.to.ndims(1)-1, 1.to.2, 1.to.nterm_max /),id='wprojx')
      wp%wprojy = f_malloc_ptr((/ 1.to.NCPLX_MAX, 0.to.ndims(2)-1, 1.to.2, 1.to.nterm_max /),id='wprojy')
      wp%wprojz = f_malloc_ptr((/ 1.to.NCPLX_MAX, 0.to.ndims(3)-1, 1.to.2, 1.to.nterm_max /),id='wprojz')
      wp%wproj = f_malloc_ptr(NCPLX_MAX*maxval(ndims)*2,id='wprojz')
      wp%work = f_malloc_ptr((/ 0.to.nw, 1.to.2, 1.to.2 /),id='work')
    end subroutine allocate_workarrays_projectors

    subroutine deallocate_workarrays_projectors(wp)
      implicit none
      type(workarrays_projectors),intent(inout) :: wp
      call f_free_ptr(wp%wprojx)
      call f_free_ptr(wp%wprojy)
      call f_free_ptr(wp%wprojz)
      call f_free_ptr(wp%wproj)
      call f_free_ptr(wp%work)
    end subroutine deallocate_workarrays_projectors

    subroutine free_tolr_ptr(tolr)
      use compression
      implicit none
      type(wfd_to_wfd), dimension(:), pointer :: tolr
      !local variables
      integer :: ilr
      if (.not. associated(tolr)) return
      do ilr=lbound(tolr,1),ubound(tolr,1)
         call deallocate_wfd_to_wfd(tolr(ilr))
      end do
      deallocate(tolr)
      nullify(tolr)
    end subroutine free_tolr_ptr

    subroutine create_workarrays_tolr_0d(w, lr)
      use locregs
      use dynamic_memory
      implicit none
      type(workarrays_tolr), intent(out) :: w
      type(locreg_descriptors), intent(in) :: lr

      w%keyag_lin_cf = f_malloc_ptr(lr%wfd%nseg_c + lr%wfd%nseg_f, id="nbsegs_cf")
      nullify(w%nbsegs_cf)
      w%nseg = 0
    end subroutine create_workarrays_tolr_0d

    subroutine create_workarrays_tolr_1d(w, lrs)
      use locregs
      use dynamic_memory
      implicit none
      type(workarrays_tolr), intent(out) :: w
      type(locreg_descriptors), dimension(:), intent(in) :: lrs

      integer :: nseg, i

      nseg = 0
      do i = 1, size(lrs)
         nseg = max(nseg, lrs(i)%wfd%nseg_c + lrs(i)%wfd%nseg_f)
      end do

      w%keyag_lin_cf = f_malloc_ptr(nseg, id="nbsegs_cf")
      nullify(w%nbsegs_cf)
      w%nseg = 0
    end subroutine create_workarrays_tolr_1d

    subroutine workarrays_tolr_add_plr(w, plr)
      use locregs
      use dynamic_memory
      implicit none
      type(workarrays_tolr), intent(inout) :: w
      type(locreg_descriptors), intent(in) :: plr

      w%nseg = max(w%nseg, plr%wfd%nseg_c + plr%wfd%nseg_f)
    end subroutine workarrays_tolr_add_plr
    
    subroutine workarrays_tolr_allocate(w)
      use locregs
      use dynamic_memory
      implicit none
      type(workarrays_tolr), intent(inout) :: w

      w%nbsegs_cf = f_malloc_ptr(w%nseg, id = "nbsegs_cf")
    end subroutine workarrays_tolr_allocate
    
    subroutine deallocate_workarrays_tolr(w)
      use dynamic_memory
      implicit none
      type(workarrays_tolr), intent(inout) :: w

      call f_free_ptr(w%nbsegs_cf)
      call f_free_ptr(w%keyag_lin_cf)
    end subroutine deallocate_workarrays_tolr

    !> initialize the information for matching the localisation region
    !! of each projector to all the localisation regions of the system
    subroutine set_lr_to_lr(Plr,Glr,w,lrs,lr_mask)
      use compression
      use dictionaries, only: f_err_raise
      implicit none
      type(interacting_locreg), intent(inout) :: Plr
      type(locreg_descriptors), intent(in) :: Glr !<global simulation domain
      type(workarrays_tolr), intent(inout) :: w
      !> mask array which is associated to the localization regions of interest in this processor
      logical, dimension(:), optional, intent(in) :: lr_mask
      !> descriptors of all the localization regions of the simulation domain
      !! susceptible to interact with the projector
      type(locreg_descriptors), dimension(:), optional, intent(in) :: lrs
      !local variables
      logical :: overlap
      integer :: ilr, iilr, ioverlap

      call f_routine(id='set_wfd_to_wfd')

      call free_tolr_ptr(Plr%tolr)
      call f_free_ptr(Plr%lut_tolr)

      if (Plr%plr%wfd%nseg_c + Plr%plr%wfd%nseg_f == 0) return

      ! Determine the size of tolr and initialize the corresponding lookup table
      Plr%nlr=1
      overlap=.true.
      if (present(lrs)) then
         Plr%nlr=size(lrs)
      end if
      if (Plr%nlr <=0) return

      ! Count how many overlaps exist
      Plr%noverlap=0
      do ilr=1,Plr%nlr
         !control if the projector overlaps with this locreg
         if (present(lrs)) then
            overlap=.true.
            if (present(lr_mask)) overlap=lr_mask(ilr)
            if (overlap) call check_overlap(lrs(ilr),Plr%plr,Glr,overlap)
            !if there is overlap, activate the strategy for the application
            if (overlap) then
               Plr%noverlap=Plr%noverlap+1
            end if
         else
            Plr%noverlap=Plr%noverlap+1
         end if
      end do
      Plr%lut_tolr = f_malloc0_ptr(Plr%noverlap,id='lut_tolr')

      ! Now assign the values
      ioverlap=0
      do ilr=1,Plr%nlr
         !control if the projector overlaps with this locreg
         if (present(lrs)) then
            overlap=.true.
            if (present(lr_mask)) overlap=lr_mask(ilr)
            if (overlap) call check_overlap(lrs(ilr),Plr%plr,Glr,overlap)
            !if there is overlap, activate the strategy for the application
            if (overlap) then
               ioverlap=ioverlap+1
               Plr%lut_tolr(ioverlap)=ilr
            end if
         else
            ioverlap=ioverlap+1
            Plr%lut_tolr(ioverlap)=ilr
         end if
      end do
      if (ioverlap/=Plr%noverlap) stop 'ioverlap/=Plr%noverlap'

      if (present(lrs) .and. present(lr_mask)) then
         if (f_err_raise(Plr%nlr /= size(lr_mask),'The sizes of lr_mask and lrs should coincide',&
              err_name='BIGDFT_RUNTIME_ERROR')) return
      end if
      !allocate the pointer with the good size
      allocate(Plr%tolr(Plr%noverlap))
      !then for any of the localization regions check the strategy
      !for applying the projectors
      !ioverlap=0
      do ilr=1,Plr%noverlap
         iilr=Plr%lut_tolr(ilr)
         !this will set to PSP_APPLY_STRATEGY_SKIP the projector application
         call nullify_wfd_to_wfd(Plr%tolr(ilr))
         !now control if the projector overlaps with this locreg
         if (present(lrs)) then
            overlap=.true.
            if (present(lr_mask)) overlap=lr_mask(iilr)
            if (overlap) call check_overlap(lrs(iilr),Plr%plr,Glr,overlap)
            !if there is overlap, activate the strategy for the application
            if (overlap) then
               call init_tolr(Plr%tolr(ilr),lrs(iilr)%wfd,Plr%plr%wfd,w%keyag_lin_cf,w%nbsegs_cf)
            end if
         else
            call init_tolr(Plr%tolr(ilr),Glr%wfd,Plr%plr%wfd,w%keyag_lin_cf,w%nbsegs_cf)
         end if
         !then the best strategy can be decided according to total number of
         !common points
         !complete stategy, the packing array is created after first projector
!!$         if (overlap) tolr(ilr)%strategy=STRATEGY_MASK_PACK
         if (overlap) call tolr_set_strategy(Plr%tolr(ilr),'MASK_PACK')
         !masking is used but packing is not created,
         !useful when only one projector has to be applied
         !tolr(ilr)%strategy=PSP_APPLY_STRATEGY_MASK
         !old scheme, even though mask arrays is created it is never used.
         !most likely this scheme is useful for debugging purposes
         !tolr(ilr)%strategy=PSP_APPLY_STRATEGY_KEYS
      end do

      !!if (ioverlap/=Plr%noverlap) stop 'ioverlap/=Plr%noverlap'

      call f_release_routine()

    end subroutine set_lr_to_lr

    subroutine initialize_locham_sized(nspinor, nw1, nw2, nc, nf, nfi, init_to_zero, w)
      use dynamic_memory
      implicit none
      integer, intent(in) :: nspinor, nw1, nw2, nc, nf, nfi
      logical, intent(in) :: init_to_zero
      type(workarr_locham), intent(out) :: w

      w%nw1 = nw1
      w%nw2 = nw2
      w%nyc = nc
      w%nyf = nf
      w%nxc = nc
      w%nxf = nf
      w%nxf1 = nfi
      w%nxf2 = nfi
      w%nxf3 = nfi

      nullify(w%w1)
      nullify(w%w2)
      nullify(w%x_c)
      nullify(w%y_c)
      nullify(w%x_f1)
      nullify(w%x_f2)
      nullify(w%x_f3)
      nullify(w%x_f)
      nullify(w%y_f)
      !allocation of work arrays
      if (nc > 0 .and. init_to_zero) then
         w%y_c = f_malloc0_ptr((/ w%nyc, nspinor /),id='w%y_c',info='{alignment: 32}')
         w%x_c = f_malloc0_ptr((/ w%nxc, nspinor /),id='w%x_c')
      else if (nc > 0) then
         w%y_c = f_malloc_ptr((/ w%nyc, nspinor /),id='w%y_c',info='{alignment: 32}')
         w%x_c = f_malloc_ptr((/ w%nxc, nspinor /),id='w%x_c')
      end if
      if (nf > 0) then
         w%y_f = f_malloc0_ptr((/ w%nyf, nspinor /),id='w%y_f')
         w%x_f = f_malloc0_ptr((/ w%nxf, nspinor /),id='w%x_f')
      end if
      if (nw1 > 0) w%w1 = f_malloc_ptr(w%nw1,id='w%w1')
      if (nw2 > 0) w%w2 = f_malloc_ptr(w%nw2,id='w%w2')
      if (nfi > 0) then
         w%x_f1 = f_malloc0_ptr((/ w%nxf1, nspinor /),id='w%x_f1')
         w%x_f2 = f_malloc0_ptr((/ w%nxf2, nspinor /),id='w%x_f2')
         w%x_f3 = f_malloc0_ptr((/ w%nxf3, nspinor /),id='w%x_f3')
      end if
    end subroutine initialize_locham_sized

    subroutine locham_sizes(nw1, nw2, nc, nf, nfi, coarse, interp, fine, geocode, hyb)
      implicit none
      integer, intent(out) :: nw1, nw2, nc, nf, nfi
      integer, dimension(3), intent(in) :: coarse, interp, fine
      character(len = 1), intent(in) :: geocode
      logical, intent(in) :: hyb

      integer :: n1, n2, n3, n1i, n2i, n3i, n1f, n2f, n3f

      n1 = coarse(1)
      n2 = coarse(2)
      n3 = coarse(3)
      n1i = interp(1)
      n2i = interp(2)
      n3i = interp(3)
      n1f = fine(1)
      n2f = fine(2)
      n3f = fine(3)

      nw1 = 0
      nw2 = 0
      nc = 0
      nf = 0
      nfi = 0
      select case(geocode)
      case('F')
         !dimensions of work arrays
         ! shrink convention: nw1>nw2
         nw1=max(n3*n1i*n2i, n1*n2i*n3i,&
              2*(n1f+1)*(2*n2f+31)*(2*n3f+31),&
              2*(n3f+1)*(2*n1f+31)*(2*n2f+31))
         nw2=max(4*(n2f+1)*(n3f+1)*(2*n1f+31),&
              4*(n1f+1)*(n2f+1)*(2*n3f+31),&
              n1*n2*n3i, n1i*n2*n3)

         nfi=(n1f+1)*(n2f+1)*(n3f+1)
         nc = n1*n2*n3
         nf = 7 * nfi
      case('S')
         nc = n1i*n2i*n3i
      case('P')
         if (hyb) then
            ! Wavefunction expressed everywhere in fine scaling functions (for potential and kinetic energy)
            nfi=(n1f+1)*(n2f+1)*(n3f+1)

            nw1=max(4*(n2f+1)*(n3f+1)*n1i,&
                 n1i*(n2+1)*(n3+1),&
                 2*n3*n1*n2,&      ! for the comb_shrink_hyb_c
                 4*n3i*(n1f+1)*(n2f+1)) ! for the _f

            nw2=max(2*(n3f+1)*n1i*n2i,&
                 n3*n1i*n2i,&
                 4*n2*n3*n1,&   ! for the comb_shrink_hyb_c
                 2*n2i*n3i*(n1f+1)) ! for the _f

            nc = n1*n2*n3
            nf = 7*nfi
         else
            nc = n1i*n2i*n3i
         endif
      case('W')
         nc = n1i*n2i*n3i
      end select
    end subroutine locham_sizes

    !> Initialize work arrays for local hamiltonian
    subroutine initialize_work_arrays_locham_nlr(nlr,lr,nspinor,w)
      use at_domain, only: domain_periodic_dims,domain_geocode
      use liborbs_errors
      implicit none
      integer, intent(in) :: nlr, nspinor
      type(locreg_descriptors), dimension(nlr), intent(in) :: lr
      type(workarr_locham), intent(out) :: w
      !local variables
      logical :: hyb
      integer :: ilr
      integer, dimension(3) :: coarse,interp, nfine
      integer :: n1f,n2f,n3f,nw,nww,nf,nc, nfi
      character(len=1) :: geocode


      ! Determine the maximum array sizes for all locregs 1,..,nlr
      ! If the sizes for a specific locreg are needed, simply call the routine with nlr=1
      ! For the moment the geocode of all locregs must be the same
      coarse = 0
      interp = 0
      n1f=0
      n2f=0
      n3f=0

      if (nlr > 0) then
         geocode = domain_geocode(lr(1)%mesh%dom)
         hyb=lr(1)%hybrid_on
      else
         geocode = 'F'
         hyb = .false.
      end if

      do ilr=1,nlr
         coarse = max(coarse, lr(ilr)%mesh_coarse%ndims)
         interp = max(interp ,lr(ilr)%mesh%ndims)
         n1f=max(n1f,lr(ilr)%nboxf(2,1)-lr(ilr)%nboxf(1,1))
         n2f=max(n2f,lr(ilr)%nboxf(2,2)-lr(ilr)%nboxf(1,2))
         n3f=max(n3f,lr(ilr)%nboxf(2,3)-lr(ilr)%nboxf(1,3))
         if (any(domain_periodic_dims(lr(ilr)%mesh%dom) .neqv. domain_periodic_dims(lr(1)%mesh%dom))) &
             call f_err_throw('The lrs do not have same BC', err_id = LIBORBS_LOCREG_ERROR())
         if (lr(ilr)%hybrid_on .neqv. hyb) &
             call f_err_throw('lr(ilr)%hybrid_on .neqv. hyb', err_id = LIBORBS_LOCREG_ERROR())
      end do

      nfine = (/ n1f, n2f, n3f /)
      call locham_sizes(nw, nww, nc, nf, nfi, coarse, interp, &
           nfine, geocode, hyb)
      call initialize_locham_sized(nspinor, nw, nww, nc, nf, nfi, .false., w)
    END SUBROUTINE initialize_work_arrays_locham_nlr

    !> Initialize work arrays for local hamiltonian
    subroutine initialize_work_arrays_locham_llr(lr,nspinor,w)
      use at_domain, only: domain_geocode
      implicit none
      integer, intent(in) ::  nspinor
      type(locreg_descriptors), intent(in) :: lr
      type(workarr_locham), intent(out) :: w
      !local variables
      integer :: nw,nww,nf,nc, nfi
      integer, dimension(3) :: nfs

      nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
      nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
      nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)
      call locham_sizes(nw, nww, nc, nf, nfi, lr%mesh_coarse%ndims, lr%mesh%ndims, &
           nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
      call initialize_locham_sized(nspinor, nw, nww, nc, nf, nfi, &
           domain_geocode(lr%mesh%dom) == 'F' .or. lr%hybrid_on, w)
    END SUBROUTINE initialize_work_arrays_locham_llr

    !> Reset work array sizes for given lr
    subroutine reset_work_arrays_locham(w, lr)
      use at_domain, only: domain_geocode
      use liborbs_errors
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      type(workarr_locham), intent(inout) :: w
      !local variables
      integer :: nw,nww,nf,nc, nfi
      integer, dimension(3) :: nfs

      nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
      nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
      nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)

      call locham_sizes(nw, nww, nc, nf, nfi, lr%mesh_coarse%ndims, lr%mesh%ndims, &
           nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
      if (f_err_raise(nw > size(w%w1), "too small w1 size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
           f_err_raise(nww > size(w%w2), "too small w2 size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
           f_err_raise(nc > size(w%x_c), "too small x_c size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
           f_err_raise(nf > size(w%x_f), "too small x_f size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
           f_err_raise(nfi > size(w%x_f1), "too small x_fi size in reset", err_id = LIBORBS_LOCREG_ERROR())) return
      
      w%nw1 = nw
      w%nw2 = nww
      w%nyc = nc
      w%nyf = nf
      w%nxc = nc
      w%nxf = nf
      w%nxf1 = nfi
      w%nxf2 = nfi
      w%nxf3 = nfi

      if (domain_geocode(lr%mesh%dom) == 'F' .or. lr%hybrid_on .or. domain_geocode(lr%mesh%dom) == 'W') then
         call f_zero(w%x_c)
         call f_zero(w%y_c)
         call f_zero(w%x_f)
         call f_zero(w%y_f)
         call f_zero(w%x_f1)
         call f_zero(w%x_f2)
         call f_zero(w%x_f3)
      end if
    end subroutine reset_work_arrays_locham

    subroutine memspace_work_arrays_locham(lr,memwork) !n(c) nspinor (arg:2)
      use at_domain, only: domain_geocode
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer(kind=8), intent(out) :: memwork
      !local variables
      integer :: nw1,nw2,nc,nf,nfi
      integer, dimension(3) :: nfs

      nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
      nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
      nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)

      call locham_sizes(nw1, nw2, nc, nf, nfi, lr%mesh_coarse%ndims, lr%mesh%ndims, &
           nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
      memwork=nw1+nw2+nc+nf+nc+nf+3*nfi
    END SUBROUTINE memspace_work_arrays_locham


    subroutine deallocate_work_arrays_locham(w)
      implicit none
      type(workarr_locham), intent(inout) :: w
      !local variables
      character(len=*), parameter :: subname='deallocate_work_arrays_locham'

      call f_free_ptr(w%y_c,cptr=.true.)
      call f_free_ptr(w%x_c)
      call f_free_ptr(w%x_f1)
      call f_free_ptr(w%x_f2)
      call f_free_ptr(w%x_f3)
      call f_free_ptr(w%y_f)
      call f_free_ptr(w%x_f)
      call f_free_ptr(w%w1)
      call f_free_ptr(w%w2)
    END SUBROUTINE deallocate_work_arrays_locham


    subroutine initialize_work_arrays_sumrho_nlr(nlr,lr,allocate_arrays,w)
      use at_domain, only: domain_periodic_dims,domain_geocode
      use dictionaries, only: f_err_throw
      implicit none
      integer, intent(in) :: nlr
      type(locreg_descriptors), dimension(nlr), intent(in) :: lr
      logical, intent(in) :: allocate_arrays
      type(workarr_sumrho), intent(out) :: w
      !local variables
      character(len=*), parameter :: subname='initialize_work_arrays_sumrho'
      integer :: n1,n2,n3,n1i,n2i,n3i,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3!n(c) n1i,n2i,n3i
      integer :: ilr
      character(len=1) :: geo
      logical :: hyb

      call f_routine(id='initialize_work_arrays_sumrho')

      ! Determine the maximum array sizes for all locregs 1,..,nlr
      ! If the sizes for a specific locreg are needed, simply call the routine with nlr=1
      ! For the moment the geocode of all locregs must be the same

      n1=0
      n2=0
      n3=0
      n1i=0
      n2i=0
      n3i=0
      nfl1=1000000000
      nfl2=1000000000
      nfl3=1000000000
      nfu1=0
      nfu2=0
      nfu3=0
      if (nlr > 0) then
         geo=domain_geocode(lr(1)%mesh%dom)
!!$      geo=lr(1)%geocode
         hyb=lr(1)%hybrid_on
      else
         geo='F'
         hyb=.false.
      end if
      do ilr=1,nlr
         n1=max(n1,lr(ilr)%mesh_coarse%ndims(1))
         n2=max(n2,lr(ilr)%mesh_coarse%ndims(2))
         n3=max(n3,lr(ilr)%mesh_coarse%ndims(3))
         n1i=max(n1i,lr(ilr)%mesh%ndims(1))
         n2i=max(n2i,lr(ilr)%mesh%ndims(2))
         n3i=max(n3i,lr(ilr)%mesh%ndims(3))
         nfl1=min(nfl1,lr(ilr)%nboxf(1,1))
         nfl2=min(nfl2,lr(ilr)%nboxf(1,2))
         nfl3=min(nfl3,lr(ilr)%nboxf(1,3))
         nfu1=max(nfu1,lr(ilr)%nboxf(2,1))
         nfu2=max(nfu2,lr(ilr)%nboxf(2,2))
         nfu3=max(nfu3,lr(ilr)%nboxf(2,3))
         if (any(domain_periodic_dims(lr(ilr)%mesh%dom) .neqv. domain_periodic_dims(lr(1)%mesh%dom))) &
             call f_err_throw('The lrs do not have same BC',err_name='BIGDFT_RUNTIME_ERROR')
         if (lr(ilr)%hybrid_on .neqv. hyb) stop 'lr(ilr)%hybrid_on .neqv. hyb'
      end do

      if (allocate_arrays) then
         nullify(w%x_c)
         nullify(w%x_f)
         nullify(w%w1)
         nullify(w%w2)
      end if

!!$      select case(geo)
      select case(geo)
      case('F')
         !dimension of the work arrays
         ! shrink convention: nw1>nw2
         w%nw1=max(n3*n1i*n2i,&
              n1*n2i*n3i,&
              2*(nfu1-nfl1+1)*(2*(nfu2-nfl2)+31)*(2*(nfu3-nfl3)+31),&
              2*(nfu3-nfl3+1)*(2*(nfu1-nfl1)+31)*(2*(nfu2-nfl2)+31))
         w%nw2=max(4*(nfu2-nfl2+1)*(nfu3-nfl3+1)*(2*(nfu1-nfl1)+31),&
              4*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(2*(nfu3-nfl3)+31),&
              n1*n2*n3i,&
              n1i*n2*n3)
         w%nxc=n1*n2*n3!(2*n1+2)*(2*n2+2)*(2*n3+2)
         w%nxf=7*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(nfu3-nfl3+1)
      case('S')
         !dimension of the work arrays
         w%nw1=1
         w%nw2=1
         w%nxc=n1i*n2i*n3i
         w%nxf=1
      case('P')
         if (hyb) then
            ! hybrid case:
            w%nxc=n1*n2*n3
            w%nxf=7*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(nfu3-nfl3+1)

            w%nw1=max(4*(nfu2-nfl2+1)*(nfu3-nfl3+1)*n1i,n1i*n2*n3)
            w%nw1=max(w%nw1,2*n3*n1*n2)      ! for the comb_shrink_hyb_c
            w%nw1=max(w%nw1,4*n3i*(nfu1-nfl1+1)*(nfu2-nfl2+1)) ! for the _f

            w%nw2=max(2*(nfu3-nfl3+1)*n1i*n2i,n3*n1i*n2i)
            w%nw2=max(w%nw2,4*n2*n3*n1)   ! for the comb_shrink_hyb_c
            w%nw2=max(w%nw2,2*n2i*n3i*(nfu1-nfl1+1)) ! for the _f
         else
            !dimension of the work arrays, fully periodic case
            w%nw1=1
            w%nw2=1
            w%nxc=n1i*n2i*n3i
            w%nxf=1
         endif
      case('W')
         !dimension of the work arrays
         w%nw1=1
         w%nw2=1
         w%nxc=n1i*n2i*n3i
         w%nxf=1
      end select
      !work arrays
      if (allocate_arrays) then
         w%x_c = f_malloc_ptr(w%nxc,id='w%x_c')
         w%x_f = f_malloc_ptr(w%nxf,id='w%x_f')
         w%w1 = f_malloc_ptr(w%nw1,id='w%w1')
         w%w2 = f_malloc_ptr(w%nw2,id='w%w2')
      end if


      if (geo == 'F') then
         call f_zero(w%x_c)
         call f_zero(w%x_f)
      end if

      call f_release_routine()

    END SUBROUTINE initialize_work_arrays_sumrho_nlr


    subroutine initialize_work_arrays_sumrho_llr(lr,allocate_arrays,w)
      use at_domain, only: domain_geocode
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      logical, intent(in) :: allocate_arrays
      type(workarr_sumrho), intent(out) :: w
      !local variables
      character(len=*), parameter :: subname='initialize_work_arrays_sumrho'
      integer :: n1,n2,n3,n1i,n2i,n3i,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3!n(c) n1i,n2i,n3i

      call f_routine(id='initialize_work_arrays_sumrho')

      ! Determine the maximum array sizes for all locregs 1,..,nlr
      ! If the sizes for a specific locreg are needed, simply call the routine with nlr=1
      ! For the moment the geocode of all locregs must be the same

      n1=lr%mesh_coarse%ndims(1)
      n2=lr%mesh_coarse%ndims(2)
      n3=lr%mesh_coarse%ndims(3)
      n1i=lr%mesh%ndims(1)
      n2i=lr%mesh%ndims(2)
      n3i=lr%mesh%ndims(3)
      nfl1=lr%nboxf(1,1)
      nfl2=lr%nboxf(1,2)
      nfl3=lr%nboxf(1,3)
      nfu1=lr%nboxf(2,1)
      nfu2=lr%nboxf(2,2)
      nfu3=lr%nboxf(2,3)

      if (allocate_arrays) then
         nullify(w%x_c)
         nullify(w%x_f)
         nullify(w%w1)
         nullify(w%w2)
      end if

!!$      select case(lr%geocode)
      select case(domain_geocode(lr%mesh%dom))
      case('F')
         !dimension of the work arrays
         ! shrink convention: nw1>nw2
         w%nw1=max(n3*n1i*n2i,&
              n1*n2i*n3i,&
              2*(nfu1-nfl1+1)*(2*(nfu2-nfl2)+31)*(2*(nfu3-nfl3)+31),&
              2*(nfu3-nfl3+1)*(2*(nfu1-nfl1)+31)*(2*(nfu2-nfl2)+31))
         w%nw2=max(4*(nfu2-nfl2+1)*(nfu3-nfl3+1)*(2*(nfu1-nfl1)+31),&
              4*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(2*(nfu3-nfl3)+31),&
              n1*n2*n3i,&
              n1i*n2*n3)
         w%nxc=n1*n2*n3!(2*n1+2)*(2*n2+2)*(2*n3+2)
         w%nxf=7*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(nfu3-nfl3+1)
      case('S')
         !dimension of the work arrays
         w%nw1=1
         w%nw2=1
         w%nxc=n1i*n2i*n3i
         w%nxf=1
      case('P')
         if (lr%hybrid_on) then
            ! hybrid case:
            w%nxc=n1*n2*n3
            w%nxf=7*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(nfu3-nfl3+1)

            w%nw1=max(4*(nfu2-nfl2+1)*(nfu3-nfl3+1)*n1i,n1i*n2*n3)
            w%nw1=max(w%nw1,2*n3*n1*n2)      ! for the comb_shrink_hyb_c
            w%nw1=max(w%nw1,4*n3i*(nfu1-nfl1+1)*(nfu2-nfl2+1)) ! for the _f

            w%nw2=max(2*(nfu3-nfl3+1)*n1i*n2i,n3*n1i*n2i)
            w%nw2=max(w%nw2,4*n2*n3*n1)   ! for the comb_shrink_hyb_c
            w%nw2=max(w%nw2,2*n2i*n3i*(nfu1-nfl1+1)) ! for the _f
         else
            !dimension of the work arrays, fully periodic case
            w%nw1=1
            w%nw2=1
            w%nxc=n1i*n2i*n3i
            w%nxf=1
         endif
      case('W')
         !dimension of the work arrays
         w%nw1=1
         w%nw2=1
         w%nxc=n1i*n2i*n3i
         w%nxf=1
      end select
      !work arrays
      if (allocate_arrays) then
         w%x_c = f_malloc_ptr(w%nxc,id='w%x_c')
         w%x_f = f_malloc_ptr(w%nxf,id='w%x_f')
         w%w1 = f_malloc_ptr(w%nw1,id='w%w1')
         w%w2 = f_malloc_ptr(w%nw2,id='w%w2')
      end if


!!$      if (lr%geocode == 'F') then
      if (domain_geocode(lr%mesh%dom) == 'F') then
         call f_zero(w%x_c)
         call f_zero(w%x_f)
      end if

      call f_release_routine()

    END SUBROUTINE initialize_work_arrays_sumrho_llr

    pure subroutine nullify_work_arrays_sumrho(ws)
      implicit none
      type(workarr_sumrho),intent(out) :: ws
      nullify(ws%x_c)
      nullify(ws%x_f)
      nullify(ws%w1)
      nullify(ws%w2)
    end subroutine nullify_work_arrays_sumrho

    subroutine memspace_work_arrays_sumrho(lr,memwork)
      use at_domain, only: domain_geocode
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer(kind=8), intent(out) :: memwork
      !local variables
      integer :: n1,n2,n3,n1i,n2i,n3i,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3
      integer :: nw1,nw2,nxc,nxf

      n1=lr%mesh_coarse%ndims(1)
      n2=lr%mesh_coarse%ndims(2)
      n3=lr%mesh_coarse%ndims(3)
      n1i=lr%mesh%ndims(1)
      n2i=lr%mesh%ndims(2)
      n3i=lr%mesh%ndims(3)
      nfl1=lr%nboxf(1,1)
      nfl2=lr%nboxf(1,2)
      nfl3=lr%nboxf(1,3)
      nfu1=lr%nboxf(2,1)
      nfu2=lr%nboxf(2,2)
      nfu3=lr%nboxf(2,3)

!!$      select case(lr%geocode)
      select case(domain_geocode(lr%mesh%dom))
      case('F')
         !dimension of the work arrays
         ! shrink convention: nw1>nw2
         nw1=max(n3*n1i*n2i,&
              n1*n2i*n3i,&
              2*(nfu1-nfl1+1)*(2*(nfu2-nfl2)+31)*(2*(nfu3-nfl3)+31),&
              2*(nfu3-nfl3+1)*(2*(nfu1-nfl1)+31)*(2*(nfu2-nfl2)+31))
         nw2=max(4*(nfu2-nfl2+1)*(nfu3-nfl3+1)*(2*(nfu1-nfl1)+31),&
              4*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(2*(nfu3-nfl3)+31),&
              n1*n2*n3i,&
              n1i*n2*n3)
         nxc=n1*n2*n3!(2*n1+2)*(2*n2+2)*(2*n3+2)
         nxf=7*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(nfu3-nfl3+1)
      case('S')
         !dimension of the work arrays
         nw1=1
         nw2=1
         nxc=n1i*n2i*n3i
         nxf=1
      case('P')
         if (lr%hybrid_on) then
            ! hybrid case:
            nxc=n1*n2*n3
            nxf=7*(nfu1-nfl1+1)*(nfu2-nfl2+1)*(nfu3-nfl3+1)

            nw1=max(4*(nfu2-nfl2+1)*(nfu3-nfl3+1)*n1i,n1i*n2*n3)
            nw1=max(nw1,2*n3*n1*n2)      ! for the comb_shrink_hyb_c
            nw1=max(nw1,4*n3i*(nfu1-nfl1+1)*(nfu2-nfl2+1)) ! for the _f

            nw2=max(2*(nfu3-nfl3+1)*n1i*n2i,n3*n1i*n2i)
            nw2=max(nw2,4*n2*n3*n1)   ! for the comb_shrink_hyb_c
            nw2=max(nw2,2*n2i*n3i*(nfu1-nfl1+1)) ! for the _f
         else
            !dimension of the work arrays, fully periodic case
            nw1=1
            nw2=1
            nxc=n1i*n2i*n3i
            nxf=1
         endif
      case('W')
         !dimension of the work arrays
         nw1=1
         nw2=1
         nxc=n1i*n2i*n3i
         nxf=1
      case default
         nw1 = 0
         nw2 = 0
         nxc = 0
         nxf = 0
      end select
      memwork=nxc+nxf+nw1+nw2

    END SUBROUTINE memspace_work_arrays_sumrho


    subroutine deallocate_work_arrays_sumrho(w)
      implicit none
      type(workarr_sumrho), intent(inout) :: w
      !local variables
      character(len=*), parameter :: subname='deallocate_work_arrays_sumrho'

      call f_routine(id='deallocate_work_arrays_sumrho')

      call f_free_ptr(w%x_c)
      call f_free_ptr(w%x_f)
      call f_free_ptr(w%w1)
      call f_free_ptr(w%w2)

      call f_release_routine()

    END SUBROUTINE deallocate_work_arrays_sumrho

!!$    subroutine allocate_work_arrays(geocode,hybrid_on,ncplx,d,w)
    subroutine allocate_work_arrays(lr,ncplx,w)
      use at_domain, only: domain_geocode
      implicit none
!!$      character(len=1), intent(in) :: geocode !< @copydoc poisson_solver::doc::geocode
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: ncplx
      type(workarr_precond), intent(out) :: w
      !local variables
      character(len=*), parameter :: subname='allocate_work_arrays'
      integer, parameter :: lowfil=-14,lupfil=14
      integer :: nd1,nd2,nd3
      integer :: n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b
      integer :: nf

      select case(domain_geocode(lr%mesh%dom))
      case('F')
!!$      if (geocode == 'F') then
         !if (cell_geocode(mesh) == 'F') then

         nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)
         !allocate work arrays
         w%xpsig_c = f_malloc_ptr((/ 0.to.(lr%mesh_coarse%ndims(1)-1), &
              0.to.(lr%mesh_coarse%ndims(2)-1), &
              0.to.(lr%mesh_coarse%ndims(3)-1) /),id='w%xpsig_c')
         w%xpsig_f = f_malloc_ptr((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), &
              lr%nboxf(1,2).to.lr%nboxf(2,2), lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='w%xpsig_f')
         w%ypsig_c = f_malloc_ptr((/ 0.to.(lr%mesh_coarse%ndims(1)-1), &
              0.to.(lr%mesh_coarse%ndims(2)-1), &
              0.to.(lr%mesh_coarse%ndims(3)-1) /),id='w%ypsig_c')
         w%ypsig_f = f_malloc_ptr((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), &
              lr%nboxf(1,2).to.lr%nboxf(2,2), lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='w%ypsig_f')

         w%x_f1 = f_malloc_ptr(nf,id='w%x_f1')
         w%x_f2 = f_malloc_ptr(nf,id='w%x_f2')
         w%x_f3 = f_malloc_ptr(nf,id='w%x_f3')

      case('P')
!!$      else if (geocode == 'P') then
         !else if (cell_geocode(mesh) == 'P') then

         if (lr%hybrid_on) then

            call dimensions_fft(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1, &
                 lr%mesh_coarse%ndims(3)-1, &
                 nd1,nd2,nd3,n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b)

            nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)

            w%kern_k1 = f_malloc_ptr(0.to.(lr%mesh_coarse%ndims(1)-1),id='w%kern_k1')
            w%kern_k2 = f_malloc_ptr(0.to.(lr%mesh_coarse%ndims(2)-1),id='w%kern_k2')
            w%kern_k3 = f_malloc_ptr(0.to.(lr%mesh_coarse%ndims(3)-1),id='w%kern_k3')
            w%z1 = f_malloc_ptr((/ 2, nd1b, nd2, nd3, 2 /),id='w%z1')
            w%z3 = f_malloc_ptr((/ 2, nd1, nd2, nd3f, 2 /),id='w%z3')
            w%x_c = f_malloc_ptr((/ 0.to.(lr%mesh_coarse%ndims(1)-1), &
              0.to.(lr%mesh_coarse%ndims(2)-1), &
              0.to.(lr%mesh_coarse%ndims(3)-1) /),id='w%x_c')

            w%x_f = f_malloc_ptr((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), &
                 lr%nboxf(1,2).to.lr%nboxf(2,2), lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='w%x_f')
            w%x_f1 = f_malloc_ptr(nf,id='w%x_f1')
            w%x_f2 = f_malloc_ptr(nf,id='w%x_f2')
            w%x_f3 = f_malloc_ptr(nf,id='w%x_f3')
            w%y_f = f_malloc_ptr((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), &
                 lr%nboxf(1,2).to.lr%nboxf(2,2), lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='w%y_f')
            w%ypsig_c = f_malloc_ptr((/ 0.to.(lr%mesh_coarse%ndims(1)-1), &
              0.to.(lr%mesh_coarse%ndims(2)-1), &
              0.to.(lr%mesh_coarse%ndims(3)-1) /),id='w%ypsig_c')


         else

            if (ncplx == 1) then
               !periodic, not k-points
               w%modul1 = f_malloc_ptr(lowfil.to.(lr%mesh_coarse%ndims(1)+lupfil-1),id='w%modul1')
               w%modul2 = f_malloc_ptr(lowfil.to.(lr%mesh_coarse%ndims(2)+lupfil-1),id='w%modul2')
               w%modul3 = f_malloc_ptr(lowfil.to.(lr%mesh_coarse%ndims(3)+lupfil-1),id='w%modul3')
               w%af = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%af')
               w%bf = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%bf')
               w%cf = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%cf')
               w%ef = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%ef')
            end if

            w%psifscf = f_malloc_ptr(ncplx*lr%mesh_fine%ndim,id='w%psifscf')
            w%ww = f_malloc_ptr(ncplx*lr%mesh_fine%ndim,id='w%ww')

         end if

!!$      else if (geocode == 'S') then
      !else if (cell_geocode(mesh) == 'S') then
      case('S')

         if (ncplx == 1) then
            w%modul1 = f_malloc_ptr(lowfil.to.(lr%mesh_coarse%ndims(1)+lupfil-1),id='w%modul1')
            w%modul3 = f_malloc_ptr(lowfil.to.(lr%mesh_coarse%ndims(3)+lupfil-1),id='w%modul3')
            w%af = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%af')
            w%bf = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%bf')
            w%cf = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%cf')
            w%ef = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%ef')
         end if

         w%psifscf = f_malloc_ptr(ncplx*lr%mesh_fine%ndim,id='w%psifscf')
         w%ww = f_malloc_ptr(ncplx*lr%mesh_fine%ndim,id='w%ww')

      case('W')

         if (ncplx == 1) then
            w%modul3 = f_malloc_ptr(lowfil.to.(lr%mesh_coarse%ndims(3)+lupfil-1),id='w%modul3')
            w%af = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%af')
            w%bf = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%bf')
            w%cf = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%cf')
            w%ef = f_malloc_ptr((/ lowfil.to.lupfil, 1.to.3 /),id='w%ef')
         end if

         w%psifscf = f_malloc_ptr(ncplx*lr%mesh_fine%ndim,id='w%psifscf')
         w%ww = f_malloc_ptr(ncplx*lr%mesh_fine%ndim,id='w%ww')
         w%kern_k3 = f_malloc_ptr(0.to.(lr%mesh_coarse%ndims(3)-1),id='w%kern_k3')
         w%x_c = f_malloc_ptr((/ 0.to.(lr%mesh_coarse%ndims(1)-1), &
              0.to.(lr%mesh_coarse%ndims(2)-1), &
              0.to.(lr%mesh_coarse%ndims(3)-1) /),id='w%x_c')

      end select

    END SUBROUTINE allocate_work_arrays


    subroutine memspace_work_arrays_precond(lr,ncplx,memwork)
      use at_domain, only: domain_geocode
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: ncplx
      integer(kind=8), intent(out) :: memwork
      !local variables
      integer, parameter :: lowfil=-14,lupfil=14
      integer :: nd1,nd2,nd3
      integer :: n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b
      integer :: nf


      select case(domain_geocode(lr%mesh%dom))
      case('F')

         nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)

         memwork=2*lr%mesh_coarse%ndim+2*7*nf+3*nf


      case ('P')
         
         if (lr%hybrid_on) then

            call dimensions_fft(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1, &
                 lr%mesh_coarse%ndims(3)-1, &
                 nd1,nd2,nd3,n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b)

            nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-&
                 lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)

            memwork=sum(lr%mesh_coarse%ndims)+2*nd1b*nd2*nd3*2+2*nd1*nd2*nd3f*2+&
                 lr%mesh_coarse%ndim+2*7*nf+3*nf

         else

            memwork=0
            if (ncplx == 1) then
               memwork=sum(lr%mesh_coarse%ndims)-3+15*(lupfil-lowfil+1)
            end if
            memwork=memwork+2*ncplx*lr%mesh_fine%ndim

         end if

      case ('S')

         memwork=0
         if (ncplx == 1) then
            memwork=lr%mesh_coarse%ndims(1)-1+lr%mesh_coarse%ndims(3)-1+14*(lupfil-lowfil+1)
         end if
         memwork=memwork+2*ncplx*lr%mesh_fine%ndim

      case ('W')
         memwork=0
         if (ncplx == 1) then
            memwork=lr%mesh_coarse%ndims(3)-1+14*(lupfil-lowfil+1)  !!! To be checked !!!
         end if
         memwork=memwork+2*ncplx*lr%mesh_fine%ndim

      end select

    END SUBROUTINE memspace_work_arrays_precond

!!$    subroutine deallocate_work_arrays(geocode,hybrid_on,ncplx,w)
    subroutine deallocate_work_arrays(mesh,hybrid_on,ncplx,w)
      use box, only: cell
      use at_domain, only: domain_geocode
      implicit none
!!$      character(len=1), intent(in) :: geocode !< @copydoc poisson_solver::doc::geocode
      type(cell), intent(in) :: mesh
      logical, intent(in) :: hybrid_on
      integer, intent(in) :: ncplx
      type(workarr_precond), intent(inout) :: w
      !local variables
      character(len=*), parameter :: subname='deallocate_work_arrays'

!!$      if (geocode == 'F') then
      if (domain_geocode(mesh%dom) == 'F') then

         call f_free_ptr(w%xpsig_c)
         call f_free_ptr(w%ypsig_c)
         call f_free_ptr(w%xpsig_f)
         call f_free_ptr(w%ypsig_f)
         call f_free_ptr(w%x_f1)
         call f_free_ptr(w%x_f2)
         call f_free_ptr(w%x_f3)

!!$      else if ((geocode == 'P' .and. .not. hybrid_on) .or. geocode == 'S') then
      else if ((domain_geocode(mesh%dom) == 'P' .and. .not. hybrid_on) .or. domain_geocode(mesh%dom) == 'S') then

         if (ncplx == 1) then
            call f_free_ptr(w%modul1)
!!$            if (geocode /= 'S') then
            if (domain_geocode(mesh%dom) /= 'S') then
               call f_free_ptr(w%modul2)
            end if
            call f_free_ptr(w%modul3)
            call f_free_ptr(w%af)
            call f_free_ptr(w%bf)
            call f_free_ptr(w%cf)
            call f_free_ptr(w%ef)
         end if

         call f_free_ptr(w%psifscf)
         call f_free_ptr(w%ww)

!!$      else if (geocode == 'P' .and. hybrid_on) then
      else if (domain_geocode(mesh%dom) == 'P' .and. hybrid_on) then

         call f_free_ptr(w%z1)
         call f_free_ptr(w%z3)
         call f_free_ptr(w%kern_k1)
         call f_free_ptr(w%kern_k2)
         call f_free_ptr(w%kern_k3)
         call f_free_ptr(w%x_c)
         call f_free_ptr(w%x_f)
         call f_free_ptr(w%x_f1)
         call f_free_ptr(w%x_f2)
         call f_free_ptr(w%x_f3)
         call f_free_ptr(w%y_f)
         call f_free_ptr(w%ypsig_c)

      else if (domain_geocode(mesh%dom) == 'W') then

         if (ncplx == 1) then
            call f_free_ptr(w%modul3)
            call f_free_ptr(w%af)
            call f_free_ptr(w%bf)
            call f_free_ptr(w%cf)
            call f_free_ptr(w%ef)
         end if

         call f_free_ptr(w%psifscf)
         call f_free_ptr(w%ww)
         call f_free_ptr(w%kern_k3)
         call f_free_ptr(w%x_c)

      end if

    END SUBROUTINE deallocate_work_arrays

    subroutine deallocate_workarrays_quartic_convolutions(work)
      implicit none

      ! Calling arguments
      type(workarrays_quartic_convolutions),intent(inout):: work

      call f_free_ptr(work%xx_c)
      call f_free_ptr(work%xy_c)
      call f_free_ptr(work%xz_c)
      call f_free_ptr(work%xx_f1)
      call f_free_ptr(work%xx_f)
      call f_free_ptr(work%xy_f2)
      call f_free_ptr(work%xy_f)
      call f_free_ptr(work%xz_f4)
      call f_free_ptr(work%xz_f)
      call f_free_ptr(work%y_c)
      call f_free_ptr(work%y_f)
      call f_free_ptr(work%aeff0array)
      call f_free_ptr(work%beff0array)
      call f_free_ptr(work%ceff0array)
      call f_free_ptr(work%eeff0array)
      call f_free_ptr(work%aeff0_2array)
      call f_free_ptr(work%beff0_2array)
      call f_free_ptr(work%ceff0_2array)
      call f_free_ptr(work%eeff0_2array)
      call f_free_ptr(work%aeff0_2auxarray)
      call f_free_ptr(work%beff0_2auxarray)
      call f_free_ptr(work%ceff0_2auxarray)
      call f_free_ptr(work%eeff0_2auxarray)
      call f_free_ptr(work%xya_c)
      call f_free_ptr(work%xyc_c)
      call f_free_ptr(work%xza_c)
      call f_free_ptr(work%xzc_c)
      call f_free_ptr(work%yza_c)
      call f_free_ptr(work%yzb_c)
      call f_free_ptr(work%yzc_c)
      call f_free_ptr(work%yze_c)
      call f_free_ptr(work%xya_f)
      call f_free_ptr(work%xyb_f)
      call f_free_ptr(work%xyc_f)
      call f_free_ptr(work%xye_f)
      call f_free_ptr(work%xza_f)
      call f_free_ptr(work%xzb_f)
      call f_free_ptr(work%xzc_f)
      call f_free_ptr(work%xze_f)
      call f_free_ptr(work%yza_f)
      call f_free_ptr(work%yzb_f)
      call f_free_ptr(work%yzc_f)
      call f_free_ptr(work%yze_f)

    end subroutine deallocate_workarrays_quartic_convolutions


    subroutine init_local_work_arrays(lr, with_confpot, work)
      use locregs
      implicit none

      ! Calling arguments
      type(locreg_descriptors), intent(in) :: lr
      logical,intent(in):: with_confpot
      type(workarrays_quartic_convolutions),intent(inout):: work

      ! Local variables
      integer :: n1, n2, n3
      integer :: nfl1, nfu1, nfl2, nfu2, nfl3, nfu3
      integer:: i
      integer,parameter :: lowfil=-14,lupfil=14

      n1 = lr%mesh_coarse%ndims(1)-1
      n2 = lr%mesh_coarse%ndims(2)-1
      n3 = lr%mesh_coarse%ndims(3)-1
      nfl1 = lr%nboxf(1,1)
      nfu1 = lr%nboxf(2,1)
      nfl2 = lr%nboxf(1,2)
      nfu2 = lr%nboxf(2,2)
      nfl3 = lr%nboxf(1,3)
      nfu3 = lr%nboxf(2,3)

      work%xx_c = f_malloc0_ptr((/ 0.to.n1, 0.to.n2, 0.to.n3 /),id='work%xx_c')
      work%xy_c = f_malloc0_ptr((/ 0.to.n2, 0.to.n1, 0.to.n3 /),id='work%xy_c')
      work%xz_c = f_malloc0_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%xz_c')

      work%xx_f1 = f_malloc0_ptr((/ nfl1.to.nfu1, nfl2.to.nfu2, nfl3.to.nfu3 /),id='work%xx_f1')
      work%xx_f = f_malloc0_ptr((/ 1.to.7, nfl1.to.nfu1, nfl2.to.nfu2, nfl3.to.nfu3 /),id='work%xx_f')


      work%xy_f2 = f_malloc0_ptr((/ nfl2.to.nfu2, nfl1.to.nfu1, nfl3.to.nfu3 /),id='work%xy_f2')
      work%xy_f = f_malloc0_ptr((/ 1.to.7, nfl2.to.nfu2, nfl1.to.nfu1, nfl3.to.nfu3 /),id='work%xy_f')


      work%xz_f4 = f_malloc0_ptr((/ nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%xz_f4')
      work%xz_f = f_malloc0_ptr((/ 1.to.7, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%xz_f')


      work%y_c = f_malloc0_ptr((/ 0.to.n1, 0.to.n2, 0.to.n3 /),id='work%y_c')

      work%y_f = f_malloc0_ptr((/ 1.to.7, nfl1.to.nfu1, nfl2.to.nfu2, nfl3.to.nfu3 /),id='work%y_f')

      i=max(n1,n2,n3)
      work%aeff0array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%aeff0array')
      work%beff0array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%beff0array')
      work%ceff0array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%ceff0array')
      work%eeff0array = f_malloc0_ptr((/ lowfil.to.lupfil, 0.to.i /),id='work%eeff0array')

      work%aeff0_2array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%aeff0_2array')
      work%beff0_2array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%beff0_2array')
      work%ceff0_2array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%ceff0_2array')
      work%eeff0_2array = f_malloc0_ptr((/ lowfil.to.lupfil, 0.to.i /),id='work%eeff0_2array')

      work%aeff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%aeff0_2auxarray')
      work%beff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%beff0_2auxarray')
      work%ceff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%ceff0_2auxarray')
      work%eeff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.i /),id='work%eeff0_2auxarray')

      work%xya_c = f_malloc_ptr((/ 0.to.n2, 0.to.n1, 0.to.n3 /),id='work%xya_c')
      work%xyc_c = f_malloc_ptr((/ 0.to.n2, 0.to.n1, 0.to.n3 /),id='work%xyc_c')
      if(with_confpot) then
         call f_zero(work%xya_c)
         call f_zero(work%xyc_c)
      end if

      work%xza_c = f_malloc_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%xza_c')
      work%xzc_c = f_malloc_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%xzc_c')
      if(with_confpot) then
         call f_zero(work%xza_c)
         call f_zero(work%xzc_c)
      end if

      work%yza_c = f_malloc_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%yza_c')
      work%yzb_c = f_malloc_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%yzb_c')
      work%yzc_c = f_malloc_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%yzc_c')
      work%yze_c = f_malloc_ptr((/ 0.to.n3, 0.to.n1, 0.to.n2 /),id='work%yze_c')
      if(with_confpot) then
         call f_zero(work%yza_c)
         call f_zero(work%yzb_c)
         call f_zero(work%yzc_c)
         call f_zero(work%yze_c)
      end if

      work%xya_f = f_malloc_ptr((/ 1.to.3, nfl2.to.nfu2, nfl1.to.nfu1, nfl3.to.nfu3 /),id='work%xya_f')
      work%xyb_f = f_malloc_ptr((/ 1.to.4, nfl2.to.nfu2, nfl1.to.nfu1, nfl3.to.nfu3 /),id='work%xyb_f')
      work%xyc_f = f_malloc_ptr((/ 1.to.3, nfl2.to.nfu2, nfl1.to.nfu1, nfl3.to.nfu3 /),id='work%xyc_f')
      work%xye_f = f_malloc_ptr((/ 1.to.4, nfl2.to.nfu2, nfl1.to.nfu1, nfl3.to.nfu3 /),id='work%xye_f')
      if(with_confpot) then
         call f_zero(work%xya_f)
         call f_zero(work%xyb_f)
         call f_zero(work%xyc_f)
         call f_zero(work%xye_f)
      end if

      work%xza_f = f_malloc_ptr((/ 1.to.3, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%xza_f')
      work%xzb_f = f_malloc_ptr((/ 1.to.4, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%xzb_f')
      work%xzc_f = f_malloc_ptr((/ 1.to.3, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%xzc_f')
      work%xze_f = f_malloc_ptr((/ 1.to.4, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%xze_f')
      if(with_confpot) then
         call f_zero(work%xza_f)
         call f_zero(work%xzb_f)
         call f_zero(work%xzc_f)
         call f_zero(work%xze_f)
      end if

      work%yza_f = f_malloc_ptr((/ 1.to.3, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%yza_f')
      work%yzb_f = f_malloc_ptr((/ 1.to.4, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%yzb_f')
      work%yzc_f = f_malloc_ptr((/ 1.to.3, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%yzc_f')
      work%yze_f = f_malloc_ptr((/ 1.to.4, nfl3.to.nfu3, nfl1.to.nfu1, nfl2.to.nfu2 /),id='work%yze_f')
      if(with_confpot) then
         call f_zero(work%yza_f)
         call f_zero(work%yzb_f)
         call f_zero(work%yzc_f)
         call f_zero(work%yze_f)
      end if


    END SUBROUTINE init_local_work_arrays


    subroutine zero_local_work_arrays(n1, n2, n3, with_confpot, work)
      implicit none

      ! Calling arguments
      integer,intent(in)::n1, n2, n3
      logical,intent(in):: with_confpot
      type(workarrays_quartic_convolutions),intent(inout):: work

      ! Local variables
      integer:: i
      integer,parameter :: lowfil=-14,lupfil=14

      call f_routine(id='zero_local_work_arrays')

      call f_zero(work%xx_c)
      call f_zero(work%xy_c)
      call f_zero(work%xz_c)

      call f_zero(work%xx_f1)
      call f_zero(work%xx_f)


      call f_zero(work%xy_f2)
      call f_zero(work%xy_f)


      call f_zero(work%xz_f4)
      call f_zero(work%xz_f)


      call f_zero(work%y_c)

      call f_zero(work%y_f)

      i=max(n1,n2,n3)
      call f_zero(work%aeff0array)
      call f_zero(work%beff0array)
      call f_zero(work%ceff0array)
      call f_zero(work%eeff0array)

      call f_zero(work%aeff0_2array)
      call f_zero(work%beff0_2array)
      call f_zero(work%ceff0_2array)
      call f_zero(work%eeff0_2array)

      call f_zero(work%aeff0_2auxarray)
      call f_zero(work%beff0_2auxarray)
      call f_zero(work%ceff0_2auxarray)
      call f_zero(work%eeff0_2auxarray)

      if(with_confpot) then
         call f_zero(work%xya_c)
         call f_zero(work%xyc_c)
      end if

      if(with_confpot) then
         call f_zero(work%xza_c)
         call f_zero(work%xzc_c)
      end if

      if(with_confpot) then
         call f_zero(work%yza_c)
         call f_zero(work%yzb_c)
         call f_zero(work%yzc_c)
         call f_zero(work%yze_c)
      end if

      if(with_confpot) then
         call f_zero(work%xya_f)
         call f_zero(work%xyb_f)
         call f_zero(work%xyc_f)
         call f_zero(work%xye_f)
      end if

      if(with_confpot) then
         call f_zero(work%xza_f)
         call f_zero(work%xzb_f)
         call f_zero(work%xzc_f)
         call f_zero(work%xze_f)
      end if

      if(with_confpot) then
         call f_zero(work%yza_f)
         call f_zero(work%yzb_f)
         call f_zero(work%yzc_f)
         call f_zero(work%yze_f)
      end if

      call f_release_routine()

    END SUBROUTINE zero_local_work_arrays

    !> Tranform wavefunction between localisation region and the global region
    !!!!!#######!> This routine only works if both locregs have free boundary conditions.
    !! @warning
    !! WARNING: Make sure psi is set to zero where Glr does not collide with Llr (or everywhere)
    subroutine Lpsi_to_global2(ldim, gdim, norb, nspin, Glr, Llr, lpsi, psi)
     implicit none

      ! Subroutine Scalar Arguments
      integer :: Gdim          ! dimension of psi
      integer :: Ldim          ! dimension of lpsi
      integer :: norb          ! number of orbitals
      integer :: nspin         ! number of spins
      type(locreg_descriptors),intent(in) :: Glr  ! Global grid descriptor
      type(locreg_descriptors), intent(in) :: Llr  ! Localization grid descriptors

      !Subroutine Array Arguments
      real(wp),dimension(Gdim),intent(inout) :: psi       !Wavefunction (compressed format)
      real(wp),dimension(Ldim),intent(in) :: lpsi         !Wavefunction in localization region

      !local variables
      integer :: igrid,isegloc,isegG,ix!,iorbs
      integer :: lmin,lmax,Gmin,Gmax
      integer :: icheck      ! check to make sure the dimension of loc_psi does not overflow
      integer :: offset      ! gives the difference between the starting point of Lseg and Gseg
      integer :: length      ! Length of the overlap between Lseg and Gseg
      integer :: lincrement  ! Increment for writing orbitals in loc_psi
      integer :: Gincrement  ! Increment for reading orbitals in psi
      integer :: nseg        ! total number of segments in Llr
      !integer, allocatable :: keymask(:,:)  ! shift for every segment of Llr (with respect to Glr)
      character(len=*), parameter :: subname='Lpsi_to_global'
      integer :: start,Gstart,Lindex
      integer :: lfinc,Gfinc,spinshift,ispin,Gindex,isegstart
      integer :: istart
      !integer :: i_stat

      call f_routine(id=subname)

      !!! This routine is only intended for conversions between locregs with the same boundary conditions.
      !!if (glr%geocode/= 'F' .or. llr%geocode/='F') then
      !!    call f_err_throw('Lpsi_to_global2 can only be used for locregs with free boundary conditions', &
      !!         err_name='BIGDFT_RUNTIME_ERROR')
      !!end if

      if(nspin/=1) stop 'not fully implemented for nspin/=1!'

    ! Define integers
      nseg = Llr%wfd%nseg_c + Llr%wfd%nseg_f
      lincrement = array_dim(Llr)
      Gincrement = array_dim(Glr)
      icheck = 0
      spinshift = Gdim / nspin

    ! Get the keymask: shift for every segment of Llr (with respect to Glr)
    ! allocate(keymask(2,nseg),stat=i_stat)
      !keymask = f_malloc((/2,nseg/),id='keymask')

      !call shift_locreg_indexes(Glr,Llr,keymask,nseg)
      !call shift_locreg_indexes_global(Glr,Llr,keymask,nseg)
      !!keymask = llr%wfd%keyglob

    !####################################################
    ! Do coarse region
    !####################################################
      isegstart=1


      !$omp parallel default(private) &
      !$omp shared(Glr,Llr, lpsi,icheck,psi,norb) &
      !$omp firstprivate(isegstart,nseg,lincrement,Gincrement,spinshift,nspin)

      !$omp do reduction(+:icheck)
      local_loop_c: do isegloc = 1,Llr%wfd%nseg_c
         lmin = llr%wfd%keyglob(1,isegloc)
         lmax = llr%wfd%keyglob(2,isegloc)
         istart = llr%wfd%keyvglob(isegloc)-1


         global_loop_c: do isegG = isegstart,Glr%wfd%nseg_c
            Gmin = Glr%wfd%keyglob(1,isegG)
            Gmax = Glr%wfd%keyglob(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            !if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax) exit global_loop_c

            !if((lmin > Gmax) .or. (lmax < Gmin))  cycle global_loop_c
            if(lmin > Gmax)  cycle global_loop_c

            ! Define the offset between the two segments
            offset = lmin - Gmin
            if(offset < 0) then
               offset = 0
            end if

            ! Define the length of the two segments
            length = min(lmax,Gmax)-max(lmin,Gmin)

            !Find the common elements and write them to the new global wavefunction
            icheck = icheck + (length + 1)

            ! WARNING: index goes from 0 to length because it is the offset of the element

            do ix = 0,length
               istart = istart + 1
               do ispin=1,nspin
                  Gindex = Glr%wfd%keyvglob(isegG)+offset+ix+spinshift*(ispin-1)
                  Lindex = istart+lincrement*norb*(ispin-1)
                  psi(Gindex) = lpsi(Lindex)
               end do
            end do
         end do global_loop_c
      end do local_loop_c
      !$omp end do


    !##############################################################
    ! Now do fine region
    !##############################################################

      start = Llr%wfd%nvctr_c
      Gstart = Glr%wfd%nvctr_c
      lfinc  = Llr%wfd%nvctr_f
      Gfinc = Glr%wfd%nvctr_f

      isegstart=Glr%wfd%nseg_c+1

      !$omp do reduction(+:icheck)
      local_loop_f: do isegloc = Llr%wfd%nseg_c+1,nseg
         lmin = llr%wfd%keyglob(1,isegloc)
         lmax = llr%wfd%keyglob(2,isegloc)
         istart = llr%wfd%keyvglob(isegloc)-1

         global_loop_f: do isegG = isegstart,Glr%wfd%nseg_c+Glr%wfd%nseg_f

            Gmin = Glr%wfd%keyglob(1,isegG)
            Gmax = Glr%wfd%keyglob(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            ! if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax)  exit global_loop_f
            !if((lmin > Gmax) .or. (lmax < Gmin))  cycle global_loop_f
            if(lmin > Gmax)  cycle global_loop_f
            offset = lmin - Gmin
            if(offset < 0) offset = 0

            length = min(lmax,Gmax)-max(lmin,Gmin)

            !Find the common elements and write them to the new global wavefunction
            ! First set to zero those elements which are not copied. WARNING: will not work for npsin>1!!

            icheck = icheck + (length + 1)

            ! WARNING: index goes from 0 to length because it is the offset of the element
            do ix = 0,length
            istart = istart + 1
               do igrid=1,7
                  do ispin = 1, nspin
                     Gindex = Gstart + (Glr%wfd%keyvglob(isegG)+offset+ix-1)*7+igrid + spinshift*(ispin-1)
                     Lindex = start+(istart-1)*7+igrid + lincrement*norb*(ispin-1)
                     psi(Gindex) = lpsi(Lindex)
                  end do
               end do
            end do
         end do global_loop_f
      end do local_loop_f
      !$omp end do

      !$omp end parallel

      !Check if the number of elements in loc_psi is valid
      if(icheck .ne. Llr%wfd%nvctr_f+Llr%wfd%nvctr_c) then
        write(*,*)'There is an error in Lpsi_to_global2: sum of fine and coarse points used',icheck
        write(*,*)'is not equal to the sum of fine and coarse points in the region',Llr%wfd%nvctr_f+Llr%wfd%nvctr_c
        stop
      end if

      !!call f_free(keymask)

      call f_release_routine()

    END SUBROUTINE Lpsi_to_global2


    !> Projects a quantity stored with the global indexes (i1,i2,i3) within the localisation region.
    !! @warning
    !!    The quantity must not be stored in a compressed form.
    subroutine global_to_local_parallel(Glr,Llr,size_rho,size_Lrho,rho,Lrho,i1s,i1e,i2s,i2e,i3s,i3e,ni1,ni2, &
               i1shift, i2shift, i3shift, ise)
     implicit none

     ! Arguments
     type(locreg_descriptors),intent(in) :: Llr   !< Local localization region
     type(locreg_descriptors),intent(in) :: Glr   !< Global localization region
     integer, intent(in) :: size_rho  ! size of rho array
     integer, intent(in) :: size_Lrho ! size of Lrho array
     real(wp),dimension(size_rho),intent(in) :: rho  !< quantity in global region
     real(wp),dimension(size_Lrho),intent(out) :: Lrho !< piece of quantity in local region
     integer,intent(in) :: i1s, i1e, i2s, i2e
     integer,intent(in) :: i3s, i3e ! starting and ending indices on z direction (related to distribution of rho when parallel)
     integer,intent(in) :: ni1, ni2 ! x and y extent of rho
     integer,intent(in) :: i1shift, i2shift, i3shift
     integer,dimension(6) :: ise

    ! Local variable
     integer :: ispin,ii1,ii2,ii3  !integer for loops
     !integer :: i1,i2,i3
     integer :: indSmall, indSpin, indLarge ! indexes for the arrays
     integer :: ist2S,ist3S, ist2L, ist3L, istsa, ists, istl
     integer :: ii1shift, ii2shift, ii3shift, i1glob, i2glob, i3glob
     integer :: iii1, iii2, iii3

     call f_routine(id='global_to_local_parallel')

     !THIS ROUTINE NEEDS OPTIMIZING

     !!write(*,'(a,8i8)') 'in global_to_local_parallel: i1s, i1e, i2s, i2e, i3s, i3e, ni1, ni2', i1s, i1e, i2s, i2e, i3s, i3e, ni1, ni2

     ! Cut out a piece of the quantity (rho) from the global region (rho) and
     ! store it in a local region (Lrho).
     indSmall=0
     indSpin=0
     ! Deactivate the spin for the moment
     do ispin=1,1!nspin
         !$omp parallel default(none) &
         !$omp shared(Glr, Llr, Lrho, rho, indSpin, i1s, i1e, i2s, i2e, i3s, i3e) &
         !$omp shared(i1shift, i2shift, i3shift, ni1, ni2, ise) &
         !$omp private(ii1, ii2, ii3, i1glob, i2glob, i3glob, ii1shift, ii2shift, ii3shift) &
         !$omp private(ist3S, ist3L, istsa, ist2S, ist2L, ists, istl, indSmall, indLarge) &
         !$omp private(iii1, iii2, iii3)
         !$omp do
         do ii3=i3s,i3e
             i3glob = ii3+ise(5)-1
             !i3=modulo(i3glob-1,glr%d%n3i)+1
             if (modulo(ii3-1,glr%mesh%ndims(3))+1>modulo(i3e-1,glr%mesh%ndims(3))+1) then
                 !This is a line before the wrap around, i.e. one needs a shift since
                 ii3shift = i3shift
             else
                 ii3shift = 0
             end if
             if (i3glob<=glr%mesh%ndims(3)) then
                 iii3=ii3+i3shift
             else
                 iii3=modulo(i3glob-1,glr%mesh%ndims(3))+1
             end if
             ist3S = (ii3-i3s)*Llr%mesh%ndims(2)*Llr%mesh%ndims(1)
             ist3L = (iii3-1)*ni2*ni1
             istsa=ist3S-i1s+1
             do ii2=i2s,i2e
                 i2glob = ii2+ise(3)-1
                 !i2=modulo(i2glob-1,glr%d%n2i)+1
                 if (modulo(ii2-1,glr%mesh%ndims(2))+1>modulo(i2e-1,glr%mesh%ndims(2))+1) then
                     !This is a line before the wrap around, i.e. one needs a shift since
                     !the potential in the global region starts with the wrapped around part
                     ii2shift = i2shift
                 else
                     ii2shift = 0
                 end if
                 if (i2glob<=glr%mesh%ndims(2)) then
                     iii2=ii2+i2shift
                 else
                     iii2=modulo(i2glob-1,glr%mesh%ndims(2))+1
                 end if
                 ist2S = (ii2-i2s)*Llr%mesh%ndims(1)
                 ist2L = (iii2-1)*ni1
                 ists=istsa+ist2S
                 istl=ist3L+ist2L
                 do ii1=i1s,i1e
                     i1glob = ii1+ise(1)-1
                     !i1=modulo(i1glob-1,glr%d%n1i)+1
                     if (modulo(ii1-1,glr%mesh%ndims(1))+1>modulo(i1e-1,glr%mesh%ndims(1))+1) then
                         !This is a line before the wrap around, i.e. one needs a shift since
                         !the potential in the global region starts with the wrapped around part
                         ii1shift = i1shift
                     else
                         ii1shift = 0
                     end if
                     if (i1glob<=glr%mesh%ndims(1)) then
                         iii1=ii1+i1shift
                     else
                         iii1=modulo(i1glob-1,glr%mesh%ndims(1))+1
                     end if
                     ! indSmall is the index in the local localization region
                     indSmall=ists+ii1
                     ! indLarge is the index in the global localization region.
                     indLarge= iii1+istl
                     Lrho(indSmall)=rho(indLarge+indSpin)
                     !write(600+bigdft_mpi%iproc,'(a,14i7,2es16.8)') 'i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, i1shift, i2shift, i3shift, indsmall, indlarge, val, testval', &
                     !    i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, i1shift, i2shift, i3shift, indsmall, indlarge, Lrho(indSmall), real((i1+(i2-1)*glr%d%n1i+(i3-1)*glr%d%n1i*glr%d%n2i),kind=8)
                     !if (abs(Lrho(indSmall)-real((i1+(i2-1)*glr%d%n1i+(i3-1)*glr%d%n1i*glr%d%n2i),kind=8))>1.d-3) then
                     !    write(700+bigdft_mpi%iproc,'(a,11i7,2es16.8)') 'i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, indsmall, indlarge, val, testval', &
                     !        i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, indsmall, indlarge, Lrho(indSmall), real((i1+(i2-1)*glr%d%n1i+(i3-1)*glr%d%n1i*glr%d%n2i),kind=8)
                     !end if
                 end do
             end do
         end do
         !$omp end do
         !$omp end parallel
         indSpin=indSpin+int(Glr%mesh%ndim)
     end do

     call f_release_routine()

    END SUBROUTINE global_to_local_parallel

    function boundary_weight(hgrids,glr,lr,rad,psi) result(weight_normalized)
      use at_domain, only: domain_periodic_dims
      use compression
      implicit none
      real(gp), intent(in) :: rad
      real(gp), dimension(3) :: hgrids
      type(locreg_descriptors), intent(in) :: glr,lr
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      real(gp) :: weight_normalized
      !local variables
      integer :: iseg, jj, j0, j1, ii, i3, i2, i0, i1, i, ind
      integer :: ij3, ij2, ij1, jj3, jj2, jj1, ijs3, ijs2, ijs1, ije3, ije2, ije1
      !integer :: iorb, iiorb, ilr
      real(kind=8) :: h, x, y, z, d, weight_inside, weight_boundary, points_inside, points_boundary, ratio
      real(kind=8) :: boundary
      logical :: perx, pery, perz, on_boundary
      logical, dimension(3) :: peri


      ! mean value of the grid spacing
      h = sqrt(hgrids(1)**2+hgrids(2)**2+hgrids(3)**2)

      ! periodicity in the three directions
!!$      perx=(glr%geocode /= 'F')
!!$      pery=(glr%geocode == 'P')
!!$      perz=(glr%geocode /= 'F')
      peri=domain_periodic_dims(glr%mesh%dom)
      perx=peri(1)
      pery=peri(2)
      perz=peri(3)

      ! For perdiodic boundary conditions, one has to check also in the neighboring
      ! cells (see in the loop below)
      if (perx) then
         ijs1 = -1
         ije1 = 1
      else
         ijs1 = 0
         ije1 = 0
      end if
      if (pery) then
         ijs2 = -1
         ije2 = 1
      else
         ijs2 = 0
         ije2 = 0
      end if
      if (perz) then
         ijs3 = -1
         ije3 = 1
      else
         ijs3 = 0
         ije3 = 0
      end if


      boundary = min(rad,lr%locrad)

      weight_boundary = 0.d0
      weight_inside = 0.d0
      points_inside = 0.d0
      points_boundary = 0.d0
      ind = 0
      do iseg=1,lr%wfd%nseg_c
         jj=lr%wfd%keyvglob(iseg)
         j0=lr%wfd%keyglob(1,iseg)
         j1=lr%wfd%keyglob(2,iseg)
         ii=j0-1
         i3=ii/glr%mesh_coarse%ndims(1)/glr%mesh_coarse%ndims(2)
         ii=ii-i3*glr%mesh_coarse%ndims(1)*glr%mesh_coarse%ndims(2)
         i2=ii/glr%mesh_coarse%ndims(1)
         i0=ii-i2*glr%mesh_coarse%ndims(1)
         i1=i0+j1-j0
         do i=i0,i1
            ind = ind + 1
            on_boundary = .false.
            do ij3=ijs3,ije3!-1,1
               jj3=i3+ij3*glr%mesh_coarse%ndims(3)
               z = real(jj3,kind=8)*hgrids(3)
               do ij2=ijs2,ije2!-1,1
                  jj2=i2+ij2*glr%mesh_coarse%ndims(2)
                  y = real(jj2,kind=8)*hgrids(2)
                  do ij1=ijs1,ije1!-1,1
                     jj1=i+ij1*glr%mesh_coarse%ndims(1)
                     x = real(i,kind=8)*hgrids(1)
                     d = sqrt((x-lr%locregcenter(1))**2 + &
                          (y-lr%locregcenter(2))**2 + &
                          (z-lr%locregcenter(3))**2)
                     if (abs(d-boundary)<h) then
                        on_boundary=.true.
                     end if
                  end do
               end do
            end do
            if (on_boundary) then
               ! This value is on the boundary
               !write(*,'(a,2f9.2,3i8,3es16.8)') 'on boundary: boundary, d, i1, i2, i3, x, y, z', &
               !    boundary, d, i, i2, i3, x, y, z
               weight_boundary = weight_boundary + psi(ind)**2
               points_boundary = points_boundary + 1.d0
            else
               weight_inside = weight_inside + psi(ind)**2
               points_inside = points_inside + 1.d0
            end if
         end do
      end do
      ! fine part, to be done only if nseg_f is nonzero
      do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
         jj=lr%wfd%keyvglob(iseg)
         j0=lr%wfd%keyglob(1,iseg)
         j1=lr%wfd%keyglob(2,iseg)
         ii=j0-1
         i3=ii/glr%mesh_coarse%ndims(1)/glr%mesh_coarse%ndims(2)
         ii=ii-i3*glr%mesh_coarse%ndims(1)*glr%mesh_coarse%ndims(2)
         i2=ii/glr%mesh_coarse%ndims(1)
         i0=ii-i2*glr%mesh_coarse%ndims(1)
         i1=i0+j1-j0
         do i=i0,i1
            ind = ind + 7
            on_boundary = .false.
            do ij3=ijs3,ije3!-1,1
               jj3=i3+ij3*glr%mesh_coarse%ndims(3)
               z = real(jj3,kind=8)*hgrids(3)
               do ij2=ijs2,ije2!-1,1
                  jj2=i2+ij2*glr%mesh_coarse%ndims(2)
                  y = real(jj2,kind=8)*hgrids(2)
                  do ij1=ijs1,ije1!-1,1
                     jj1=i+ij1*glr%mesh_coarse%ndims(1)
                     x = real(i,kind=8)*hgrids(1)
                     d = sqrt((x-lr%locregcenter(1))**2 + &
                          (y-lr%locregcenter(2))**2 + &
                          (z-lr%locregcenter(3))**2)
                     if (abs(d-boundary)<h) then
                        on_boundary=.true.
                     end if
                  end do
               end do
            end do
            if (on_boundary) then
               ! This value is on the boundary
               !write(*,'(a,f9.2,3i8,3es16.8)') 'on boundary: d, i1, i2, i3, x, y, z', d, i, i2, i3, x, y, z
               weight_boundary = weight_boundary + psi(ind-6)**2
               weight_boundary = weight_boundary + psi(ind-5)**2
               weight_boundary = weight_boundary + psi(ind-4)**2
               weight_boundary = weight_boundary + psi(ind-3)**2
               weight_boundary = weight_boundary + psi(ind-2)**2
               weight_boundary = weight_boundary + psi(ind-1)**2
               weight_boundary = weight_boundary + psi(ind-0)**2
               points_boundary = points_boundary + 7.d0
            else
               weight_inside = weight_inside + psi(ind-6)**2
               weight_inside = weight_inside + psi(ind-5)**2
               weight_inside = weight_inside + psi(ind-4)**2
               weight_inside = weight_inside + psi(ind-3)**2
               weight_inside = weight_inside + psi(ind-2)**2
               weight_inside = weight_inside + psi(ind-1)**2
               weight_inside = weight_inside + psi(ind-0)**2
               points_inside = points_inside + 7.d0
            end if
         end do
      end do
      ! Ratio of the points on the boundary with resepct to the total number of points
      ratio = points_boundary/(points_boundary+points_inside)
      weight_normalized = weight_boundary/ratio
      !write(*,'(a,2f9.1,4es16.6)') 'iiorb, pi, pb, weight_inside, weight_boundary, ratio, xi', &
      !    points_inside, points_boundary, weight_inside, weight_boundary, &
      !    points_boundary/(points_boundary+points_inside), &
      !    weight_boundary/ratio

    end function boundary_weight

    !> Tranform one wavefunction between Global region and localisation region
    subroutine psi_to_locreg2(iproc, ldim, gdim, Llr, Glr, gpsi, lpsi)

     implicit none

      ! Subroutine Scalar Arguments
      integer,intent(in) :: iproc                  ! process ID
      integer,intent(in) :: ldim          ! dimension of lpsi
      integer,intent(in) :: gdim          ! dimension of gpsi
      type(locreg_descriptors),intent(in) :: Llr  ! Local grid descriptor
      type(locreg_descriptors),intent(in) :: Glr  ! Global grid descriptor

      !Subroutine Array Arguments
      real(wp),dimension(gdim),intent(in) :: gpsi       !Wavefunction (compressed format)
      real(wp),dimension(ldim),intent(out) :: lpsi   !Wavefunction in localization region

      !local variables
      integer :: igrid,isegloc,isegG,ix!,iorbs
      integer :: lmin,lmax,Gmin,Gmax
      integer :: icheck      ! check to make sure the dimension of loc_psi does not overflow
      integer :: offset      ! gives the difference between the starting point of Lseg and Gseg
      integer :: length      ! Length of the overlap between Lseg and Gseg
      integer :: lincrement  ! Increment for writing orbitals in loc_psi
      integer :: Gincrement  ! Increment for reading orbitals in psi
      integer :: nseg        ! total number of segments in Llr
      integer, allocatable :: keymask(:,:)  ! shift for every segment of Llr (with respect to Glr)
      character(len=*), parameter :: subname='psi_to_locreg'
    !  integer :: i_stat,i_all
      integer :: start,Gstart
      integer :: isegstart,istart

      call f_routine(id=subname)

    ! Define integers
      nseg = Llr%wfd%nseg_c + Llr%wfd%nseg_f
      lincrement = array_dim(Llr)
      Gincrement = array_dim(Glr)
      icheck = 0

    ! Initialize loc_psi
      call f_zero(lpsi)

    ! Get the keymask: shift for every segment of Llr (with respect to Glr)
    ! allocate(keymask(2,nseg),stat=i_stat)
      keymask = f_malloc((/ 2, nseg /),id='keymask')

      call shift_locreg_indexes(Glr,Llr,keymask,nseg)


    !####################################################
    ! Do coarse region
    !####################################################
      isegstart=1
      icheck = 0


    !$omp parallel default(private) &
    !$omp shared(icheck,lpsi,gpsi,Glr,Llr,keymask,lincrement,Gincrement,Gstart) &
    !$omp firstprivate(isegstart,nseg)

      !$omp do reduction(+:icheck)
      local_loop_c: do isegloc = 1,Llr%wfd%nseg_c
         lmin = keymask(1,isegloc)
         lmax = keymask(2,isegloc)
         istart = llr%wfd%keyvloc(isegloc)-1

         global_loop_c: do isegG = isegstart,Glr%wfd%nseg_c
            Gmin = Glr%wfd%keygloc(1,isegG)
            Gmax = Glr%wfd%keygloc(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            ! if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax) exit global_loop_c
            if((lmin > Gmax) .or. (lmax < Gmin)) cycle global_loop_c

            ! Define the offset between the two segments
            offset = lmin - Gmin
            if(offset < 0) then
               offset = 0
            end if

            ! Define the length of the two segments
            length = min(lmax,Gmax)-max(lmin,Gmin)

            icheck = icheck + (length + 1)

            !Find the common elements and write them to the new localized wavefunction
            ! WARNING: index goes from 0 to length because it is the offset of the element

            do ix = 0,length
               istart = istart + 1
               lpsi(istart) = gpsi(Glr%wfd%keyvloc(isegG)+offset+ix)
            end do
         end do global_loop_c
      end do local_loop_c
      !$omp end do

    ! Check if the number of elements in loc_psi is valid
     ! if(icheck .ne. Llr%wfd%nvctr_c) then
       ! write(*,*)'There is an error in psi_to_locreg2: number of coarse points used',icheck
       ! write(*,*)'is not equal to the number of coarse points in the region',Llr%wfd%nvctr_c
     ! end if

    !##############################################################
    ! Now do fine region
    !##############################################################

      !icheck = 0
      start = Llr%wfd%nvctr_c
      Gstart = Glr%wfd%nvctr_c

      isegstart=Glr%wfd%nseg_c+1

      !$omp do reduction(+:icheck)
      local_loop_f: do isegloc = Llr%wfd%nseg_c+1,nseg
         lmin = keymask(1,isegloc)
         lmax = keymask(2,isegloc)
         istart = llr%wfd%keyvloc(isegloc)-1

         global_loop_f: do isegG = isegstart,Glr%wfd%nseg_c+Glr%wfd%nseg_f

            Gmin = Glr%wfd%keygloc(1,isegG)
            Gmax = Glr%wfd%keygloc(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            ! if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax)  exit global_loop_f
            if((lmin > Gmax) .or. (lmax < Gmin))  cycle global_loop_f

            offset = lmin - Gmin
            if(offset < 0) offset = 0

            length = min(lmax,Gmax)-max(lmin,Gmin)

            icheck = icheck + (length + 1)

            !Find the common elements and write them to the new localized wavefunction
            ! WARNING: index goes from 0 to length because it is the offset of the element
            do ix = 0,length
               istart = istart+1
               do igrid=1,7
                  lpsi(start+(istart-1)*7+igrid) = gpsi(Gstart+(Glr%wfd%keyvloc(isegG)+offset+ix-1)*7+igrid)
               end do
            end do
         end do global_loop_f
      end do local_loop_f
      !$omp end do

      !$omp end parallel

     !! Check if the number of elements in loc_psi is valid
      if(icheck .ne. Llr%wfd%nvctr_f+Llr%wfd%nvctr_c) then
        write(*,'(a,i0,a,i0)')'process ',iproc,': There is an error in psi_to_locreg: number of fine points used ',icheck
        write(*,'(a,i0)')'is not equal to the number of fine points in the region ',Llr%wfd%nvctr_f+Llr%wfd%nvctr_c
      end if



    !  i_all=-product(shape(keymask))*kind(keymask)
    ! deallocate(keymask,stat=i_stat)
      call f_free(keymask)
      call f_release_routine()

    END SUBROUTINE psi_to_locreg2


    !> Find the shift necessary for the indexes of every segment of Blr
    !!   to make them compatible with the indexes of Alr. These shifts are
    !!   returned in the array keymask(nseg), where nseg should be the number
    !!   of segments in Blr.
    !! @warning
    !!   This routine supposes that the region Blr is contained in the region Alr.
    !!   This should always be the case, if we concentrate on the overlap between two regions.
    subroutine shift_locreg_indexes(Alr,Blr,keymask,nseg)
     use at_domain, only: domain_geocode
     use dictionaries, only: f_err_throw
     implicit none

    ! Arguments
     type(locreg_descriptors),intent(in) :: Alr,Blr   ! The two localization regions
     integer,intent(in) :: nseg
     integer,intent(out) :: keymask(2,nseg)

    ! Local variable
     integer :: iseg      !integer for the loop
     integer :: Bindex    !starting index of segments in Blr
     integer :: x,y,z     !coordinates of start of segments in Blr
     integer :: shift(3)  !shift between the beginning of the segment in Blr and the origin of Alr
     integer ::  tmp


     ! This routine is only intended for conversions between locregs with the same boundary conditions.
!!$     if (blr%geocode/='F') then
     if (domain_geocode(blr%mesh%dom) /= 'F') then
         call f_err_throw('shift_locreg_indexes can only be used for locregs with free boundary conditions', &
              err_name='BIGDFT_RUNTIME_ERROR')
     end if

    !Big loop on all segments
    !$omp parallel do default(private) shared(Blr,nseg,Alr,keymask)
     do iseg=1,nseg

    !##########################################
    ! For the Starting index
        Bindex = Blr%wfd%keygloc(1,iseg)
        tmp = Bindex -1
        z   = tmp / Blr%mesh_coarse%ndims(2) / Blr%mesh_coarse%ndims(1)
        tmp = tmp - z*Blr%mesh_coarse%ndims(2)*Blr%mesh_coarse%ndims(1)
        y   = tmp / Blr%mesh_coarse%ndims(1)
        x   = tmp - y * Blr%mesh_coarse%ndims(1)

    ! Shift between the beginning of the segment and the start of the Alr region
        shift(1) = x + Blr%nboxc(1,1) - Alr%nboxc(1,1)
        shift(2) = y + Blr%nboxc(1,2) - Alr%nboxc(1,2)
        shift(3) = z + Blr%nboxc(1,3) - Alr%nboxc(1,3)

    ! Write the shift in index form
        keymask(1,iseg) = shift(3)*Alr%mesh_coarse%ndims(1)*Alr%mesh_coarse%ndims(2) + &
             shift(2)*Alr%mesh_coarse%ndims(1) + shift(1) + 1

    !######################################
    ! For the ending index

        Bindex = Blr%wfd%keygloc(2,iseg)
        tmp = Bindex -1
        z   = tmp / Blr%mesh_coarse%ndims(2) / Blr%mesh_coarse%ndims(1)
        tmp = tmp - z*Blr%mesh_coarse%ndims(2)*Blr%mesh_coarse%ndims(1)
        y   = tmp / Blr%mesh_coarse%ndims(1)
        x   = tmp - y * Blr%mesh_coarse%ndims(1)

    ! Shift between the beginning of the segment and the start of the Alr region
        shift(1) = x + Blr%nboxc(1,1) - Alr%nboxc(1,1)
        shift(2) = y + Blr%nboxc(1,2) - Alr%nboxc(1,2)
        shift(3) = z + Blr%nboxc(1,3) - Alr%nboxc(1,3)

    ! Write the shift in index form
        keymask(2,iseg) = shift(3)*Alr%mesh_coarse%ndims(1)*Alr%mesh_coarse%ndims(2) + &
             shift(2)*Alr%mesh_coarse%ndims(1) + shift(1) + 1
     end do
    !$omp end parallel do

    END SUBROUTINE shift_locreg_indexes

    !> Projects a quantity stored with the global indexes (i1,i2,i3) within the localisation region.
    !! @warning: The quantity must not be stored in a compressed form.
    subroutine global_to_local(Glr,Llr,nspin,size_rho,size_Lrho,rho,Lrho)
     use at_domain, only: domain_geocode

     implicit none

    ! Arguments
     type(locreg_descriptors),intent(in) :: Llr   ! Local localization region
     type(locreg_descriptors),intent(in) :: Glr   ! Global localization region
     integer, intent(in) :: size_rho  ! size of rho array
     integer, intent(in) :: size_Lrho ! size of Lrho array
     integer, intent(in) :: nspin  !number of spins
     real(wp),dimension(size_rho),intent(in) :: rho  ! quantity in global region
     real(wp),dimension(size_Lrho),intent(out) :: Lrho ! piece of quantity in local region

    ! Local variable
     integer :: ispin,i1,i2,i3,ii1,ii2,ii3  !integer for loops
     integer :: indSmall, indSpin, indLarge ! indexes for the arrays
     logical:: z_inside, y_inside, x_inside
     integer:: iz, iy, m

    ! Cut out a piece of the quantity (rho) from the global region (rho) and
    ! store it in a local region (Lrho).

!!$     if(Glr%geocode == 'F') then
     if(domain_geocode(Glr%mesh%dom) == 'F') then
         ! Use loop unrolling here
         indSmall=0
         indSpin=0
         do ispin=1,nspin
             ! WARNING: I added the factors 2.
             do i3=Llr%nboxi(1,3)+1,Llr%nboxi(2,3)
                 iz=(i3-1)*Glr%mesh%ndims(2)*Glr%mesh%ndims(1)
                 do i2=Llr%nboxi(1,2)+1,Llr%nboxi(2,2)
                     iy=(i2-1)*Glr%mesh%ndims(1)
                     m=mod(Llr%nboxi(2,1)-Llr%nboxi(1,1),4)
                     if(m/=0) then
                         do i1=Llr%nboxi(1,1)+1,Llr%nboxi(1,1)+m
                            indSmall=indSmall+1
                            indLarge=iz+iy+i1
                            Lrho(indSmall)=rho(indLarge+indSpin)
                         end do
                      end if
                      do i1=Llr%nboxi(1,1)+1+m,Llr%nboxi(2,1),4
                         Lrho(indSmall+1)=rho(iz+iy+i1+0+indSpin)
                         Lrho(indSmall+2)=rho(iz+iy+i1+1+indSpin)
                         Lrho(indSmall+3)=rho(iz+iy+i1+2+indSpin)
                         Lrho(indSmall+4)=rho(iz+iy+i1+3+indSpin)
                         indSmall=indSmall+4
                      end do
                 end do
             end do
             indSpin=indSpin+int(Glr%mesh%ndim)
          end do
     else
         ! General case
         indSmall=0
         indSpin=0
         do ispin=1,nspin
             ! WARNING: I added the factors 2.
             do ii3=Llr%nboxi(1,3)+1,Llr%nboxi(2,3)
                 i3 = mod(ii3-1,Glr%mesh%ndims(3))+1
                 z_inside = (i3>0 .and. i3<=Glr%mesh%ndims(3)+1)
                 iz=(i3-1)*Glr%mesh%ndims(2)*Glr%mesh%ndims(1)
                 do ii2=Llr%nboxi(1,2)+1,Llr%nboxi(2,2)
                     i2 = mod(ii2-1,Glr%mesh%ndims(2))+1
                     y_inside = (i2>0 .and. i2<=Glr%mesh%ndims(2)+1)
                     iy=(i2-1)*Glr%mesh%ndims(1)
                     do ii1=Llr%nboxi(1,1)+1,Llr%nboxi(2,1)
                         i1 = mod(ii1-1,Glr%mesh%ndims(1))+1
                         x_inside = (i1 > 0 .and. i1 <= Glr%mesh%ndims(1)+1)
                         ! indSmall is the index in the local localization region
                         indSmall=indSmall+1
                         !!if (i3 > 0 .and. i2 > 0 .and. i1 > 0 .and.&                                       !This initializes the buffers of locreg to zeros if outside the simulation box.
                         !!    i3 <= Glr%d%n3i+1 .and. i2 <= Glr%d%n2i+1 .and. i1 <= Glr%d%n1i+1) then       !Should use periodic image instead... MUST FIX THIS.
                         !!   ! indLarge is the index in the global localization region.
                         !!   indLarge=(i3-1)*Glr%d%n2i*Glr%d%n1i + (i2-1)*Glr%d%n1i + i1
                         if(z_inside .and. y_inside .and. x_inside) then
                            indLarge=iz+iy+i1
                            Lrho(indSmall)=rho(indLarge+indSpin)
                         else
                            Lrho(indSmall)= 0.0_wp
                         end if
                     end do
                 end do
             end do
             indSpin=indSpin+int(Glr%mesh%ndim)
         end do
     end if

    END SUBROUTINE global_to_local

    pure subroutine nullify_confpot_data(c)
      use f_precisions, only: UNINITIALIZED
      implicit none
      type(confpot_data), intent(out) :: c
      c%potorder=0
      !the rest is not useful
      c%prefac     =UNINITIALIZED(c%prefac)
      c%hh(1)      =UNINITIALIZED(c%hh(1))
      c%hh(2)      =UNINITIALIZED(c%hh(2))
      c%hh(3)      =UNINITIALIZED(c%hh(3))
      c%rxyzConf(1)=UNINITIALIZED(c%rxyzConf(1))
      c%rxyzConf(2)=UNINITIALIZED(c%rxyzConf(2))
      c%rxyzConf(3)=UNINITIALIZED(c%rxyzConf(3))
      c%ioffset(1) =UNINITIALIZED(c%ioffset(1))
      c%ioffset(2) =UNINITIALIZED(c%ioffset(2))
      c%ioffset(3) =UNINITIALIZED(c%ioffset(3))
      c%damping    =UNINITIALIZED(c%damping)

    end subroutine nullify_confpot_data

    !> apply the potential to the psir wavefunction and calculate potential energy
    subroutine psir_to_vpsi(npot,nspinor,lr,pot,vpsir,epot,confdata,vpsir_noconf,econf)
      use dynamic_memory
      use at_domain, only: domain_geocode
      implicit none
      integer, intent(in) :: npot,nspinor
      type(locreg_descriptors), intent(in) :: lr !< localization region of the wavefunction
      real(wp), dimension(lr%mesh%ndim,npot), intent(in) :: pot
      real(wp), dimension(lr%mesh%ndim,nspinor), intent(inout) :: vpsir
      real(gp), intent(out) :: epot
      type(confpot_data), intent(in), optional :: confdata !< data for the confining potential
      real(wp), dimension(lr%mesh%ndim,nspinor), intent(inout), optional :: vpsir_noconf !< wavefunction with  the potential without confinement applied
      real(gp), intent(out),optional :: econf !< confinement energy
      !local variables
      logical :: confining
      integer, dimension(3) :: ishift !temporary variable in view of wavefunction creation

      call f_routine(id='psir_to_vpsi')

      !write(*,'(a,a4,2l5)') 'in psir_to_vpsi: lr%geocode, present(vpsir_noconf), present(econf)', lr%geocode, present(vpsir_noconf), present(econf)

      epot=0.0_gp
      ishift=(/0,0,0/)
      confining=present(confdata)
      if (confining) confining= (confdata%potorder /=0)

      if (confining) then
!!$         if (lr%geocode == 'F') then
         if (domain_geocode(lr%mesh%dom) == 'F') then
            if (present(vpsir_noconf)) then
               if (.not.present(econf)) stop 'ERROR: econf must be present when vpsir_noconf is present!'
               !call apply_potential_lr(lr%d%n1i,lr%d%n2i,lr%d%n3i,&
               call apply_potential_lr_conf_noconf(lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    ishift,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    nspinor,npot,vpsir,pot,epot,&
                    confdata,lr%bounds%ibyyzz_r,vpsir_noconf,econf)
               !confdata=confdata,ibyyzz_r=lr%bounds%ibyyzz_r,psir_noconf=vpsir_noconf,econf=econf)
            else
               !call apply_potential_lr(lr%d%n1i,lr%d%n2i,lr%d%n3i,&
               call apply_potential_lr_conf(lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    ishift,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    nspinor,npot,vpsir,pot,epot,&
                    confdata,lr%bounds%ibyyzz_r)
               !confdata=confdata,ibyyzz_r=lr%bounds%ibyyzz_r)
            end if
         else
!!!if (present(vpsir_noconf)) then
!!!if (.not.present(econf)) stop 'ERROR: econf must be present when vpsir_noconf is present!'
!!!!call apply_potential_lr(lr%d%n1i,lr%d%n2i,lr%d%n3i,&
!!!    call apply_potential_lr_conf_noconf_nobounds(lr%d%n1i,lr%d%n2i,lr%d%n3i,&
!!!         lr%d%n1i,lr%d%n2i,lr%d%n3i,&
!!!         ishift,lr%d%n2,lr%d%n3,&
!!!         nspinor,npot,vpsir,pot,epot,&
!!!         confdata,vpsir_noconf,econf)
!!!         !confdata=confdata)
!!!else
            call apply_potential_lr_conf_nobounds(lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 ishift,&
                 nspinor,npot,vpsir,pot,epot,&
                 confdata)
            !confdata=confdata)
!!! end if
         end if

      else

!!$         if (lr%geocode == 'F') then
         if (domain_geocode(lr%mesh%dom) == 'F') then
            !call apply_potential_lr(lr%d%n1i,lr%d%n2i,lr%d%n3i,&
            call apply_potential_lr_bounds(lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 ishift,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 nspinor,npot,vpsir,pot,epot,&
                 lr%bounds%ibyyzz_r)
            !     ibyyzz_r=lr%bounds%ibyyzz_r)
         else
            !call apply_potential_lr(lr%d%n1i,lr%d%n2i,lr%d%n3i,&
            call apply_potential_lr_nobounds(lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 ishift,&
                 nspinor,npot,vpsir,pot,epot)
         end if
      end if

      call f_release_routine()

    end subroutine psir_to_vpsi

    subroutine isf_to_daub_kinetic(lr,kx,ky,kz,nspinor,w,psir,hpsi,ekin,k_strten)
      use at_domain, only: domain_geocode
      use liborbs_errors
      implicit none
      integer, intent(in) :: nspinor
      real(gp), intent(in) :: kx,ky,kz
      type(locreg_descriptors), intent(in) :: lr
      type(workarr_locham), intent(inout) :: w
      real(wp), dimension(lr%mesh%ndim,nspinor), intent(in) :: psir
      real(gp), intent(out) :: ekin
      real(wp), dimension(array_dim(lr),nspinor), intent(inout) :: hpsi
      real(wp), dimension(6), optional :: k_strten
      !Local variables
      logical :: usekpts
      integer :: idx,i,i_f,iseg_f,ipsif,isegf
      real(gp) :: ekino
      real(wp), dimension(0:3) :: scal
      real(wp), dimension(6) :: kstrten,kstrteno


      !control whether the k points are to be used
      !real k-point different from Gamma still not implemented
      usekpts = kx**2+ky**2+kz**2 > 0.0_gp .or. nspinor == 2

      do i=0,3
         scal(i)=1.0_wp
      enddo

      !starting point for the fine degrees, to avoid boundary problems
      i_f=min(1,lr%wfd%nvctr_f)
      iseg_f=min(1,lr%wfd%nseg_f)
      ipsif=lr%wfd%nvctr_c+i_f
      isegf=lr%wfd%nseg_c+iseg_f

      !call MPI_COMM_RANK(bigdft_mpi%mpi_comm,iproc,ierr)
      ekin=0.0_gp

      kstrten=0.0_wp
!!$      select case(lr%geocode)
      select case(domain_geocode(lr%mesh%dom))
      case('F')

         !here kpoints cannot be used (for the moment, to be activated for the
         !localisation region scheme
         if (f_err_raise(usekpts, 'K points not allowed for Free BC locham', &
              err_id = LIBORBS_OPERATION_ERROR())) return

         do idx=1,nspinor

            call comb_shrink(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
                 w%w1,w%w2,psir(1,idx),&
                 lr%bounds%kb%ibxy_c,lr%bounds%sb%ibzzx_c,lr%bounds%sb%ibyyzz_c,&
                 lr%bounds%sb%ibxy_ff,lr%bounds%sb%ibzzx_f,lr%bounds%sb%ibyyzz_f,&
                 w%y_c(1,idx),w%y_f(1,idx))

            call ConvolkineticT(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                 lr%mesh_coarse%hgrids(1),lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3),&
                 lr%bounds%kb%ibyz_c,lr%bounds%kb%ibxz_c,lr%bounds%kb%ibxy_c,&
                 lr%bounds%kb%ibyz_f,lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f, &
                 w%x_c(1,idx),w%x_f(1,idx),&
                 w%y_c(1,idx),w%y_f(1,idx),ekino, &
                 w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),111)
            ekin=ekin+ekino

            call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                 hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))
         end do

      case('S')

         if (usekpts) then
            !first calculate the proper arrays then transpose them before passing to the
            !proper routine
            do idx=1,nspinor
               call convolut_magic_t_slab_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))
            end do

            !Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%x_c,psir,.true.)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.true.)

            ! compute the kinetic part and add  it to psi_out
            ! the kinetic energy is calculated at the same time
            ! do this thing for both components of the spinors
            do idx=1,nspinor,2
               call convolut_kinetic_slab_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kx,ky,kz)
               ekin=ekin+ekino
            end do

            !re-Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.false.)

            do idx=1,nspinor
               !new compression routine in mixed form
               call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do

         else
            do idx=1,nspinor
               call convolut_magic_t_slab_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))

               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               call convolut_kinetic_slab_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
               ekin=ekin+ekino

               !new compression routine in mixed form
               call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do
         end if

      case('P')

         if (lr%hybrid_on) then

            !here kpoints cannot be used, such BC are used in general to mimic the Free BC
            if (f_err_raise(usekpts, 'K points not allowed for hybrid BC locham', &
                 err_id = LIBORBS_OPERATION_ERROR())) return

            !here the grid spacing is not halved
            do idx=1,nspinor
               call comb_shrink_hyb(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
                    w%w2,w%w1,psir(1,idx),w%y_c(1,idx),w%y_f(1,idx),lr%bounds%sb)

               call convolut_kinetic_hyb_T(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
                    lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                    lr%mesh_coarse%hgrids,w%x_c(1,idx),w%x_f(1,idx),w%y_c(1,idx),w%y_f(1,idx),kstrteno,&
                    w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),lr%bounds%kb%ibyz_f,&
                    lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f)
               kstrten=kstrten+kstrteno
               !ekin=ekin+ekino

               call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                    hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))
            end do
         else

            if (usekpts) then
               !first calculate the proper arrays then transpose them before passing to the
               !proper routine
               do idx=1,nspinor
                  call convolut_magic_t_per_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       psir(1,idx),w%y_c(1,idx))
               end do

               !Transposition of the work arrays (use psir as workspace)
               call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    w%x_c,psir,.true.)
               call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    w%y_c,psir,.true.)


               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               do idx=1,nspinor,2
                  !print *,'AAA',2*lr%d%n1+1,2*lr%d%n2+1,2*lr%d%n3+1,hgridh

                  call convolut_kinetic_per_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno,kx,ky,kz)
                  kstrten=kstrten+kstrteno
                  !ekin=ekin+ekino
               end do

               !Transposition of the work arrays (use psir as workspace)
               call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    w%y_c,psir,.false.)

               do idx=1,nspinor

                  call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                       w%y_c(1,idx),psir(1,idx))
                  call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                       hpsi(1,idx), psir(1,idx))
               end do
            else
               !first calculate the proper arrays then transpose them before passing to the
               !proper routine
               do idx=1,nspinor
                  call convolut_magic_t_per_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       psir(1,idx),w%y_c(1,idx))
                  ! compute the kinetic part and add  it to psi_out
                  ! the kinetic energy is calculated at the same time
                  call convolut_kinetic_per_t(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno)
                  kstrten=kstrten+kstrteno

                  call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                       w%y_c(1,idx),psir(1,idx))
                  call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                       hpsi(1,idx), psir(1,idx))
               end do
            end if

         end if
         ekin=ekin+kstrten(1)+kstrten(2)+kstrten(3)
         if (present(k_strten)) k_strten=kstrten

      case('W')

         if (usekpts) then
            !first calculate the proper arrays then transpose them before passing to the
            !proper routine
            do idx=1,nspinor
               call convolut_magic_t_wire_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))
            end do

            !Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%x_c,psir,.true.)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.true.)

            ! compute the kinetic part and add  it to psi_out
            ! the kinetic energy is calculated at the same time
            ! do this thing for both components of the spinors
            do idx=1,nspinor,2
               call convolut_kinetic_wire_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kx,ky,kz)
               ekin=ekin+ekino
            end do

            !re-Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.false.)

            do idx=1,nspinor
               !new compression routine in mixed form
               call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do

         else
            do idx=1,nspinor
               call convolut_magic_t_wire_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))

               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               call convolut_kinetic_wire_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
               ekin=ekin+ekino

               !new compression routine in mixed form
               call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do
         end if

      end select

    END SUBROUTINE isf_to_daub_kinetic

    subroutine daub_to_isf_ocl(lr, queue, psi_r, psi, w)
      use f_precisions, only: f_address
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), intent(inout) :: w
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      real(wp), dimension(lr%mesh%ndim), intent(out) :: psi_r

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      call set_ocl_sumrho_sync(w, queue, lr, psi)
      call f_daub_to_isf(queue, lr%mesh_coarse%ndims, periodic,&
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c,&
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f,&
           w%psi_c, w%psi_f, w%work1, w%work2, w%work3, w%d)
      call ocl_enqueue_read_buffer(queue, w%work2, lr%mesh%ndim*8, psi_r(1))
    end subroutine daub_to_isf_ocl

    subroutine isf_to_daub_ocl(lr, queue, psi, psi_r, w)
      use f_precisions, only: f_address
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), intent(inout) :: w
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      real(wp), dimension(lr%mesh%ndim), intent(in) :: psi_r

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      call ocl_enqueue_write_buffer(queue, w%work1, lr%mesh%ndim*8, psi_r(1))
      call f_isf_to_daub(queue, lr%mesh_coarse%ndims, periodic, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w%psi_c, w%psi_f, w%work1, w%work2, w%work3, w%d)
      call get_ocl_sumrho_sync(w, queue, lr, psi)
    end subroutine isf_to_daub_ocl

    subroutine full_locham_ocl_async(lr, queue, kxyz, psi, hpsi, nspinor, &
         rhopot_addr, epot, ekin, w, p_addr, h_addr, h_offset)
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: nspinor, h_offset
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), dimension(2), intent(inout) :: w
      real(wp), dimension(array_dim(lr), nspinor), intent(out) :: psi
      real(wp), dimension(array_dim(lr), nspinor), intent(out) :: hpsi
      real(gp), dimension(3) :: kxyz
      integer(f_address), intent(in) :: rhopot_addr, h_addr
      integer(f_address), dimension(2, nspinor), intent(in) :: p_addr
      real(gp), dimension(2), intent(out) :: epot, ekin

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      call set_ocl_sumrho_async(w(1), queue, lr, psi(1, 1), p_addr(1, 1))
      if (nspinor == 2) call set_ocl_sumrho_async(w(2), queue, lr, psi(1, 2), p_addr(1, 2))
      !calculate the local hamiltonian
      !WARNING: the difference between full_locham and normal locham is inside
      call f_fulllocham_generic_k(queue, lr%mesh_coarse%ndims, periodic, &
           lr%mesh%hgrids, kxyz, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w(1)%psi_c, w(1)%psi_f, w(2)%psi_c, w(2)%psi_f, rhopot_addr, &
           w(1)%work1, w(1)%work2, w(1)%work3, &
           w(2)%work1, w(2)%work2, w(2)%work3, &
           w(1)%d, w(2)%d, nspinor, epot, ekin)

      call get_ocl_sumrho_async(w(1), queue, lr, hpsi(1, 1), h_addr, h_offset)
      if (nspinor == 2) then
         call get_ocl_sumrho_async(w(2), queue, lr, hpsi(1, 2), h_addr, h_offset+1)
      end if
    end subroutine full_locham_ocl_async
    
    subroutine density_ocl(lr, queue, scal, psi, rhopot_addr, w, p_addr, p_offset)
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: p_offset
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), intent(inout) :: w
      real(gp), intent(in) :: scal
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      integer(f_address), intent(in) :: rhopot_addr, p_addr

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      if (p_addr > 0_f_address) call ocl_map_write_buffer_async(queue, p_addr, &
           p_offset, lr%wfd%nvctr_c*8)
      call ocl_enqueue_write_buffer(queue, w%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (p_addr > 0_f_address) call ocl_unmap_mem_object(queue, p_addr, psi(1))

      if (lr%wfd%nvctr_f > 0) then
         if (p_addr > 0_f_address) call ocl_map_write_buffer_async(queue, p_addr, &
              p_offset + lr%wfd%nvctr_c*8, 7*lr%wfd%nvctr_f*8)
         call ocl_enqueue_write_buffer(queue, w%psi_f, 7*lr%wfd%nvctr_f*8, &
              psi(lr%wfd%nvctr_c + 1))
         if (p_addr > 0_f_address) call ocl_unmap_mem_object(queue, p_addr, &
              psi(lr%wfd%nvctr_c + 1))
      end if

      !calculate the density
      call f_locden_generic(queue, lr%mesh_coarse%ndims, periodic, scal, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w%psi_c, w%psi_f, w%work1, w%work2, w%work3, rhopot_addr)
    end subroutine density_ocl

    subroutine create_ocl_sumrho(GPU, context, lr)
      implicit none
      type(workarrays_ocl_sumrho), intent(out) :: GPU
      integer(f_address), intent(in) :: context
      type(locreg_descriptors), intent(in) :: lr

      !allocate the compressed wavefunctions such as to be used as workspace
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8,GPU%psi_c)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8,GPU%psi_f)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%work1)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%work2)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%work3)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%d)
    end subroutine create_ocl_sumrho

    subroutine set_ocl_sumrho_sync(GPU, queue, lr, psi, pin) 
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      integer(f_address), dimension(2), intent(in), optional :: pin

      integer(f_address) :: pin_c, pin_f

      pin_c = 0
      if (present(pin)) pin_c = pin(1)
      if (pin_c > 0) &
           call ocl_pin_read_buffer_async(queue, lr%wfd%nvctr_c*8, psi(1), pin_c)
      call ocl_enqueue_write_buffer(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (pin_c > 0) call ocl_release_mem_object(pin_c)

      if (lr%wfd%nvctr_f > 0) then
         pin_f = 0
         if (present(pin)) pin_f = pin(2)
         if (pin_f > 0) &
              call ocl_pin_read_buffer_async(queue, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c + 1), pin_f)
         call ocl_enqueue_write_buffer(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c+1))
         if (pin_f > 0) call ocl_release_mem_object(pin_f)
      end if
    end subroutine set_ocl_sumrho_sync

    subroutine set_ocl_sumrho_async(GPU, queue, lr, psi, pin)
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      integer(f_address), dimension(2), intent(in), optional :: pin
      integer(f_address) :: pin_c, pin_f

      pin_c = 0
      if (present(pin)) pin_c = pin(1)
      if (pin_c > 0) &
           call ocl_pin_read_buffer_async(queue, lr%wfd%nvctr_c*8, psi(1), pin_c)
      call ocl_enqueue_write_buffer_async(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (pin_c > 0) call ocl_release_mem_object(pin_c)

      if (lr%wfd%nvctr_f > 0) then
         pin_f = 0
         if (present(pin)) pin_f = pin(2)
         if (pin_f > 0) &
              call ocl_pin_read_buffer_async(queue, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c + 1), pin_f)
         call ocl_enqueue_write_buffer_async(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c + 1))
         if (pin_f > 0) call ocl_release_mem_object(pin_f)
      end if
    end subroutine set_ocl_sumrho_async

    subroutine get_ocl_sumrho_sync(GPU, queue, lr, psi, pin, offset) 
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      integer, intent(in), optional :: offset
      integer(f_address), intent(in), optional :: pin

      integer :: off
      integer(f_address) :: p

      off = 0
      if (present(offset)) off = offset
      p = 0
      if (present(pin)) p = pin

      if (p > 0) &
           call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off, &
           lr%wfd%nvctr_c*8)
      call ocl_enqueue_read_buffer(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (p > 0) call ocl_unmap_mem_object(queue, p, psi(1))

      if (lr%wfd%nvctr_f > 0) then
         if (p > 0) &
              call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off + &
              lr%wfd%nvctr_c*8, 7*lr%wfd%nvctr_f*8)
         call ocl_enqueue_read_buffer(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, &
              psi(lr%wfd%nvctr_c+1))
         if (p > 0) call ocl_unmap_mem_object(queue, p, psi(lr%wfd%nvctr_c + 1))
      end if
    end subroutine get_ocl_sumrho_sync

    subroutine get_ocl_sumrho_async(GPU, queue, lr, psi, pin, offset)
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      integer, intent(in), optional :: offset
      integer(f_address), intent(in), optional :: pin

      integer :: off
      integer(f_address) :: p

      off = 0
      if (present(offset)) off = offset
      p = 0
      if (present(pin)) p = pin

      if (p > 0) &
           call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off, &
           lr%wfd%nvctr_c*8)
      call ocl_enqueue_read_buffer_async(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (p > 0) call ocl_unmap_mem_object(queue, p, psi(1))

      if (lr%wfd%nvctr_f > 0) then
         if (p > 0) &
              call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off + &
              lr%wfd%nvctr_c*8, 7*lr%wfd%nvctr_f*8)
         call ocl_enqueue_read_buffer_async(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, &
              psi(lr%wfd%nvctr_c + 1))
         if (p > 0) call ocl_unmap_mem_object(queue, p, psi(lr%wfd%nvctr_c + 1))
      end if
    end subroutine get_ocl_sumrho_async

    subroutine deallocate_ocl_sumrho(GPU)
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU

      call ocl_release_mem_object(GPU%psi_c)
      call ocl_release_mem_object(GPU%psi_f)
      call ocl_release_mem_object(GPU%d)
      call ocl_release_mem_object(GPU%work1)
      call ocl_release_mem_object(GPU%work2)
      call ocl_release_mem_object(GPU%work3)
    end subroutine deallocate_ocl_sumrho

    subroutine create_ocl_precond(GPU, context, lr)
      implicit none
      type(workarrays_ocl_precond), intent(out) :: GPU
      integer(f_address), intent(in) :: context
      type(locreg_descriptors), intent(in) :: lr

      !allocate the compressed wavefunctions such as to be used as workspace
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8, GPU%psi_c_r)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8, GPU%psi_f_r)
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8, GPU%psi_c_b)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8, GPU%psi_f_b)
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8, GPU%psi_c_d)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8, GPU%psi_f_d)

      GPU%b_host(1) = 0_f_address
      GPU%b_host(2) = 0_f_address
    end subroutine create_ocl_precond

    subroutine set_ocl_precond_b(GPU, queue, lr, b)
      implicit none
      type(workarrays_ocl_precond), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: b

      call ocl_enqueue_write_buffer(queue, GPU%psi_c_b, lr%wfd%nvctr_c*8, b)
      if (lr%wfd%nvctr_f > 0) &
           call ocl_enqueue_write_buffer(queue, GPU%psi_f_b, 7*lr%wfd%nvctr_f*8, &
           b(lr%wfd%nvctr_c + 1))
    end subroutine set_ocl_precond_b

    subroutine pin_ocl_precond_b(GPU, context, queue, lr, b)
      implicit none
      type(workarrays_ocl_precond), intent(inout) :: GPU
      integer(f_address), intent(in) :: context, queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: b

      call ocl_pin_read_buffer(context, queue, lr%wfd%nvctr_c*8, b(1), GPU%b_host(1))
      if (lr%wfd%nvctr_f > 0) &
           call ocl_pin_read_buffer(context, queue, 7*lr%wfd%nvctr_f*8, &
           b(lr%wfd%nvctr_c + 1), GPU%b_host(2))
    end subroutine pin_ocl_precond_b

    subroutine unpin_ocl_precond_b(GPU)
      implicit none
      type(workarrays_ocl_precond), intent(inout) :: GPU

      if (GPU%b_host(1) /= 0_f_address) call ocl_release_mem_object(GPU%b_host(1))
      if (GPU%b_host(2) /= 0_f_address) call ocl_release_mem_object(GPU%b_host(2))
    end subroutine unpin_ocl_precond_b

    subroutine deallocate_ocl_precond(GPU)
      implicit none
      type(workarrays_ocl_precond), intent(inout) :: GPU

      call ocl_release_mem_object(GPU%psi_c_r)
      call ocl_release_mem_object(GPU%psi_f_r)
      call ocl_release_mem_object(GPU%psi_c_b)
      call ocl_release_mem_object(GPU%psi_f_b)
      call ocl_release_mem_object(GPU%psi_c_d)
      call ocl_release_mem_object(GPU%psi_f_d)
      call unpin_ocl_precond_b(GPU)
    end subroutine deallocate_ocl_precond

    subroutine precondition_residue_ocl(lr, queue, kxyz, hpsi, ncplx, &
         ncong, cprecr, ekin, w, b, w_sumrho, w_precond, &
         p_addr, h_addr, h_offset)
      use wrapper_linalg
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: ncplx, h_offset, ncong
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), dimension(2), intent(inout) :: w_sumrho
      type(workarrays_ocl_precond), dimension(2), intent(inout) :: w_precond
      real(wp), dimension(array_dim(lr), ncplx), intent(inout) :: hpsi
      real(wp), dimension(array_dim(lr), ncplx), intent(inout) :: b
      real(gp), intent(in) :: cprecr
      real(gp), dimension(3), intent(in) :: kxyz
      real(gp), dimension(ncplx), intent(out) :: ekin
      type(workarr_precond), intent(inout) :: w
      integer(f_address), intent(in) :: h_addr
      integer(f_address), dimension(2, ncplx), intent(in) :: p_addr

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri
      real(gp), dimension(0:7) :: scal

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1
      
      call precondition_preconditioner(lr, ncplx, &
           scal, cprecr, w, hpsi(1, 1), b(1,1))

      call set_ocl_sumrho_sync(w_sumrho(1), queue, lr, hpsi(1, 1), p_addr(1,1))
      call set_ocl_precond_b(w_precond(1), queue, lr, b(1,1))

      if(ncplx == 2) then
         call set_ocl_sumrho_sync(w_sumrho(2), queue, lr, hpsi(1, 2), p_addr(1,2))
         call set_ocl_precond_b(w_precond(2), queue, lr, b(1,2))
      endif
      call f_preconditioner_generic_k(queue, lr%mesh_coarse%ndims, periodic,&
           lr%mesh%hgrids, kxyz, cprecr, ncong, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w_sumrho(1)%psi_c,w_sumrho(1)%psi_f, &
           w_sumrho(2)%psi_c,w_sumrho(2)%psi_f, &
           w_precond(1)%psi_c_r,w_precond(1)%psi_f_r, &
           w_precond(2)%psi_c_r,w_precond(2)%psi_f_r, &
           w_precond(1)%psi_c_b,w_precond(1)%psi_f_b, &
           w_precond(2)%psi_c_b,w_precond(2)%psi_f_b, &
           w_precond(1)%psi_c_d,w_precond(1)%psi_f_d, &
           w_precond(2)%psi_c_d,w_precond(2)%psi_f_d, &
           w_sumrho(1)%d,w_sumrho(1)%work1,w_sumrho(1)%work2,w_sumrho(1)%work3, &
           w_sumrho(2)%d,w_sumrho(2)%work1,w_sumrho(2)%work2,w_sumrho(2)%work3, &
           ncplx, ekin) !buffer for scalars resulting from reductions

      call get_ocl_sumrho_sync(w_sumrho(1), queue, lr, hpsi(1, 1), &
           h_addr, h_offset)
      if ( ncplx == 2 ) then
         call get_ocl_sumrho_sync(w_sumrho(2), queue, lr, hpsi(1, 2), &
              h_addr, h_offset+1)
      endif
    end subroutine precondition_residue_ocl
end module locreg_operations
