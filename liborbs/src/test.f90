program test
  use locregs
  implicit none

  type(locreg_descriptors) :: lr

  lr = locreg_null()

  call deallocate_locreg_descriptors(lr)

  call precondition_ket
end program test
