!> @file
!! Basic operations relative to the localization regions
!! @author
!!    Copyright (C) 2007-2014 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS
subroutine psi_to_tpsi(kptv,nspinor,lr,psi,w,hpsi,ekin,k_strten)
  use liborbs_precisions
  use locregs
  use locreg_operations, only: workarr_locham
  use at_domain, only: domain_geocode
  use wrapper_linalg
  use f_utils, only: f_zero
  implicit none
  integer, intent(in) :: nspinor
  real(gp), dimension(3), intent(in) :: kptv
  type(locreg_descriptors), intent(in) :: lr
  type(workarr_locham), intent(inout) :: w
  real(wp), dimension(lr%mesh%ndim,nspinor), intent(in) :: psi
  real(gp), intent(out) :: ekin
  real(wp), dimension(array_dim(lr),nspinor), intent(inout) :: hpsi
  real(wp), dimension(6), optional :: k_strten
  !Local variables
  logical, parameter :: transpose=.false.
  logical :: usekpts
  integer :: idx
  real(gp) :: ekino
  real(wp), dimension(0:7), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp /)
  real(wp), dimension(6) :: kstrten,kstrteno


  !control whether the k points are to be used
  !real k-point different from Gamma still not implemented
  usekpts = nrm2(3,kptv(1),1) > 0.0_gp .or. nspinor == 2

  !call MPI_COMM_RANK(bigdft_mpi%mpi_comm,iproc,ierr)
  ekin=0.0_gp

  kstrten=0.0_wp
!!$  select case(lr%geocode)
  select case(domain_geocode(lr%mesh%dom))
  case('F')

     !here kpoints cannot be used (for the moment, to be activated for the 
     !localisation region scheme
     if (usekpts) stop 'K points not allowed for Free BC locham'

     do idx=1,nspinor
        call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
             w%x_c(1,idx), w%x_f(1,idx), psi(1,idx), scal, &
             w%x_f1(1,idx), w%x_f2(1,idx), w%x_f3(1,idx))

        call f_zero(w%y_c(1,idx),w%nyc)
        call f_zero(w%y_f(1,idx),w%nyf)

        call ConvolkineticT(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
             lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
             lr%mesh_coarse%hgrids(1),lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3), &
             lr%bounds%kb%ibyz_c,lr%bounds%kb%ibxz_c,lr%bounds%kb%ibxy_c,&
             lr%bounds%kb%ibyz_f,lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f, &
             w%x_c(1,idx),w%x_f(1,idx),&
             w%y_c(1,idx),w%y_f(1,idx),ekino, &
             w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),111)
        ekin=ekin+ekino

        !new compression routine in standard form
        call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
             hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))

     end do

  case('S')

     if (usekpts) then
        !first calculate the proper arrays then transpose them before passing to the
        !proper routine
        do idx=1,nspinor
           call uncompress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, &
                w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))
        end do

        !Transposition of the work arrays (use y_c as workspace)
        call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
             w%x_c,w%y_c,.true.)
        call f_zero(w%y_c)

        ! compute the kinetic part and add  it to psi_out
        ! the kinetic energy is calculated at the same time
        ! do this thing for both components of the spinors
        do idx=1,nspinor,2
           call convolut_kinetic_slab_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kptv(1),kptv(2),kptv(3))
           ekin=ekin+ekino        
        end do

        !re-Transposition of the work arrays (use x_c as workspace)
        call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
             w%y_c,w%x_c,.false.)

        do idx=1,nspinor
           !new compression routine in mixed form
           call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                w%y_c(1,idx),w%x_c(1,idx))
           call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                hpsi(1,idx), w%x_c(1,idx))

        end do

     else
        do idx=1,nspinor
           call uncompress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, &
                w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))

           call f_zero(w%y_c(1,idx),w%nyc)
           ! compute the kinetic part and add  it to psi_out
           ! the kinetic energy is calculated at the same time
           call convolut_kinetic_slab_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
           ekin=ekin+ekino

           !new compression routine in mixed form
           call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                w%y_c(1,idx),w%x_c(1,idx))
           call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                hpsi(1,idx), w%x_c(1,idx))
        end do
     end if

  case('P')

     if (lr%hybrid_on) then

        !here kpoints cannot be used, such BC are used in general to mimic the Free BC
        if (usekpts) stop 'K points not allowed for hybrid BC locham'

        !here the grid spacing is not halved
        do idx=1,nspinor
           call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, w%x_c(1,idx), w%x_f(1,idx), &
                psi(1,idx), scal, w%x_f1(1,idx), w%x_f2(1,idx), w%x_f3(1,idx))

           call f_zero(w%y_c(1,idx),w%nyc)
           call f_zero(w%y_f(1,idx),w%nyf)

           call convolut_kinetic_hyb_T(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
                lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                lr%mesh_coarse%hgrids,w%x_c(1,idx),w%x_f(1,idx),w%y_c(1,idx),w%y_f(1,idx),kstrteno,&
                w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),lr%bounds%kb%ibyz_f,&
                lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f)
           kstrten=kstrten+kstrteno
           !ekin=ekin+ekino

           call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))

        end do
     else

        if (usekpts) then

           do idx=1,nspinor
              call uncompress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))
           end do

           if (transpose) then
              !Transposition of the work arrays (use psir as workspace)
              call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                   w%x_c,w%y_c,.true.)

              call f_zero(w%y_c)
              ! compute the kinetic part and add  it to psi_out
              ! the kinetic energy is calculated at the same time
              do idx=1,nspinor,2
                 !print *,'AAA',2*lr%d%n1+1,2*lr%d%n2+1,2*lr%d%n3+1,hgridh

                 call convolut_kinetic_per_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                      lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno,kptv(1),kptv(2),kptv(3))
                 kstrten=kstrten+kstrteno
                 !ekin=ekin+ekino
              end do

              !Transposition of the work arrays (use psir as workspace)
              call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                   w%y_c,w%x_c,.false.)

           else
              call f_zero(w%y_c)
              do idx=1,nspinor,2
                 call convolut_kinetic_per_T_k_notranspose(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                      lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno,kptv(1),kptv(2),kptv(3))
                 kstrten=kstrten+kstrteno
              end do
           end if

           do idx=1,nspinor

              call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                   w%y_c(1,idx),w%x_c(1,idx))
              call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                   hpsi(1,idx), w%x_c(1,idx))

           end do
        else
           !first calculate the proper arrays then transpose them before passing to the
           !proper routine
           do idx=1,nspinor
              call uncompress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))

              call f_zero(w%y_c(1,idx),w%nyc)
              ! compute the kinetic part and add  it to psi_out
              ! the kinetic energy is calculated at the same time
              call convolut_kinetic_per_t(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                   lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno)
              kstrten=kstrten+kstrteno

              call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                   w%y_c(1,idx),w%x_c(1,idx))
              call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                   hpsi(1,idx), w%x_c(1,idx))

           end do
        end if

     end if
     ekin=ekin+kstrten(1)+kstrten(2)+kstrten(3)
     if (present(k_strten)) k_strten=kstrten 

  case('W')


     if (usekpts) then
        !first calculate the proper arrays then transpose them before passing to the
        !proper routine
        do idx=1,nspinor
           call uncompress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))
        end do

        !Transposition of the work arrays (use y_c as workspace)
        call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
             w%x_c,w%y_c,.true.)
        call f_zero(w%y_c(1,1),nspinor*w%nyc)

        ! compute the kinetic part and add  it to psi_out
        ! the kinetic energy is calculated at the same time
        ! do this thing for both components of the spinors
        do idx=1,nspinor,2
           call convolut_kinetic_wire_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kptv(1),kptv(2),kptv(3))
           ekin=ekin+ekino        
        end do

        !re-Transposition of the work arrays (use x_c as workspace)
        call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
             w%y_c,w%x_c,.false.)

        do idx=1,nspinor
           !new compression routine in mixed form
           call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                w%y_c(1,idx),w%x_c(1,idx))
           call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                hpsi(1,idx), w%x_c(1,idx))

        end do

     else
        do idx=1,nspinor
           call uncompress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))

           call f_zero(w%y_c(1,idx),w%nyc)
           ! compute the kinetic part and add  it to psi_out
           ! the kinetic energy is calculated at the same time
           call convolut_kinetic_wire_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
           ekin=ekin+ekino

           !new compression routine in mixed form
           call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                w%y_c(1,idx),w%x_c(1,idx))
           call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                hpsi(1,idx), w%x_c(1,idx))
        end do
     end if
  end select

END SUBROUTINE psi_to_tpsi


subroutine psi_to_tpsi_dir(lr,psi,w,hpsi_x, hpsi_y, hpsi_z)
  use liborbs_precisions
  use locregs
  use locreg_operations, only: workarr_locham
  use f_utils, only: f_zero
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  type(workarr_locham), intent(inout) :: w
  real(wp), dimension(lr%mesh%ndim), intent(in) :: psi
  real(wp), dimension(array_dim(lr)), intent(out) :: hpsi_x, hpsi_y, hpsi_z

  integer :: iidim, idir
  real(gp) :: ekino
  real(wp), dimension(0:3), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp /)

  !!call daub_to_isf_locham(orbs%nspinor, lzd%llr(ilr), wrk_lh, psi(ist), psir)
  call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
       w%x_c(1,1), w%x_f(1,1), psi(1), scal, &
       w%x_f1(1,1), w%x_f2(1,1), w%x_f3(1,1))

  do idir=1,3
     iidim = 10**(3-idir)
     call f_zero(w%y_c)
     call f_zero(w%y_f)
     !!call convolut_kinetic_per_T_1D(2*lr%d%n1+1, 2*lr%d%n2+1, 2*lr%d%n3+1, &
     !!     hgridh(idir), idir, w%x_c(1,1), w%y_c(1,1))
     call ConvolkineticT(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
          lr%nboxf(1,1), lr%nboxf(2,1), &
          lr%nboxf(1,2), lr%nboxf(2,2), &
          lr%nboxf(1,3), lr%nboxf(2,3), &
          lr%mesh_coarse%hgrids(1), lr%mesh_coarse%hgrids(2), lr%mesh_coarse%hgrids(3), &
          lr%bounds%kb%ibyz_c, lr%bounds%kb%ibxz_c, lr%bounds%kb%ibxy_c, &
          lr%bounds%kb%ibyz_f, lr%bounds%kb%ibxz_f, lr%bounds%kb%ibxy_f, &
          w%x_c(1,1), w%x_f(1,1), &
          w%y_c(1,1), w%y_f(1,1), ekino, &
          w%x_f1(1,1), w%x_f2(1,1), w%x_f3(1,1),iidim)
     ! This is ugly at the moment, but data are not packed properly in force_linear where it's used.
     select case (idir)
        case (1)
           call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, hpsi_x, &
                w%y_c(1,1), w%y_f(1,1), scal)
        case (2)
           call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, hpsi_y, &
                w%y_c(1,1), w%y_f(1,1), scal)
        case (3)
           call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, hpsi_z, &
                w%y_c(1,1), w%y_f(1,1), scal)
     end select
  end do
end subroutine psi_to_tpsi_dir

subroutine get_one_derivative_supportfunction(lr,phi,phid)
   use locregs
   implicit none
   
   ! Calling arguments
   type(locreg_descriptors),intent(in) :: lr
   real(kind=8),dimension(array_dim(lr)),intent(in) :: phi !< Basis functions
   real(kind=8),dimension(3*array_dim(lr)),intent(inout) :: phid  !< Derivative basis functions

   ! Local variables
   integer :: nf
   real(kind=8),dimension(0:3),parameter :: scal=1.d0
   real(kind=8),dimension(:),allocatable :: w_f1, w_f2, w_f3
   real(kind=8),dimension(:),allocatable :: w_c, phix_c, phiy_c, phiz_c
   real(kind=8),dimension(:,:,:,:),allocatable :: w_f, phix_f, phiy_f, phiz_f
   character(len=*),parameter :: subname='get_one_derivative_supportfunction'

   call allocateWorkarrays()

   ! Uncompress the wavefunction.
   call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
        w_c, w_f, phi, scal, w_f1, w_f2, w_f3)

   call createDerivativeBasis(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
        lr%nboxf(1,1), lr%nboxf(2,1), lr%nboxf(1,2), lr%nboxf(2,2), &
        lr%nboxf(1,3), lr%nboxf(2,3),  &
        lr%bounds%kb%ibyz_c, lr%bounds%kb%ibxz_c, lr%bounds%kb%ibxy_c, &
        lr%bounds%kb%ibyz_f, lr%bounds%kb%ibxz_f, lr%bounds%kb%ibxy_f, &
        w_c, w_f, w_f1, w_f2, w_f3, phix_c, phix_f, phiy_c, phiy_f, phiz_c, phiz_f)

   ! Compress the x wavefunction.
   call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, phid, phix_c, phix_f, scal)

   ! Compress the y wavefunction.
   call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
        phid(array_dim(lr)+1), phiy_c, phiy_f, scal)

   ! Compress the z wavefunction.
   call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
        phid(2*array_dim(lr)+1), phiy_c, phiy_f, scal)

   call deallocateWorkarrays()

contains

  subroutine allocateWorkarrays()
    use dynamic_memory
    ! THIS IS COPIED FROM allocate_work_arrays. Works only for free boundary.
    nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-lr%nboxf(1,2)+1)* &
       (lr%nboxf(2,3)-lr%nboxf(1,3)+1)

    ! Allocate work arrays
    w_c = f_malloc0(lr%mesh_coarse%ndim,id='w_c')

    w_f = f_malloc0((/ 1.to.7 , lr%nboxf(1,1).to.lr%nboxf(2,1) , lr%nboxf(1,2).to.lr%nboxf(2,2) , &
                 lr%nboxf(1,3) .to. lr%nboxf(2,3)/),id='w_f')

  
    w_f1 = f_malloc0(nf,id='w_f1')
    
    w_f2 = f_malloc0(nf,id='w_f2')

    w_f3 = f_malloc0(nf,id='w_f3')
  
  
    phix_f = f_malloc0((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), lr%nboxf(1,2).to.lr%nboxf(2,2), &
                              lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='phix_f')

    phix_c = f_malloc0(lr%mesh_coarse%ndim,id='phix_c')
    !call to_zero((lr%d%n1+1)*(lr%d%n2+1)*(lr%d%n3+1), phix_c(0,0,0))

    phiy_f = f_malloc0((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), lr%nboxf(1,2).to.lr%nboxf(2,2), &
                              lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='phiy_f')

    phiy_c = f_malloc0(lr%mesh_coarse%ndim,id='phiy_c')

    phiz_f = f_malloc0((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), lr%nboxf(1,2).to.lr%nboxf(2,2), &
                              lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='phiz_f')

    phiz_c = f_malloc0(lr%mesh_coarse%ndim,id='phiz_c')
  
  end subroutine allocateWorkarrays


  subroutine deallocateWorkarrays
    use dynamic_memory
    call f_free(w_c)
    call f_free(w_f)
    call f_free(w_f1)
    call f_free(w_f2)
    call f_free(w_f3)
    call f_free(phix_f)
    call f_free(phix_c)
    call f_free(phiy_f)
    call f_free(phiy_c)
    call f_free(phiz_f)
    call f_free(phiz_c)

  end subroutine deallocateWorkarrays

end subroutine get_one_derivative_supportfunction
