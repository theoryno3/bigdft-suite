#ifndef MAGICFILTER_GENERATOR_H
#define MAGICFILTER_GENERATOR_H

#include "liborbs_ocl.h"

#ifdef __cplusplus
extern "C" char* generate_magicfilter_program(struct liborbs_device_infos * infos);
#else
char* generate_magicfilter_program(struct liborbs_device_infos * infos);
#endif

#endif
