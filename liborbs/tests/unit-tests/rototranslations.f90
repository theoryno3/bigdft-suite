program test_rototranslations
  use yaml_output
  use f_unittests
  use wrapper_MPI

  implicit none

  type(mpi_environment) :: env
  
  call mpiinit()

  env = mpi_environment_comm()  

  if (env%iproc == 0) then
     call f_lib_initialize()

     call yaml_sequence_open("type(rototranslations)")
     call run(identity)
     call run(noRotation)
     call run(rigidRotationX)
     call run(rigidRotationY)
     call run(rigidRotationZ)
     call run(deformation)
     call yaml_sequence_close()

     call f_lib_finalize()
  end if

  call release_mpi_environment(env)
  call mpifinalize()

contains

  subroutine identity(label)
    use liborbs_precisions
    use rototranslations
    implicit none
    character(len = *), intent(out) :: label

    type(rototranslation) :: rt

    label = "identity"
    rt = rototranslation_identity()
    call compare(rt%rot_center_dest, (/ 0._gp, 0._gp, 0._gp /), "rotation centre destination")
    call compare(rt%rot_center_src, (/ 0._gp, 0._gp, 0._gp /), "rotation centre source")
    call compare(rt%rot_axis, (/ 1._gp, 1._gp, 1._gp /) / sqrt(3._gp), "rotation axis")
    call compare(rt%theta, 0._gp, "rotation angle")
    call compare(rt%Werror, 0._gp, "Wahba error")
    call compare(rt%Rmat, reshape([1.0_gp,0.0_gp,0.0_gp,0.0_gp,1.0_gp,0.0_gp,&
          0.0_gp,0.0_gp,1.0_gp],[3,3]), "rotation matrix")
  end subroutine identity

  subroutine noRotation(label)
    use liborbs_precisions
    use rototranslations
    use numerics
    implicit none
    character(len = *), intent(out) :: label

    type(rototranslation) :: rt
    integer, parameter :: nat = 4
    real(gp), dimension(3, nat) :: src, dest
    real(gp), parameter :: alpha = sqrt(2._gp) / 2._gp

    label = "no rotation"

    src(:, 1) = (/ -1._gp, -1._gp, -1._gp /)
    src(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    src(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    src(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    dest(:, 1) = (/ -1._gp, -1._gp, -1._gp /)
    dest(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    dest(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    dest(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    rt = rototranslation_new(nat, src, dest)
    call verify(rt == rototranslation_identity(), "identity")
  end subroutine noRotation

  subroutine rigidRotationX(label)
    use liborbs_precisions
    use rototranslations
    use numerics
    implicit none
    character(len = *), intent(out) :: label

    type(rototranslation) :: rt
    integer, parameter :: nat = 4
    real(gp), dimension(3, nat) :: src, dest
    real(gp), parameter :: alpha = sqrt(2._gp) / 2._gp

    label = "rigid rotation along X"

    src(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    src(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    src(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    src(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    dest(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    dest(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    dest(:, 3) = (/ 0._gp, 1._gp, -1._gp /) / sqrt(2._gp)
    dest(:, 4) = (/ 0._gp, 1._gp, 1._gp /) / sqrt(2._gp)
    
    rt = rototranslation_new(nat, src, dest)
    call compare(rt%rot_center_dest, (/ .25_gp, sqrt(2._gp) / 4._gp, 0._gp /), "rotation centre destination", tol = 1d-12)
    call compare(rt%rot_center_src, (/ .25_gp, .25_gp, .25_gp /), "rotation centre source")
    call compare(rt%rot_axis, (/ -1._gp, 0._gp, 0._gp /), "rotation axis", tol = 1d-12)
    call compare(rt%theta, pi / 4._gp, "rotation angle", tol = 1d-12)
    call compare(rt%Werror, 0._gp, "Wahba error", tol = 1d-12)
    
    call compare(rt%Rmat, reshape([1.0_gp,0.0_gp,0.0_gp,0.0_gp,alpha,alpha,&
          0.0_gp,-alpha,alpha],[3,3]), "rotation matrix", tol = 1d-12)
  end subroutine rigidRotationX

  subroutine rigidRotationY(label)
    use liborbs_precisions
    use rototranslations
    use numerics
    implicit none
    character(len = *), intent(out) :: label

    type(rototranslation) :: rt
    integer, parameter :: nat = 4
    real(gp), dimension(3, nat) :: src, dest
    real(gp), parameter :: alpha = sqrt(2._gp) / 2._gp

    label = "rigid rotation along Y"

    src(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    src(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    src(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    src(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    dest(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    dest(:, 2) = (/ 1._gp, 0._gp, 1._gp /) / sqrt(2._gp)
    dest(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    dest(:, 4) = (/ -1._gp, 0._gp, 1._gp /) / sqrt(2._gp)
    
    rt = rototranslation_new(nat, src, dest)
    call compare(rt%rot_center_dest, (/ 0._gp, .25_gp, sqrt(2._gp) / 4._gp /), "rotation centre destination", tol = 1d-12)
    call compare(rt%rot_center_src, (/ .25_gp, .25_gp, .25_gp /), "rotation centre source")
    call compare(rt%rot_axis, (/ 0._gp, -1._gp, 0._gp /), "rotation axis", tol = 1d-12)
    call compare(rt%theta, pi / 4._gp, "rotation angle", tol = 1d-12)
    call compare(rt%Werror, 0._gp, "Wahba error", tol = 1d-12)
    
    call compare(rt%Rmat, reshape([alpha,0.0_gp,-alpha,0.0_gp,1._gp,0._gp,&
          alpha,0._gp,alpha],[3,3]), "rotation matrix", tol = 1d-12)
  end subroutine rigidRotationY

  subroutine rigidRotationZ(label)
    use liborbs_precisions
    use rototranslations
    use numerics
    implicit none
    character(len = *), intent(out) :: label

    type(rototranslation) :: rt
    integer, parameter :: nat = 4
    real(gp), dimension(3, nat) :: src, dest
    real(gp), parameter :: alpha = sqrt(2._gp) / 2._gp

    label = "rigid rotation along Z"

    src(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    src(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    src(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    src(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    dest(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    dest(:, 2) = (/ 1._gp, -1._gp, 0._gp /) / sqrt(2._gp)
    dest(:, 3) = (/ 1._gp, 1._gp, 0._gp /) / sqrt(2._gp)
    dest(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    rt = rototranslation_new(nat, src, dest)
    call compare(rt%rot_center_dest, (/ sqrt(2._gp) / 4._gp, 0._gp, .25_gp /), "rotation centre destination", tol = 1d-12)
    call compare(rt%rot_center_src, (/ .25_gp, .25_gp, .25_gp /), "rotation centre source")
    call compare(rt%rot_axis, (/ 0._gp, 0._gp, -1._gp /), "rotation axis", tol = 1d-12)
    call compare(rt%theta, pi / 4._gp, "rotation angle", tol = 1d-12)
    call compare(rt%Werror, 0._gp, "Wahba error", tol = 1d-12)
    
    call compare(rt%Rmat, reshape([alpha,alpha,0._gp,-alpha,alpha,0._gp,&
         0._gp,0._gp,1._gp],[3,3]), "rotation matrix", tol = 1d-12)
  end subroutine rigidRotationZ

  subroutine deformation(label)
    use liborbs_precisions
    use rototranslations
    use numerics
    implicit none
    character(len = *), intent(out) :: label

    type(rototranslation) :: rt, id
    integer, parameter :: nat = 4
    real(gp), dimension(3, nat) :: src, dest
    real(gp), parameter :: alpha = sqrt(2._gp) / 2._gp

    label = "deformation"

    src(:, 1) = (/ -1._gp, -1._gp, -1._gp /)
    src(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    src(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    src(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    dest(:, 1) = (/ -2._gp, -2._gp, -2._gp /)
    dest(:, 2) = (/ 2._gp, 0._gp, 0._gp /)
    dest(:, 3) = (/ 0._gp, 2._gp, 0._gp /)
    dest(:, 4) = (/ 0._gp, 0._gp, 2._gp /)
    
    rt = rototranslation_new(nat, src, dest)
    id = rototranslation_identity()
    id%Werror = 6._gp
    call verify(rt == id, "large Whaba")
  end subroutine deformation

end program test_rototranslations
