Publications
------------

Here, you can find the links to papers describing_ and using_ BigDFT.

.. _describing: http://bigdft.org/Wiki/index.php?title=Articles_describing_BigDFT
.. _using: http://bigdft.org/Wiki/index.php?title=Articles_using_BigDFT

Talks
=====

You can find some conference and workshop slides about BigDFT here_.

.. _here: http://bigdft.org/Wiki/index.php?title=Presenting_BigDFT

Citing BigDFT
=============

The most recent review article about BigDFT can be cited as::

  @article{doi:10.1063/5.0004792,
    author = {Ratcliff,Laura E.  and Dawson,William  and Fisicaro,Giuseppe  and Caliste,Damien  and Mohr,Stephan  and Degomme,Augustin  and Videau,Brice  and Cristiglio,Viviana  and Stella,Martina  and D’Alessandro,Marco  and Goedecker,Stefan  and Nakajima,Takahito  and Deutsch,Thierry  and Genovese,Luigi },
    title = {Flexibilities of wavelets as a computational basis set for large-scale electronic structure calculations},
    journal = {The Journal of Chemical Physics},
    volume = {152},
    number = {19},
    pages = {194110},
    year = {2020},
    doi = {10.1063/5.0004792},
    URL = {https://doi.org/10.1063/5.0004792},
    eprint = {https://doi.org/10.1063/5.0004792}
  }