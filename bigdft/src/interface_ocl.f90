!> @file
!!  Interface routines to do GPU convolution with OpenCL
!! @author
!!    Copyright (C) 2010-2011 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


subroutine release_acceleration_OCL(GPU)
  use module_types
  implicit none
  type(GPU_pointers), intent(out) :: GPU
  call ocl_clean_command_queue(GPU%queue)
  call ocl_clean(GPU%context)
END SUBROUTINE release_acceleration_OCL


subroutine init_acceleration_OCL(matacc,GPU)
  use module_input_keys, only: material_acceleration
  use module_types
  implicit none
  type(material_acceleration), intent(in) :: matacc
  type(GPU_pointers), intent(out) :: GPU
  integer(kind=8) :: context_address

  call ocl_create_context(GPU%context, matacc%OCL_platform, matacc%OCL_devices, matacc%iacceleration,&
                     GPU%ndevices)
  !call ocl_create_gpu_context(GPU%context,GPU%ndevices)
  !call ocl_create_command_queue(GPU%queue,GPU%context)
  !to avoid a representation of the address which is lower than tiny(1.d0)
  context_address=transfer(GPU%context,context_address)
  !if (GPU%context /= 0.) then
  if (context_address /= int(0,kind=8)) then
     call ocl_build_programs(GPU%context)
     call ocl_create_command_queue_id(GPU%queue,GPU%context,GPU%id_proc)
     call init_event_list(GPU%context)
  end if
END SUBROUTINE init_acceleration_OCL


subroutine allocate_data_OCL(lr,nspin,orbs,GPU)
  use module_bigdft_arrays
  use module_bigdft_profiling
  use module_types
  use locregs
  use locreg_operations
  use f_precisions, only: f_address
  implicit none
!!$  character(len=1), intent (in) :: geocode !< @copydoc poisson_solver::doc::geocode
  integer, intent(in) :: nspin
  type(locreg_descriptors), intent(inout) :: lr
  type(orbitals_data), intent(in) :: orbs
  type(GPU_pointers), intent(out) :: GPU
  !local variables
  character(len=*), parameter :: subname='allocate_data_OCL'
  logical, parameter :: pin=.false.
  integer :: iorb,ispinor
  real(kind=8) :: rcontext, rqueue
  integer(f_address) :: context, queue
  equivalence(rcontext, context)
  equivalence(rqueue, queue)

  rcontext = GPU%context
  rqueue = GPU%queue

  call f_routine(id=subname)

  !allocate the number of GPU pointers for the wavefunctions
  !allocate(GPU%psi(orbs%norbp+ndebug),stat=i_stat)
  !call memocc(i_stat,GPU%psi,'GPU%psi',subname)

  !allocate space on the card
  call create_ocl_sumrho(GPU%sumrho(1), context, lr)
  if ( orbs%nspinor == 2) call create_ocl_sumrho(GPU%sumrho(2), context, lr)
  !here spin value should be taken into account
  call ocl_create_read_write_buffer(GPU%context, lr%mesh%ndim*8,GPU%rhopot_up)
  if( nspin == 2 ) then
    call ocl_create_read_write_buffer(GPU%context, lr%mesh%ndim*8,GPU%rhopot_down)
  end if

  call lr_ocl_init(lr, context, queue, pin)

  !for preconditioner
  call create_ocl_precond(GPU%precond(1), context, lr)
  if ( orbs%nspinor == 2) call create_ocl_precond(GPU%precond(2), context, lr)
  !full_locham stategy (always true for the moment)
  GPU%full_locham=.true.

  GPU%ekin=f_malloc_ptr((/2,orbs%norbp/),id='ekin')
  GPU%epot=f_malloc_ptr((/2,orbs%norbp/),id='epot')
  if (pin) then
     GPU%ekinpot_host=f_malloc_ptr((/orbs%nspinor,orbs%norbp,2/),id='ekinpot_host')
     GPU%psicf_host=f_malloc_ptr((/2*orbs%nspinor,orbs%norbp/),id='psicf_host')
  end if

  !pin the memory of the orbitals energies
  if (pin) then
     do iorb=1,orbs%norbp
        do ispinor=1,orbs%nspinor
           call ocl_pin_write_buffer_async(GPU%context,GPU%queue,8,GPU%ekin(ispinor,iorb),GPU%ekinpot_host(ispinor,iorb,1))
           call ocl_pin_write_buffer_async(GPU%context,GPU%queue,8,GPU%epot(ispinor,iorb),GPU%ekinpot_host(ispinor,iorb,2))
        end do
     end do
  end if

  nullify(GPU%hpsi_ASYNC)

  call f_release_routine()

END SUBROUTINE allocate_data_OCL


subroutine free_gpu_OCL(GPU,orbs,nspin)
  use module_bigdft_arrays
  use module_types
  use locreg_operations
  implicit none
  integer, intent(in) :: nspin
  type(orbitals_data), intent(in) :: orbs
  type(GPU_pointers), intent(out) :: GPU
  !local variables
  character(len=*), parameter :: subname='free_gpu_OCL'
  logical, parameter :: pin=.false.
  integer :: iorb,ispinor

  call f_free_ptr(GPU%ekin)
  call f_free_ptr(GPU%epot)


  call ocl_release_mem_object(GPU%rhopot_up)
  if ( nspin == 2 ) then
    call ocl_release_mem_object(GPU%rhopot_down)
  endif
  call deallocate_ocl_sumrho(GPU%sumrho(1))
  if ( orbs%nspinor == 2) call deallocate_ocl_sumrho(GPU%sumrho(2))
  !for preconditioner
  call deallocate_ocl_precond(GPU%precond(1))
  if ( orbs%nspinor == 2) call deallocate_ocl_precond(GPU%precond(2))

  if(associated(GPU%hpsi_ASYNC)) nullify(GPU%hpsi_ASYNC)

  if (pin) then
     !for pinning tracing
     do iorb=1,orbs%norbp
        do ispinor=1,orbs%nspinor
           call ocl_release_mem_object(GPU%ekinpot_host(ispinor,iorb,1))
           call ocl_release_mem_object(GPU%ekinpot_host(ispinor,iorb,2))
        end do
     end do
     call f_free_ptr(GPU%ekinpot_host)
     call f_free_ptr(GPU%psicf_host)

  end if


END SUBROUTINE free_gpu_OCL


subroutine local_hamiltonian_OCL(orbs,lr,&
     nspin,pot,psi,hpsi,ekin_sum,epot_sum,GPU)
  use module_precisions
  use module_types
  use locregs
  use f_precisions
  use locreg_operations
  use module_bigdft_config, only: ASYNCconv
  implicit none
  integer, intent(in) :: nspin
  type(orbitals_data), intent(in) :: orbs
  type(locreg_descriptors), intent(in) :: lr
  real(wp), dimension(array_dim(lr)*orbs%nspinor,orbs%norbp), intent(inout) :: psi
  real(wp), dimension(lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),nspin) :: pot
  real(gp), intent(out) :: ekin_sum,epot_sum
  real(wp), dimension(array_dim(lr)*orbs%nspinor,orbs%norbp), intent(out) :: hpsi
  type(GPU_pointers), intent(inout) :: GPU
  !local variables
  character(len=*), parameter :: subname='local_hamiltonian_OCL'
  logical, parameter :: pin=.false.
  integer :: iorb
  !stream ptr array
  real(kind=8) :: rhopot
  integer :: n1, n2, n3
  real(kind=8) :: rqueue, psicf(4)
  integer(f_address) :: queue, paddr(4), hpsiaddr, rhopot_addr
  equivalence(rqueue, queue)
  equivalence(psicf, paddr)
  equivalence(rhopot, rhopot_addr)

  rqueue = GPU%queue

  n1 = lr%mesh%ndims(1)
  n2 = lr%mesh%ndims(2)
  n3 = lr%mesh%ndims(3)

  !define the pinned adresses for the pinning of the interesting objects

  if (pin) call ocl_pin_read_buffer_async(GPU%queue,n1*n2*n3*8,pot,GPU%rhopot_up_host)
  call ocl_enqueue_write_buffer_async(GPU%queue,GPU%rhopot_up,n1*n2*n3*8,pot)
  if (pin) call ocl_release_mem_object(GPU%rhopot_up_host)
  if( nspin == 2 ) then
     if (pin) call ocl_pin_read_buffer_async(GPU%queue,n1*n2*n3*8,pot(1,1,1,2),GPU%rhopot_down_host)
     call ocl_enqueue_write_buffer_async(GPU%queue,GPU%rhopot_down,n1*n2*n3*8,pot(1,1,1,2))
     if (pin) call ocl_release_mem_object(GPU%rhopot_down_host)
  end if

!!$  epot_sum=0.0_gp
!!$  ekin_sum=0.0_gp

  hpsiaddr = 0
  if (pin .and. orbs%norbp > 0) call ocl_create_write_buffer_host( GPU%context, &
       orbs%norbp*orbs%nspinor*array_dim(lr)*8, hpsi, hpsiaddr )

!  call ocl_create_read_buffer_host( GPU%context, &
!       orbs%norbp*orbs%nspinor*array_dim(lr)*8, psi, GPU%psicf_host(1,1) )

  do iorb=1,orbs%norbp

     if (orbs%spinsgn(orbs%isorb+iorb) > 0.0) then
       rhopot = GPU%rhopot_up
     else
       rhopot = GPU%rhopot_down
     endif
     !if orbs%nspinor /= 1 this implementation should be rediscussed
     if (.not. GPU%full_locham) then
        stop 'ONLY FULL LOCHAM IS IMPLEMENTED!'
     end if

     psicf = 0
     if (pin) psicf(1:2) = GPU%psicf_host(1:2,iorb)
     if (orbs%nspinor == 2 .and. pin) psicf(3:4) = GPU%psicf_host(3:4,iorb)

     call full_locham_ocl_async(lr, queue, orbs%kpts(:,orbs%iokpt(iorb)), &
          psi(1, iorb), hpsi(1, iorb), orbs%nspinor, rhopot_addr, &
          GPU%epot(:, iorb), GPU%ekin(:, iorb), &
          GPU%sumrho, paddr, hpsiaddr, (iorb-1)*orbs%nspinor)
  end do
!  call ocl_release_mem_object(GPU%psicf_host(1,1))
  if (pin .and. orbs%norbp > 0) call ocl_release_mem_object(hpsiaddr)
  if (.not. ASYNCconv) then
     call finish_hamiltonian_OCL(orbs,ekin_sum,epot_sum,GPU)
  endif

END SUBROUTINE local_hamiltonian_OCL


subroutine finish_hamiltonian_OCL(orbs,ekin_sum,epot_sum,GPU)
  use module_precisions
  use module_types
  implicit none
  type(orbitals_data), intent(in) :: orbs
  real(gp), intent(out) :: ekin_sum,epot_sum
  type(GPU_pointers), intent(inout) :: GPU

  integer :: iorb

  call ocl_finish(GPU%queue)
  ekin_sum=0.0_gp
  epot_sum=0.0_gp
  do iorb=1,orbs%norbp
    ekin_sum = ekin_sum + orbs%kwgts(orbs%iokpt(iorb))*orbs%occup(orbs%isorb+iorb)*((GPU%ekin(1,iorb)+GPU%ekin(2,iorb))&
                 - (GPU%epot(1,iorb)+GPU%epot(2,iorb)))
    epot_sum = epot_sum + orbs%kwgts(orbs%iokpt(iorb))*orbs%occup(orbs%isorb+iorb)*(GPU%epot(1,iorb)+GPU%epot(2,iorb))
  end do

  !free pinning information for wavefunctions
!  do iorb=1,orbs%norbp
!     do ispinor=1,orbs%nspinor
!        call ocl_release_mem_object(GPU%psicf_host(1+(ispinor-1)*2,iorb))
!        call ocl_release_mem_object(GPU%psicf_host(2+(ispinor-1)*2,iorb))
!        call ocl_release_mem_object(GPU%hpsicf_host(1+(ispinor-1)*2,iorb))
!        call ocl_release_mem_object(GPU%hpsicf_host(2+(ispinor-1)*2,iorb))
!     end do
!  end do
  !free pinning information for potential

END SUBROUTINE finish_hamiltonian_OCL


subroutine preconditionall_OCL(orbs,lr,ncong,hpsi,gnrm,gnrm_zero,GPU)
  use module_precisions
  use module_bigdft_profiling
  use module_bigdft_arrays
  use module_types
  use locreg_operations
  use locregs
  use wrapper_linalg
  implicit none
  type(orbitals_data), intent(in) :: orbs
  integer, intent(in) :: ncong
  type(locreg_descriptors), intent(in) :: lr
  real(dp), intent(out) :: gnrm,gnrm_zero
  real(wp), dimension(array_dim(lr),orbs%nspinor,orbs%norbp), intent(inout) :: hpsi
  !local variables
  character(len=*), parameter :: subname='preconditionall_OCL'
  logical, parameter :: pin=.false.
  integer ::  iorb,jorb,ikpt,inds,ncplx
  real(wp) :: scpr
  real(gp) :: eval_zero,evalmax,cprecr
  type(GPU_pointers), intent(inout) :: GPU
  type(workarr_precond) :: w
  real(wp), dimension(:), allocatable :: b
  real(kind=8) :: rqueue, rcontext, psicf(4)
  integer(f_address) :: queue, context, paddr(2), hpsiaddr
  equivalence(rcontext, context)
  equivalence(rqueue, queue)
  equivalence(psicf, paddr)

  rcontext = GPU%context
  rqueue = GPU%queue
  !stream ptr array

  !the eval array contains all the values
  !take the max for all k-points
  !one may think to take the max per k-point

  call f_routine(id=subname)

  !arrays for the CG procedure
  b=f_malloc((/array_dim(lr) * orbs%nspinor/),id='b')

  if (pin) then
     call pin_ocl_precond_b(GPU%precond(1), context, queue, lr, b(1))
     if(orbs%nspinor == 2) then
        call pin_ocl_precond_b(GPU%precond(2), context, queue, lr, b(1 + array_dim(lr)))
     end if
  end if
  hpsiaddr = 0
  if (pin .and. orbs%norbp > 0) call ocl_create_write_buffer_host( GPU%context, &
       orbs%norbp*orbs%nspinor*array_dim(lr)*8, hpsi, hpsiaddr )

     gnrm=0.0_dp
     gnrm_zero=0.0_dp
!!$  call allocate_work_arrays(lr%geocode,lr%hybrid_on,orbs%nspinor,lr%d,w)
  call allocate_work_arrays(lr,orbs%nspinor,w)
  if (orbs%norbp >0) ikpt=orbs%iokpt(1)
  do iorb=1,orbs%norbp
     !if it is the first orbital or the k-point has changed calculate the max
     if (orbs%iokpt(iorb) /= ikpt .or. iorb == 1) then
        !the eval array contains all the values
        !take the max for all k-points
        !one may think to take the max per k-point
        evalmax=orbs%eval((orbs%iokpt(iorb)-1)*orbs%norb+1)
        do jorb=1,orbs%norb
           evalmax=max(orbs%eval((orbs%iokpt(iorb)-1)*orbs%norb+jorb),evalmax)
        enddo
        eval_zero=evalmax
        ikpt=orbs%iokpt(iorb)
     end if

     if (orbs%kpts(1,orbs%iokpt(iorb))**2+orbs%kpts(2,orbs%iokpt(iorb))**2+ &
          orbs%kpts(3,orbs%iokpt(iorb))**2 > 0.0_gp .or. orbs%nspinor==2) then
        ncplx=2
     else
        ncplx=1
     end if

     do inds = 1, orbs%nspinor, ncplx !the streams should be more if nspinor>1

        psicf = 0
        if (pin) psicf(1:2) = GPU%psicf_host(1:2,iorb)
        if (pin .and. orbs%nspinor == 2) psicf(3:4) = GPU%psicf_host(3:4,iorb)

        !the nrm2 function can be replaced here by ddot
        scpr = nrm2(ncplx*array_dim(lr),hpsi(1,inds,iorb),1)
        call cprecr_from_eval(lr%mesh_coarse, eval_zero, orbs%eval(orbs%isorb+iorb), cprecr)
        call precondition_residue_ocl(lr, queue, &
             orbs%kpts(:,orbs%iokpt(iorb)), &
             hpsi(1,1,iorb), ncplx, &
             ncong, cprecr, GPU%ekin(:, iorb), w, b, GPU%sumrho, GPU%precond, &
             paddr, hpsiaddr, orbs%nspinor*(iorb-1) + (inds-1))
     
        if (orbs%occup(orbs%isorb+iorb) == 0.0_gp) then
           gnrm_zero=gnrm_zero+orbs%kwgts(orbs%iokpt(iorb))*scpr**2
        else
           !write(17,*)'iorb,gnrm',orbs%isorb+iorb,scpr**2
           gnrm=gnrm+orbs%kwgts(orbs%iokpt(iorb))*scpr**2
        end if
     end do
  end do
  
  if (pin) then
     call unpin_ocl_precond_b(GPU%precond(1))
     if (orbs%nspinor == 2) call unpin_ocl_precond_b(GPU%precond(2))
     if (orbs%norbp > 0) call ocl_release_mem_object(hpsiaddr)
  end if

  call deallocate_work_arrays(lr%mesh,lr%hybrid_on,orbs%nspinor,w)

  call f_free(b)

  call f_release_routine()

END SUBROUTINE preconditionall_OCL


subroutine local_partial_density_OCL(orbs,&
     nrhotot,lr,nspin,psi,rho_p,GPU)
  use f_precisions, only: f_address
  use module_precisions
  use module_types
  use locregs
  use locreg_operations
  implicit none
  integer, intent(in) :: nrhotot
  type(orbitals_data), intent(in) :: orbs
  integer, intent(in) :: nspin
  type(locreg_descriptors), intent(in) :: lr
  real(wp), dimension(array_dim(lr),orbs%norbp*orbs%nspinor), intent(in) :: psi
  real(dp), dimension(lr%mesh%ndims(1),lr%mesh%ndims(2),nrhotot,nspin), intent(inout) :: rho_p
  type(GPU_pointers), intent(inout) :: GPU
  !local variables
  logical, parameter :: pin=.false.
  integer:: iorb,iorb_r
  real(gp) :: hfac
  real(kind=8) :: rhopot, rqueue
  integer(f_address) :: paddr, rhopot_addr, queue
  equivalence(rqueue, queue)
  equivalence(rhopot, rhopot_addr)

  rqueue = GPU%queue
  
  call f_set_d(GPU%queue, lr%mesh%ndim , 1.d-20,  GPU%rhopot_up)
  if ( nspin == 2 ) then
    call f_set_d(GPU%queue, lr%mesh%ndim , 1.d-20,  GPU%rhopot_down)
  end if

  paddr = 0_f_address
  if (pin .and. orbs%norbp > 0) call ocl_create_read_buffer_host( GPU%context, &
       orbs%norbp*orbs%nspinor*array_dim(lr)*8, psi, paddr )
  !copy the wavefunctions on GPU
  do iorb=1,orbs%norbp*orbs%nspinor
     iorb_r = (iorb-1)/orbs%nspinor + 1

     if (orbs%spinsgn(orbs%isorb+iorb_r) > 0.0) then
        rhopot = GPU%rhopot_up
     else
        rhopot = GPU%rhopot_down
     endif
     hfac=orbs%kwgts(orbs%iokpt(iorb_r))*orbs%occup(orbs%isorb+iorb_r)/(lr%mesh%volume_element)

     call density_ocl(lr, queue, hfac, psi(1, iorb), rhopot_addr, GPU%sumrho(1), paddr, array_dim(lr)*8*(iorb-1))
  end do

  !copy back the results and leave the uncompressed wavefunctions on the card

  if (pin) call ocl_pin_write_buffer_async(GPU%context,GPU%queue,lr%mesh%ndim*8,rho_p,GPU%rhopot_up_host)
  call ocl_enqueue_read_buffer(GPU%queue,GPU%rhopot_up,lr%mesh%ndim*8,rho_p)
  if (pin) call ocl_release_mem_object(GPU%rhopot_up_host)
  if( nspin == 2 ) then
     if (pin) call ocl_pin_write_buffer_async(GPU%context,GPU%queue,lr%mesh%ndim*8,&
          rho_p(1,1,1,2),GPU%rhopot_down_host)
    call ocl_enqueue_read_buffer(GPU%queue,GPU%rhopot_down,lr%mesh%ndim*8,rho_p(1,1,1,2))
    if (pin) call ocl_release_mem_object(GPU%rhopot_down_host)
  endif

  !free pinning information for potential
  if (pin .and. orbs%norbp>0) call ocl_release_mem_object(paddr)

END SUBROUTINE local_partial_density_OCL
