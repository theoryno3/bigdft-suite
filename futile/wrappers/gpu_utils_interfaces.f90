!> @file
!!  In this file, we define utility interface functions for gpu memory 
!!  operations which choose the correct function to call at runtime based 
!!  on an input parameter (igpu), where igpu == 1 is CUDA, and igpu == 2 is sycl

!! @author
!!    Copyright (C) 2002-2017 BigDFT group  (LG)<br/>
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    Christoph Bauinger



module gpu_utils_interfaces
    use f_precisions, only: f_address, f_double
    use dictionaries, only: f_err_throw
    use iso_c_binding
    use time_profiling, only: f_timing, TIMING_UNINITIALIZED
    use dynamic_memory, only: f_routine, f_release_routine
    !use PSbase, only: TCAT_PSOLV_COMPUT
    implicit none
    private

    public :: reset_gpu_data_interface, get_gpu_data_interface
    public :: copy_gpu_data_interface
    public :: gpumalloc_interface, gpufree_interface, gpumemset_interface
    public :: synchronize_interface
    public :: gpugetdevicecount_interface, gpusetdevice_interface
    public :: gpu_get_mem_info_interface
    public :: poisson_daxpy_interface

    !> generic function for 1d and 2d arrays. Fine since we only move the pointer through
    interface reset_gpu_data_interface
        module procedure reset_gpu_data_interface_0d_v1, reset_gpu_data_interface_1d_v1,&
            reset_gpu_data_interface_1d_v2, reset_gpu_data_interface_2d
    end interface reset_gpu_data_interface

    !> generic function for 1d and 2d arrays. Fine since we only move the pointer through 
    interface get_gpu_data_interface
        module procedure get_gpu_data_interface_0d_v1,&
            get_gpu_data_interface_1d_v1,&
            get_gpu_data_interface_1d_v2, get_gpu_data_interface_2d
    end interface get_gpu_data_interface

    interface gpufree_interface
        module procedure gpufree_interface_v1, gpufree_interface_v2
    end interface gpufree_interface

    interface gpumalloc_interface
        module procedure gpumalloc_interface_v1, gpumalloc_interface_v2
    end interface gpumalloc_interface

    interface gpumemset_interface
        module procedure gpumemset_interface_v1, gpumemset_interface_v2
    end interface gpumemset_interface

    interface copy_gpu_data_interface
        module procedure copy_gpu_data_interface_v1, copy_gpu_data_interface_v2
    end interface copy_gpu_data_interface


contains

    !> GPU interface to the reset_gpu_data function specialized for 1d host arrays
    subroutine reset_gpu_data_interface_0d_v1(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: nelems, igpu
        real(f_double), intent(in) :: h_data
        integer(f_address), intent(in) :: d_data

        interface
            subroutine dpcpp_HtoD(nelems, h_data, d_data) bind(C, name='dpcpp_HtoD')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer, intent(in), value :: nelems
                real(kind=c_double), intent(in) :: h_data
                integer(f_address), intent(in), value :: d_data
            end subroutine dpcpp_HtoD
        end interface


        if (nelems /= 1) call f_err_throw('reset_gpu_data_interface_0d_v1 called with invalid value of nelems')

        call f_routine(id='reset_gpu_data_interface')
        call f_timing(TIMING_UNINITIALIZED,'ON')

        if (igpu == 1) then
            call reset_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then 
            call dpcpp_HtoD(nelems, h_data, d_data)
        else
            call f_err_throw('reset_gpu_data_interface_0d_v1 called with invalid value of igpu')
        end if

        call f_timing(TIMING_UNINITIALIZED,'OF')
        call f_release_routine()

    end subroutine reset_gpu_data_interface_0d_v1


    !> GPU interface to the reset_gpu_data function specialized for 1d host arrays
    subroutine reset_gpu_data_interface_1d_v1(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: nelems, igpu
        real(f_double), dimension(:), intent(in) :: h_data
        integer(f_address), intent(in) :: d_data

        interface
            subroutine dpcpp_HtoD(nelems, h_data, d_data) bind(C, name='dpcpp_HtoD')
                use iso_c_binding, only: c_int, c_double, c_ptr
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), intent(in) :: h_data(*)
                !type(c_ptr), intent(in) :: h_data
                integer(f_address), intent(in), value :: d_data
            end subroutine dpcpp_HtoD
        end interface


        call f_routine(id='reset_gpu_data_interface')
        call f_timing(TIMING_UNINITIALIZED,'ON')

        if (igpu == 1) then
            call reset_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then 
            call dpcpp_HtoD(nelems, h_data, d_data)
        else
            call f_err_throw('reset_gpu_data_interface called with invalid value of igpu')
        end if

        call f_timing(TIMING_UNINITIALIZED,'OF')
        call f_release_routine()

    end subroutine reset_gpu_data_interface_1d_v1


    !> GPU interface to the reset_gpu_data function specialized for 1d host arrays
    subroutine reset_gpu_data_interface_1d_v2(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: nelems, igpu
        real(f_double), dimension(:), intent(in) :: h_data
        type(c_ptr), intent(in) :: d_data

        interface
            subroutine dpcpp_HtoD(nelems, h_data, d_data) bind(C, name='dpcpp_HtoD')
                use iso_c_binding, only: c_ptr, c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), intent(in) :: h_data(*)
                type(c_ptr), intent(in), value :: d_data
            end subroutine dpcpp_HtoD
        end interface


        call f_routine(id='reset_gpu_data_interface')
        call f_timing(TIMING_UNINITIALIZED,'ON')

        if (igpu == 1) then
            call reset_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then 
            call dpcpp_HtoD(nelems, h_data, d_data)
        else
            call f_err_throw('reset_gpu_data_interface called with invalid value of igpu')
        end if

        call f_timing(TIMING_UNINITIALIZED,'OF')
        call f_release_routine()

    end subroutine reset_gpu_data_interface_1d_v2


    !> GPU interface to the reset_gpu_data function specialized for 2d host arrays
    subroutine reset_gpu_data_interface_2d(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: nelems, igpu
        real(f_double), dimension(:, :), intent(in) :: h_data
        integer(f_address), intent(in) :: d_data

        interface
            subroutine dpcpp_HtoD(nelems, h_data, d_data) bind(C, name='dpcpp_HtoD')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), intent(in) :: h_data(*)
                integer(f_address), intent(in), value :: d_data
            end subroutine dpcpp_HtoD
        end interface


        call f_routine(id='reset_gpu_data_interface')
        call f_timing(TIMING_UNINITIALIZED,'ON')

        if (igpu == 1) then
            call reset_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then 
            call dpcpp_HtoD(nelems, h_data, d_data)
        else
            call f_err_throw('reset_gpu_data_interface called with invalid value of igpu')
        end if

        call f_timing(TIMING_UNINITIALIZED,'OF')
        call f_release_routine()

    end subroutine reset_gpu_data_interface_2d


    !> GPU interface to the get_gpu_data function specialized for 1d host arrays
    subroutine get_gpu_data_interface_0d_v1(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        real(f_double), intent(inout) :: h_data
        integer(f_address), intent(in) :: d_data

        interface
            subroutine dpcpp_DtoH(nelems, h_data, d_data) bind(C, name='dpcpp_DtoH')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), intent(inout) :: h_data
                integer(f_address), intent(in), value :: d_data
            end subroutine dpcpp_DtoH
        end interface


        if (nelems /= 1) call f_err_throw('reset_gpu_data_interface_0d_v1 called with invalid value of nelems')

        if (igpu == 1) then !cuda case
            call get_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_DtoH(nelems, h_data, d_data)
        else
            call f_err_throw('get_gpu_data_interface called with invalid value of igpu')
        end if
    end subroutine get_gpu_data_interface_0d_v1


    !> GPU interface to the get_gpu_data function specialized for 1d host arrays
    subroutine get_gpu_data_interface_1d_v1(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        real(f_double), dimension(:), intent(inout) :: h_data
        integer(f_address), intent(in) :: d_data

        interface
            subroutine dpcpp_DtoH(nelems, h_data, d_data) bind(C, name='dpcpp_DtoH')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), dimension(*), intent(inout) :: h_data
                integer(f_address), intent(in), value :: d_data
            end subroutine dpcpp_DtoH
        end interface


        if (igpu == 1) then !cuda case
            call get_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_DtoH(nelems, h_data, d_data)
        else
            call f_err_throw('get_gpu_data_interface called with invalid value of igpu')
        end if
    end subroutine get_gpu_data_interface_1d_v1


    !> GPU interface to the get_gpu_data function specialized for 1d host arrays
    subroutine get_gpu_data_interface_1d_v2(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        real(f_double), dimension(:), intent(inout) :: h_data
        type(c_ptr), intent(in) :: d_data

        interface
            subroutine dpcpp_DtoH(nelems, h_data, d_data) bind(C, name='dpcpp_DtoH')
                use iso_c_binding, only: c_ptr, c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), dimension(*), intent(inout) :: h_data
                type(c_ptr), intent(in), value :: d_data
            end subroutine dpcpp_DtoH
        end interface


        if (igpu == 1) then !cuda case
            call get_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_DtoH(nelems, h_data, d_data)
        else
            call f_err_throw('get_gpu_data_interface called with invalid value of igpu')
        end if
    end subroutine get_gpu_data_interface_1d_v2


    !> GPU interface to the get_gpu_data function specialized for 2d host arrays
    subroutine get_gpu_data_interface_2d(igpu, nelems, h_data, d_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        real(f_double), dimension(:, :), intent(inout) :: h_data
        integer(f_address), intent(in) :: d_data

        interface
            subroutine dpcpp_DtoH(nelems, h_data, d_data) bind(C, name='dpcpp_DtoH')
                use iso_c_binding, only: c_ptr, c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                real(kind=c_double), dimension(*), intent(inout) :: h_data
                integer(f_address), intent(in), value :: d_data
            end subroutine dpcpp_DtoH
        end interface


        if (igpu == 1) then !cuda case
            call get_gpu_data(nelems, h_data, d_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_DtoH(nelems, h_data, d_data)
        else
            call f_err_throw('get_gpu_data_interface called with invalid value of igpu')
        end if
    end subroutine get_gpu_data_interface_2d


    !> interface for gpu data copying
    subroutine copy_gpu_data_interface_v1(igpu, nelems, dest_data, send_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        integer(f_address), intent(in) :: dest_data, send_data

        interface
            subroutine dpcpp_DtoD(nelems, dest_data, send_data) bind(C, name='dpcpp_DtoD')
                use iso_c_binding, only: c_int
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                integer(f_address), intent(in), value :: dest_data, send_data
            end subroutine dpcpp_DtoD
        end interface

        if (igpu == 1) then !cuda case
            call copy_gpu_data(nelems, dest_data, send_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_DtoD(nelems, dest_data, send_data)
        else
            call f_err_throw('copy_gpu_data_interface called with invalid value of igpu')
        end if
    end subroutine copy_gpu_data_interface_v1


    !> interface for gpu data copying
    subroutine copy_gpu_data_interface_v2(igpu, nelems, dest_data, send_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        type(c_ptr), intent(in) :: dest_data, send_data

        interface
            subroutine dpcpp_DtoD(nelems, dest_data, send_data) bind(C, name='dpcpp_DtoD')
                use iso_c_binding, only: c_int, c_ptr
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                type(c_ptr), intent(in), value :: dest_data, send_data
            end subroutine dpcpp_DtoD
        end interface


        if (igpu == 1) then !cuda case
            call copy_gpu_data(nelems, dest_data, send_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_DtoD(nelems, dest_data, send_data)
        else
            call f_err_throw('copy_gpu_data_interface called with invalid value of igpu')
        end if
    end subroutine copy_gpu_data_interface_v2


    !> GPU interface to the cudamalloc function
    subroutine gpumalloc_interface_v1(igpu, nelems, d_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        integer(f_address), intent(inout) :: d_data !target necessary? void * d_data?
        integer :: i_stat

        interface
            subroutine dpcpp_malloc(nelems, d_data, i_stat) bind(C, name='dpcpp_malloc')
                use iso_c_binding, only: c_int
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                integer(f_address), intent(inout) :: d_data
                integer, intent(out) :: i_stat
            end subroutine dpcpp_malloc
        end interface


        i_stat = 0

        if (igpu == 1) then !cuda case
            call cudamalloc(nelems, d_data, i_stat)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_malloc(nelems, d_data, i_stat)
        else
            call f_err_throw('gpumalloc_interface called with invalid value of igpu')
        end if

        if (i_stat /= 0) call f_err_throw('Error in gpumalloc_interface')
    end subroutine gpumalloc_interface_v1


    !> GPU interface to the cudamalloc function
    subroutine gpumalloc_interface_v2(igpu, nelems, d_data)
        implicit none
        integer, intent(in) :: igpu, nelems
        type(c_ptr), intent(inout) :: d_data
        integer :: i_stat

        interface
            subroutine dpcpp_malloc(nelems, d_data, i_stat) bind(C, name='dpcpp_malloc')
                use iso_c_binding, only: c_int, c_ptr
                implicit none
                integer(kind=c_int), intent(in), value :: nelems
                type(c_ptr), intent(inout) :: d_data
                integer, intent(out) :: i_stat
            end subroutine dpcpp_malloc
        end interface


        i_stat = 0

        if (igpu == 1) then !cuda case
            call cudamalloc(nelems, d_data, i_stat)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_malloc(nelems, d_data, i_stat)
        else
            call f_err_throw('gpumalloc_interface called with invalid value of igpu')
        end if

        if (i_stat /= 0) call f_err_throw('Error in gpumalloc_interface')
    end subroutine gpumalloc_interface_v2


    !> GPU interface to the cudafree function
    subroutine gpufree_interface_v1(igpu, d_data)
        implicit none
        integer, intent(in) :: igpu
        integer(f_address), intent(inout) :: d_data

        interface
            subroutine dpcpp_free(d_data) bind(C, name='dpcpp_free')
                use f_precisions, only: f_address
                implicit none
                integer(f_address), intent(inout) :: d_data
            end subroutine dpcpp_free
        end interface


        if (igpu == 1) then !cuda case
            call cudafree(d_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_free(d_data)
        else
            call f_err_throw('gpumalloc_interface called with invalid value of igpu')
        end if
    end subroutine gpufree_interface_v1


    !> GPU interface to the cudafree function
    subroutine gpufree_interface_v2(igpu, d_data)
        implicit none
        integer, intent(in) :: igpu
        type(c_ptr), intent(inout) :: d_data

        interface
            subroutine dpcpp_free(d_data) bind(C, name='dpcpp_free')
                use iso_c_binding, only: c_ptr
                implicit none
                type(c_ptr), intent(inout) :: d_data
            end subroutine dpcpp_free
        end interface


        if (igpu == 1) then !cuda case
            call cudafree(d_data)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_free(d_data)
        else
            call f_err_throw('gpumalloc_interface called with invalid value of igpu')
        end if
    end subroutine gpufree_interface_v2


    !> GPU interface to the cudamemset function
    subroutine gpumemset_interface_v1(igpu, d_data, value, nelems)
        implicit none
        integer, intent(in) :: igpu, value, nelems
        integer(f_address), intent(in) :: d_data
        integer :: i_stat

        interface
            subroutine dpcpp_memset(d_data, value, nelems, i_stat) bind(C, name='dpcpp_memset')
                use iso_c_binding, only: c_int
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: value, nelems
                integer(f_address), intent(in), value :: d_data
                integer, intent(out) :: i_stat
            end subroutine dpcpp_memset
        end interface


        i_stat = 0

        if (igpu == 1) then !cuda case
            call cudamemset(d_data, value, nelems, i_stat)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_memset(d_data, value, nelems, i_stat)
        else
            call f_err_throw('gpumemset_interface called with invalid value of igpu')
        end if

        if (i_stat /= 0) call f_err_throw('Error in gpumemset_interface')
    end subroutine gpumemset_interface_v1


    !> GPU interface to the cudamemset function
    subroutine gpumemset_interface_v2(igpu, d_data, value, nelems)
        implicit none
        integer, intent(in) :: igpu, value, nelems
        type(c_ptr), intent(in) :: d_data
        integer :: i_stat

        interface
            subroutine dpcpp_memset(d_data, value, nelems, i_stat) bind(C, name='dpcpp_memset')
                use iso_c_binding, only: c_int, c_ptr
                implicit none
                integer(kind=c_int), intent(in), value :: value, nelems
                type(c_ptr), intent(in), value :: d_data
                integer, intent(out) :: i_stat
            end subroutine dpcpp_memset
        end interface


        i_stat = 0

        if (igpu == 1) then !cuda case
            call cudamemset(d_data, value, nelems, i_stat)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_memset(d_data, value, nelems, i_stat)
        else
            call f_err_throw('gpumemset_interface called with invalid value of igpu')
        end if

        if (i_stat /= 0) call f_err_throw('Error in gpumemset_interface')
    end subroutine gpumemset_interface_v2


    !> interface for gpu synchronization
    subroutine synchronize_interface(igpu)
        implicit none
        integer, intent(in) :: igpu

        interface
            subroutine dpcpp_synchronize() bind(C, name='dpcpp_synchronize')
                implicit none
            end subroutine dpcpp_synchronize
        end interface


        call f_routine(id='synchronize_interface')
        call f_timing(TIMING_UNINITIALIZED,'ON')

        if (igpu == 1) then !cuda case
            call synchronize()
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_synchronize()
        else
            call f_err_throw('synchronize_interface called with invalid value of igpu')
        end if

        call f_timing(TIMING_UNINITIALIZED,'OF')
        call f_release_routine()
    end subroutine synchronize_interface


    !>interface function to determine the number of available devices
    subroutine gpugetdevicecount_interface(igpu, num_devices)
        implicit none
        integer, intent(in) :: igpu
        integer, intent(inout) :: num_devices

        interface
            subroutine dpcpp_getdevicecount(where, num_devices) bind(C, name='dpcpp_getdevicecount')
                use iso_c_binding, only: c_int
                implicit none
                integer(kind=c_int), intent(in), value :: where
                integer(kind=c_int), intent(in) :: num_devices
            end subroutine dpcpp_getdevicecount
        end interface


        if (igpu == 1) then !cuda case
            call cudagetdevicecount(num_devices)
        else if (igpu == 2) then !sycl gpu case
            call dpcpp_getdevicecount(0, num_devices)
        else if (igpu == 3) then !sycl cpu case
            call dpcpp_getdevicecount(1, num_devices)
        else
            call f_err_throw('gpugetdevicecount_interface called with invalid value of igpu')
        end if
    end subroutine gpugetdevicecount_interface


    !> interface function which sets the default device for the current MPI rank
    !! to the device given by 'device'.
    subroutine gpusetdevice_interface(igpu, device)
        implicit none
        integer, intent(in) :: igpu, device

        interface
            subroutine dpcpp_setdevice(where, device) bind(C, name='dpcpp_setdevice')
                use iso_c_binding, only: c_int
                implicit none
                integer(kind=c_int), intent(in), value :: where, device
            end subroutine dpcpp_setdevice
        end interface


        if (igpu == 1) then !cuda case
            call cudasetdevice(device)
        else if (igpu == 2) then !sycl gpu case
            call dpcpp_setdevice(0, device)
        else if (igpu == 3) then !sycl cpu case
            call dpcpp_setdevice(1, device)
        else
            call f_err_throw('gpusetdevice_interface called with invalid value of igpu')
        end if
    end subroutine gpusetdevice_interface


    !> interface for memory info functionality
    subroutine gpu_get_mem_info_interface(igpu, freeGPUSize, totalGPUSize)
        use iso_c_binding, only : c_size_t

        implicit none
        integer, intent(in) :: igpu
        integer(kind=c_size_t), intent(inout) :: freeGPUSize, totalGPUSize

        interface
            subroutine dpcpp_get_mem_info(freeGPUSize, totalGPUSize) bind(C, name='dpcpp_get_mem_info')
                use iso_c_binding, only: c_int, c_size_t
                implicit none
                integer(kind=c_size_t), intent(inout) :: freeGPUSize, totalGPUSize
            end subroutine dpcpp_get_mem_info
        end interface


        if (igpu == 1) then !cuda case
            call cuda_get_mem_info(freeGPUSize, totalGPUSize)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_get_mem_info(freeGPUSize, totalGPUSize)
        else
            call f_err_throw('gpu_get_mem_info_interface called with invalid value of igpu')
        end if
    end subroutine gpu_get_mem_info_interface


    !> interface for daxpy computation
    subroutine poisson_daxpy_interface(igpu, nelems, alpha, d_x, facx, d_y, facy, offset_y)
        implicit none
        integer, intent(in) :: igpu, nelems, facx, facy, offset_y
        real(f_double), intent(in) :: alpha
        type(c_ptr), intent(in) :: d_x, d_y

        interface
            subroutine dpcpp_poisson_daxpy(nelems, alpha, d_x, facx,&
                    d_y, facy, offset_y) bind(C, name='dpcpp_poisson_daxpy')
                use iso_c_binding, only: c_int, c_double, c_ptr
                implicit none
                integer(kind=c_int), intent(in), value :: nelems, facx, facy, offset_y
                real(kind=c_double), intent(in), value :: alpha
                type(c_ptr), intent(in), value :: d_x, d_y
            end subroutine dpcpp_poisson_daxpy
        end interface


        if (igpu == 1) then !cuda case
            call poisson_cublas_daxpy(nelems, alpha, d_x, facx, d_y, facy, offset_y)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_poisson_daxpy(nelems, alpha, d_x, facx, d_y, facy, offset_y)
        else
            call f_err_throw('poisson_daxpy_interface called with invalid value of igpu')
        end if
    end subroutine poisson_daxpy_interface

end module gpu_utils_interfaces