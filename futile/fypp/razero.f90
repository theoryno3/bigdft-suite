#:def fname(basename, rank)
$: futile.func_name(basename, vector, rank=rank)
#:enddef fname
#:set ALLRANKONE = [futile.Intrinsic(name=scalar.name, rank=1) for scalar in futile.AllScalars]
#:set ALLVECTORS = [futile.Intrinsic(name=scalar.name, rank=r) for scalar in futile.AllScalars for r in range(1,8)]
#:set ALLSCALARS = [futile.Intrinsic(name=scalar.name, rank=0) for scalar in futile.AllScalars]
#:set basename = 'f_zero'
#:set internal = 'setzero'
#:set longintscalar = futile.Intrinsic(letter='li') 
#:set intscalar = futile.Intrinsic(letter='i') 
module ${basename}$_module

   use f_precisions

   implicit none

   private

   integer, public, save :: TCAT_INIT_TO_ZERO

! .. f:function:: f_zero(x [, size, length])

!     Initializes to zero the values of x. The variable x can be of any intrinsic fortran type, kind and rank.
!     Zero values for logical and character types are assumed as .false. and blanks, respectively.
!     If size is present, the variable x must be scalar, and intialization is performed for all memory elements
!     which follow the x address in memory, following the kint and type convention of x.
!     If x if of type character and of rank larger than one, or if size is specified, an extra argument len has to be specified indicating
!     the length of each of the element of the array.
!     If x is an array, is must be allocated on entry. Arrays of size larger than 1024 elements are initialized
!     with multithreaded parallelization if the functon is not called from whithin a parallel region.

   interface ${basename}$
#:for vector in ALLSCALARS + ALLVECTORS
      module procedure ${fname(basename, None)}$
#:endfor
#:for vector in ALLSCALARS
#:for int in [intscalar, longintscalar]
      module procedure ${fname(basename+'_'+int.letter, None)}$
#:endfor
#:endfor
   end interface ${basename}$

public :: ${basename}$

contains

#:for vector in ALLSCALARS
pure subroutine ${fname(basename, None)}$(array)
   ${vector.declaration()}$, intent(out) :: array
   array = ${vector.constant('0')}$
end subroutine ${fname(basename, None)}$

#:endfor
#:for vector in ALLSCALARS
#:for int in [intscalar, longintscalar]
subroutine ${fname(basename+'_'+int.letter,None)}$(array, size)
   ${int.declaration([])}$, intent(in) :: size
   ${vector.declaration()}$ :: array
   external :: ${fname(internal,1)}$
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call ${fname(internal,1)}$(array, ${longintscalar.constant('size')}$)
   call f_timer_resume()
end subroutine ${fname(basename+'_'+int.letter,None)}$

#:endfor
#:endfor
#:for vector in ALLVECTORS
subroutine ${fname(basename,None)}$(array)
   implicit none
   ${vector.declaration()}$, intent(out) :: array
   external :: ${fname(internal,1)}$
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call ${fname(internal,1)}$(array, product(${longintscalar.constant('shape(array)')}$))
   call f_timer_resume()
end subroutine ${fname(basename,None)}$

#:endfor
end module ${basename}$_module

#:for vector in ALLRANKONE
subroutine ${fname(internal,1)}$(array, size)
   use f_precisions
   implicit none
   ${longintscalar.declaration([])}$, intent(in) :: size
   ${vector.declaration(['size'])}$, intent(out) :: array
   !local variables
   ${longintscalar.type}$ :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > ${longintscalar.constant('1024')}$
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = ${vector.constant('0')}$
   end do
   !$omp end parallel do
end subroutine ${fname(internal,1)}$

#:endfor

!should be removed and placed in a suitable module
!> Routine doing daxpy with dx in real(kind=4)
subroutine dasxpdy(n,da,dx,incx,dy,incy)
  implicit none
  integer, intent(in) :: n,incx,incy
  real(kind=8), intent(in) :: da
  real(kind=4), dimension(*), intent(in) :: dx
  real(kind=8), dimension(*), intent(inout) :: dy
  !local variables
  integer :: i,ix,iy
  
  ix=1
  iy=1
  do i=1,n
     dy(iy)=dy(iy)+da*real(dx(ix),kind=8)
     ix=ix+incx
     iy=iy+incy
  end do
end subroutine dasxpdy


!> Copy from real(kind=4) into real(kind=8)
subroutine dscopy(n,dx,incx,dy,incy)
  implicit none
  integer, intent(in) :: n,incx,incy
  real(kind=8), dimension(*), intent(in) :: dx
  real(kind=4), dimension(*), intent(out) :: dy
  !local variables
  integer :: i,ix,iy
  
  ix=1
  iy=1
  do i=1,n
     dy(iy)=real(dx(ix),kind=4)
     ix=ix+incx
     iy=iy+incy
  end do

end subroutine dscopy


!> dcopy for integer arrays
subroutine icopy(n,dx,incx,dy,incy)
  implicit none
  integer, intent(in) :: n,incx,incy
  integer, dimension(*), intent(in) :: dx
  integer, dimension(*), intent(out) :: dy
  !local variables
  integer :: i,ix,iy
  
  ix=1
  iy=1
  do i=1,n
     dy(iy)=dx(ix)
     ix=ix+incx
     iy=iy+incy
  end do

end subroutine icopy

subroutine diff_i(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer, dimension(n), intent(in) :: a
  integer, dimension(n), intent(in) :: b
  integer, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_i


subroutine diff_li(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer(kind=8), dimension(n), intent(in) :: a
  integer(kind=8), dimension(n), intent(in) :: b
  integer(kind=8), intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=int(0,kind=8)
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_li


subroutine diff_r(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  real, dimension(n), intent(in) :: a
  real, dimension(n), intent(in) :: b
  real, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0.0e0
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_r


subroutine diff_d(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  double precision, dimension(n), intent(in) :: a
  double precision, dimension(n), intent(in) :: b
  double precision, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0.0d0
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_d


subroutine diff_l(n,a,b,diff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  logical, dimension(n), intent(in) :: a
  logical, dimension(n), intent(in) :: b
  logical, intent(out) :: diff
  !local variables
  integer(f_long) :: i

  diff=.false.
  do i=1,n
     diff=a(i) .eqv. b(i)
     if (diff) exit
  end do
end subroutine diff_l


subroutine diff_ci(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  character, dimension(n), intent(in) :: a
  integer, dimension(n), intent(in) :: b
  integer, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0
  do i=1,n
     if (diff < abs(ichar(a(i))-b(i))) then
        diff=max(diff,abs(ichar(a(i))-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_ci


subroutine f_itoa(n,src,dest)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer(kind=4), dimension(n), intent(in) :: src
  character, dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=achar(src(i))
  end do

end subroutine f_itoa


subroutine f_litoa(n,src,dest)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer(f_long), dimension(n), intent(in) :: src
  character, dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=achar(src(i))
  end do

end subroutine f_litoa

subroutine f_atoi(n,src,dest)
  use f_precisions, only: f_integer,f_long
  implicit none
  integer(f_long), intent(in) :: n
  character, dimension(n), intent(in) :: src
  integer(f_integer), dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=ichar(src(i))
  end do

end subroutine f_atoi

subroutine f_atoli(n,src,dest)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  character, dimension(n), intent(in) :: src
  integer(f_long), dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=ichar(src(i))
  end do

end subroutine f_atoli

